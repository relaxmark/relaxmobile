package nl.relax.posconfig;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class POSConfigActivity extends Activity {
    EditText txtURL, txtTABLES_RETRY_INTERVAL, txtNAMESPACE, txtID_SYS_MODULE, txtREF_CAN_CD, txtSTR_DB_IDENTIFIER, txtSOCKET_TIME_OUT, txtRETRIES,
            txtRETRY_INTERVAL;
    POSConfig config;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.password_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);

        alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                String password = userInput.getText().toString();
                if (password.equals("geheim")) {
                    dialog.cancel();
                } else {
                    Toast.makeText(getApplicationContext(), "Password onjuist", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();
        String mac = info.getMacAddress().replace(':', '-');
        config = new POSConfig(mac);

        try {
            config.load();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        setContentView(R.layout.main);

        txtURL = (EditText) findViewById(R.id.txtURL);
        txtNAMESPACE = (EditText) findViewById(R.id.txtNAMESPACE);
        txtID_SYS_MODULE = (EditText) findViewById(R.id.txtID_SYS_MODULE);
        txtSTR_DB_IDENTIFIER = (EditText) findViewById(R.id.txtSTR_DB_IDENTIFIER);
        txtSOCKET_TIME_OUT = (EditText) findViewById(R.id.txtSOCKET_TIME_OUT);
        txtRETRIES = (EditText) findViewById(R.id.txtRETRIES);
        txtRETRY_INTERVAL = (EditText) findViewById(R.id.txtRETRY_INTERVAL);
        txtTABLES_RETRY_INTERVAL = (EditText) findViewById(R.id.txtTABLES_RETRY_INTERVAL);

        txtURL.setText(config.getURL());
        txtNAMESPACE.setText(config.getNamespace());
        txtID_SYS_MODULE.setText(Integer.toString(config.getID_SYS_MODULE()));
        txtSTR_DB_IDENTIFIER.setText(config.getSTR_DB_IDENTIFIER());
        txtSOCKET_TIME_OUT.setText(Integer.toString(config.getTimeout()));
        txtRETRIES.setText(Integer.toString(config.getRetries()));
        txtRETRY_INTERVAL.setText(Integer.toString(config.getRetryInterval()));
        txtTABLES_RETRY_INTERVAL.setText(Integer.toString(config.getTablesRefreshInterval()));

        Button btn = (Button) findViewById(R.id.btnSave);
        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                config.setURL(txtURL.getText().toString().trim());
                config.setNamespace(txtNAMESPACE.getText().toString().trim());
                config.setID_SYS_MODULE(Util.parseInt(txtID_SYS_MODULE.getText().toString().trim(), 0));
                config.setSTR_DB_IDENTIFIER(txtSTR_DB_IDENTIFIER.getText().toString().trim());
                config.setTimeout(Util.parseInt(txtSOCKET_TIME_OUT.getText().toString(), 5000));
                config.setRetries(Util.parseInt(txtRETRIES.getText().toString(), 4));
                config.setRetryInterval(Util.parseInt(txtRETRY_INTERVAL.getText().toString(), 100));
                config.setTablesRefreshInterval(Util.parseInt(txtTABLES_RETRY_INTERVAL.getText().toString(), 15000));
                try {
                    config.save();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                finish();
            }
        });
    }

    @Override
    public void onPause() {

        super.onPause();
    }
}