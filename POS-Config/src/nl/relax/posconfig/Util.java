package nl.relax.posconfig;

public class Util {

    public static int parseInt(String s, int def) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            e.printStackTrace();
            return def;
        }
    }
}
