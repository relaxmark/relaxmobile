package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * The Device info is requested before the logon and contains various properties
 * related to the physical device that the app is running on. For example, a
 * device (i.e. a phone or tablet) can be linked to a certain user id, in which
 * case a SHA string is returned and the logon procedure can be skipped.
 * 
 * @author erik
 */
public class Device {

    /**
     * The device ID
     */
    public final int id;

    /**
     * The device name
     */
    private final String name;

    /**
     * The screen resolution of the device as known by the server (unused in
     * mobile apps)
     */
    private final String screenResolution;

    /**
     * The ID of the default location
     */
    private final Integer idLocation;

    /**
     * The ID of the default table
     */
    private final Integer idTable;

    /**
     * The table name
     */
    private final String tableName;

    /**
     * The next screen (unused in mobile apps)
     */
    private final String nextScreen;

    /**
     * The default user ID. If this is not 0, the member p_strSHA is also set
     * and the Log-on procedure can be skipped.
     */
    private final Integer idUser;

    /**
     * The SHA string (session ID). This is only filled if p_ID_SYS_User is not
     * 0.
     */
    private String sha;

    /**
     * The relation ID (unused in mobile apps)
     */
    private final Integer idRelation;

    /**
     * The relation name (unused in mobile apps)
     */
    private final String relationName;

    /**
     * The general welcome message (unused in mobile apps)
     */
    private final String welcomeMessage;

    /**
     * The personal welcome message (unused in mobile apps)
     */
    private final String welcomeMessagePersonal;

    /**
     * The group ID is needed for POSClient.getSYS_Users()
     */
    private final int idSysGroup;

    /**
     * Last control ID
     */
    final int lastControlId;

    /**
     * Constructor
     * 
     * @param o
     */
    Device(SoapObject o) {
        this.id = Integer.parseInt(o.getProperty("p_ID_SYS_Device").toString());

        String name;
        try {
            name = o.getProperty("p_strName").toString();
        } catch (Throwable e) {
            // TODO: remove the try/catch block when the property actually
            // exists
            name = "-";
        }
        this.name = name;
        this.screenResolution = o.getProperty("p_strResolution").toString();
        this.idLocation = Integer.parseInt(o.getProperty("p_ID_POS_Layout").toString());
        this.idTable = Integer.parseInt(o.getProperty("p_ID_POS_Table").toString());
        this.tableName = o.getProperty("p_strTableName").toString();
        this.nextScreen = o.getProperty("p_strNextScreen").toString();
        this.idUser = Integer.parseInt(o.getProperty("p_ID_SYS_User").toString());
        this.idRelation = Integer.parseInt(o.getProperty("p_ID_REL_Relation").toString());
        this.relationName = o.getProperty("p_strRelationName").toString();
        this.welcomeMessage = o.getProperty("p_strGeneralWelcomeMessage").toString();
        this.welcomeMessagePersonal = o.getProperty("p_strPersonalWelcomeMessage").toString();
        this.sha = o.getProperty("p_strSHA").toString();
        this.idSysGroup = Integer.parseInt(o.getProperty("p_ID_SYS_Group").toString());
        this.lastControlId = Integer.parseInt(o.getProperty("p_intControlID").toString());
    }

    /**
     * The screen resolution
     * 
     * @return
     */
    public String getScreenResolution() {
        return screenResolution;
    }

    /**
     * The location ID
     * 
     * @return
     */
    public Integer getIdLocation() {
        return idLocation;
    }

    /**
     * Get the table id
     * 
     * @return
     */
    public Integer getIdTable() {
        return idTable;
    }

    /**
     * Get the table name
     * 
     * @return
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * The next screen
     * 
     * @return
     */
    public String getNextScreen() {
        return nextScreen;
    }

    /**
     * Get the user ID. This is only filled if a default user is linked to this
     * device, otherwise it is 0.
     * 
     * @return
     */
    public Integer getIdUser() {
        return idUser;
    }

    /**
     * The SHA string is only filled if a default user is linked to this device,
     * otherwise it is empty.
     * 
     * @return
     */
    public String getSha() {
        return sha;
    }

    /**
     * The relation ID
     * 
     * @return
     */
    public Integer getIdRelation() {
        return idRelation;
    }

    /**
     * The relation name
     * 
     * @return
     */
    public String getRelationName() {
        return relationName;
    }

    /**
     * Welcome message
     * 
     * @return
     */
    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    /**
     * Personal welcome message
     * 
     * @return
     */
    public String getWelcomeMessagePersonal() {
        return welcomeMessagePersonal;
    }

    /**
     * This is needed for POSClient.getSYS_Users
     * 
     * @return
     */
    public int getIdSysGroup() {
        return idSysGroup;
    }

    /**
     * Get the name
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    public void resetSha() {
        this.sha = null;
    }
}
