package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

/**
 * This class holds all information regarding the Hotel room booking.
 * 
 * @author erik
 */
public class HotelRoomInfo extends Talk2MeBaseType {
    private final int idRelation;

    private final String description1, description2;

    private final Talk2MeDate dateArrival;

    private final Talk2MeDate dateCheckout;

    private final boolean isArrived;

    private final int idBooking;

    private HotelGuestResponse hotelGuestInfo;

    HotelRoomInfo(SoapObject o) {
        this.idRelation = Integer.parseInt(o.getPropertyAsString("P_ID_REL_Relation"));
        this.description1 = checkNIL(o.getProperty("p_strDescription1"));
        this.description2 = checkNIL(o.getProperty("p_strDescription2"));

        Object p_datArrival = o.getProperty("p_datArrival");
        Object p_datCheckout = o.getProperty("p_datCheckout");

        this.dateArrival = p_datArrival == null ? null : new Talk2MeDate(p_datArrival.toString());
        this.dateCheckout = p_datCheckout == null ? null : new Talk2MeDate(p_datCheckout.toString());
        this.idBooking = Integer.parseInt(o.getPropertyAsString("p_ID_HTL_OVR_KOP"));
        this.isArrived = Integer.parseInt(o.getPropertyAsString("p_blnArrived")) != 0;
    }

    /**
     * Get the relation ID
     * 
     * @return
     */
    public int getIdRelation() {
        return idRelation;
    }

    /**
     * Get the booking ID.
     * 
     * @return The booking ID or 0 if there is no booking ID available.
     */
    public int getIdBooking() {
        return idBooking;
    }

    /**
     * Get the description
     * 
     * @return
     */
    public String getDescription() {
        return description1;
    }

    /**
     * Get the description 2nd line
     * 
     * @return
     */
    public String getDescription2() {
        return description2;
    }

    /**
     * Check if the room is booked (i.e. if there is a valid booking ID)
     * 
     * @return
     */
    public boolean isBooked() {
        return idBooking != 0;
    }

    /**
     * Get the arrival date
     * 
     * @return
     */
    public Talk2MeDate getDateArrival() {
        return dateArrival;
    }

    /**
     * Get the check-out date
     * 
     * @return
     */
    public Talk2MeDate getDateCheckout() {
        return dateCheckout;
    }

    /**
     * Returns true if the Guest has been checked into the room
     * 
     * @return
     */
    public boolean isArrived() {
        return isArrived;
    }

    @Override
    public String toString() {
        return description1;
    }

    void setHotelGuestResponse(HotelGuestResponse hgr) {
        this.hotelGuestInfo = hgr;
    }

    /**
     * Get all the dates that there are guests in the room.
     * 
     * @return
     */
    public List<Talk2MeDate> getBookedDatesWithGuests() {
        List<Talk2MeDate> result;

        if (hotelGuestInfo != null) {
            result = hotelGuestInfo.getBookedDates();
        } else {
            result = new ArrayList<Talk2MeDate>();
        }

        return result;
    }

    public HotelGuest getGuest(int id) {
        if (hotelGuestInfo != null) {
            return hotelGuestInfo.getGuest(id);
        } else {
            return null;
        }
    }

    /**
     * Get the guests that are booked on a date.
     * 
     * @param date
     *            The date
     * @return The guests that are booked on this date
     */
    public List<HotelGuest> getBookedGuests(Talk2MeDate date) {
        List<HotelGuest> result;
        if (hotelGuestInfo != null) {
            result = hotelGuestInfo.getHotelGuestInfo(date);
        } else {
            result = new ArrayList<HotelGuest>();
        }
        return result;
    }
}