package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

/**
 * The groups + activities that can be booked from a (SPA) Hotel room on a
 * certain day.
 * 
 * @author erik
 */
public class ActivitiesResponse {

    private final List<ActivityGroup> groups;

    ActivitiesResponse(SoapObject o) {
        this.groups = new ArrayList<ActivityGroup>();

        final HashMap<Integer, ActivityGroup> groupMap = new HashMap<Integer, ActivityGroup>();

        // groups
        SoapObject rows = (SoapObject) o.getProperty(1);
        int count = rows.getPropertyCount();
        for (int i = 0; i < count; i++) {
            ActivityGroup g = new ActivityGroup((SoapObject) rows.getProperty(i));
            groups.add(g);
            groupMap.put(g.id, g);
        }

        // activities
        rows = (SoapObject) o.getProperty(2);
        count = rows.getPropertyCount();
        for (int i = 0; i < count; i++) {
            Activity a = new Activity((SoapObject) rows.getProperty(i));
            ActivityGroup g = groupMap.get(a.idGroup);
            g.addActivity(a);
        }
    }

    /**
     * Get the groups of activities that can be booked.
     * 
     * @return
     */
    public List<ActivityGroup> getActivityGroups() {
        return groups;
    }
}
