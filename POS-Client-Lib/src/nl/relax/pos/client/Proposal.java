package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.List;

public class Proposal {

    public final int id;
    private final List<ProposalRow> rows;

    public Proposal(int proposalId) {
        this.id = proposalId;
        this.rows = new ArrayList<ProposalRow>();
    }

    public void addProposalRow(ProposalRow row) {
        rows.add(row);
    }

    public List<ProposalRow> getProposalRows() {
        return rows;
    }
}
