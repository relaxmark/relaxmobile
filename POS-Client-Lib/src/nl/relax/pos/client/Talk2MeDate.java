package nl.relax.pos.client;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Talk2MeDate {

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat FORMAT_OUT = new SimpleDateFormat("d MMMM");
    private static final SimpleDateFormat FORMAT_SHORT = new SimpleDateFormat("d/M");
    private static final SimpleDateFormat FORMAT_DATETIME = new SimpleDateFormat("d/M HH:mm:ss");
    private final String sdate;
    private final GregorianCalendar cal;

    /**
     * Constructor
     * 
     * @param s
     *            The date in format yyyy-MM-dd
     */
    public Talk2MeDate(String s) {
        this.sdate = s;

        String[] dmy = s.split("-");
        cal = new GregorianCalendar(Integer.parseInt(dmy[0]), Integer.parseInt(dmy[1]) - 1, Integer.parseInt(dmy[2]));
    }

    public Talk2MeDate(String date, int secondsSinceMidnight) {
        this.sdate = date;

        String[] dmy = date.split("-");
        cal = new GregorianCalendar(Integer.parseInt(dmy[0]), Integer.parseInt(dmy[1]) - 1, Integer.parseInt(dmy[2]));
        cal.setTimeInMillis(cal.getTimeInMillis() + (secondsSinceMidnight * 1000));
    }

    public Talk2MeDate(Date date) {
        this(FORMAT.format(date));
    }

    /**
     * Constructor to create a POSDate that represents the current date
     */
    public Talk2MeDate() {
        this(new Date());
    }

    @Override
    public String toString() {
        return FORMAT_OUT.format(cal.getTime());
    }

    public String getShortFormat() {
        return FORMAT_SHORT.format(cal.getTime());
    }

    public String getDateTimeFormat() {
        return FORMAT_DATETIME.format(cal.getTime());
    }

    public String getValue() {
        return sdate;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Talk2MeDate) {
            return ((Talk2MeDate) o).sdate.equals(sdate);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return sdate.hashCode();
    }

    /**
     * Get a GregorianCalendar object that represents this date
     * 
     * @return
     */
    public GregorianCalendar getCalendar() {
        return cal;
    }

    public static void main(String[] args) {
        String s = "2012-10-14";
        Talk2MeDate d = new Talk2MeDate(s);
        System.out.println(d.cal.getTime());
        System.out.println(d.toString());
    }
}
