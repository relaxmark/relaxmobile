package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class AAA2ChartResponse {

    private final Chart chart;

    public AAA2ChartResponse(SoapObject o) {
        this.chart = new Chart((SoapObject) o.getProperty(Chart.SOAP_ELEMENT));
    }

    /**
     * @return the chart
     */
    public Chart getChart() {
        return chart;
    }
}
