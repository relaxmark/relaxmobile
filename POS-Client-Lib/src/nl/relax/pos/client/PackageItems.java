package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class PackageItems extends Talk2MeTable<PackageItem> {

    PackageItems(SoapObject o) {
        super(o, "ttPOS_PackageItem");
    }

    @Override
    protected PackageItem createRow(SoapObject row) {
        return new PackageItem(row);
    }
}