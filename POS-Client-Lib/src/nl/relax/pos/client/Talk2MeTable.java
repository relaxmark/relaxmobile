package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

/**
 * Generic base type for table data from the Talk2Me server.
 * 
 * @author Erik
 * 
 * @param <E>
 */
abstract class Talk2MeTable<E> extends Talk2MeBaseType {

    final List<E> rows = new ArrayList<E>();

    Talk2MeTable(SoapObject o, String rowsCollectionName) {
        super();
        try {
            SoapObject rows = (SoapObject) o.getPropertySafely(rowsCollectionName);
            if (rows != null && rows.getPropertyCount() > 0) {
                int count = rows.getPropertyCount();
                for (int i = 0; i < count; i++) {
                    this.rows.add(createRow((SoapObject) rows.getProperty(i)));
                }
            }
        } catch (ClassCastException ignore) {
            // NIL response -> no package items; ignore
        }
    }

    protected abstract E createRow(SoapObject row);

    /**
     * Get the size
     * 
     * @return The size of the table in number of rows
     */
    public int size() {
        return rows.size();
    }

    /**
     * Get the rows
     * 
     * @return
     */
    public List<E> getRows() {
        return rows;
    }
}
