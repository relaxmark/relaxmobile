package nl.relax.pos.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.relax.pos.client.util.LruCache;

/**
 * The IconProvider is a utility class to retrieve Icon instances in a
 * convenient and fast way. The icons are loaded from the POS-server and cached
 * on the local file system and cached in memory for maximum performance.
 * 
 * @author erik
 */
public class IconProvider {

    private final Talk2MeClient client;
    private final File dir;
    private final File directoryFile;
    protected final Map<Integer, Icon> iconsRemote;
    private final Map<Integer, Icon> directory;
    private final String sha;
    private final LruCache<Integer, Icon> cache;

    /**
     * Constructor
     * 
     * @param client
     *            The POSClient to use to retrieve the icons from the server
     * @param sha
     *            The SHA String
     * @param localDir
     *            the directory to use for our local file-cache
     * @param cacheSize
     *            the maximum number of entry for the memory cache. Set to 0 to
     *            cache all entries.
     * @param preload
     *            Preload the images in the memory cache. Note that if the
     *            memory cache is small, this might not be effective to enable.
     * @throws Talk2MeException
     *             If there was a problem communicating to the POS-server
     * @throws IOException
     *             If there was an I/O problem with the local File system
     */
    public IconProvider(Talk2MeClient client, String sha, File localDir, int cacheSize, boolean preload) throws Talk2MeException, IOException {
        this.client = client;
        this.dir = localDir;
        this.directoryFile = new File(dir, "directory.dat");
        this.sha = sha;
        this.iconsRemote = loadFromServer();
        this.directory = loadDirectory();
        this.cache = new LruCache<Integer, Icon>(cacheSize == 0 ? iconsRemote.size() : cacheSize);
        sync();
        if (preload) {
            preload();
        }
    }

    private void preload() throws IOException {
        int i = 0;
        Set<Integer> ids = directory.keySet();
        for (Integer id : ids) {
            if (i++ > ids.size()) {
                return;
            }
            loadIcon(id);
        }
    }

    /**
     * Constructor. All images are preloaded and the memory cache contains all
     * images.
     * 
     * @param client
     *            The POSClient to use to retrieve the icons from the server
     * @param sha
     *            The SHA String
     * @param localDir
     *            the directory to use for our local file-cache
     * @throws Talk2MeException
     * @throws IOException
     */
    public IconProvider(Talk2MeClient client, String sha, File localCacheDir) throws Talk2MeException, IOException {
        this(client, sha, localCacheDir, 0, true);
    }

    /**
     * Constructor. The memory cache can contain all images.
     * 
     * @param client
     *            The POSClient to use to retrieve the icons from the server
     * @param sha
     *            The SHA String
     * @param localDir
     *            the directory to use for our local file-cache
     * @param preload
     *            Preload the images in the memory cache. Note that if the
     *            memory cache is small, this might not be effective to enable.
     * @throws Talk2MeException
     * @throws IOException
     */
    public IconProvider(Talk2MeClient client, String sha, File localCacheDir, boolean preload) throws Talk2MeException, IOException {
        this(client, sha, localCacheDir, 0, preload);
    }

    /**
     * Get an Icon. This Icon is guaranteed to have the image data. A best
     * effort is done to get the icon as fast as possible: In the best case,
     * it's loaded directly from memory; in the worst case it's downloaded from
     * the server.
     * 
     * @param id
     * @return The Icon
     * @throws IOException
     */
    public Icon getIcon(int id) throws IOException {
        Icon icon = cache.get(id);
        if (icon == null) {
            icon = loadIcon(id);
        }
        return icon;
    }

    private Icon loadIcon(int id) throws IOException {
        Icon entry = directory.get(id);
        Icon icon = new Icon(entry.id, entry.getName(), entry.getModifiedDate(), entry.getModified());
        File f = iconFile(icon);
        BufferedInputStream bis = null;

        try {
            bis = new BufferedInputStream(new FileInputStream(f));
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] b = new byte[16384];
            int read = 0;
            while ((read = bis.read(b)) >= 0) {
                if (read > 0) {
                    out.write(b, 0, read);
                }
            }
            icon.setData(out.toByteArray());
            cache.put(id, icon);
        } finally {
            if (bis != null) {
                bis.close();
            }
        }

        return icon;
    }

    private void saveIcon(Icon icon) throws IOException {
        File f = iconFile(icon);
        System.out.println("saving " + f.getAbsolutePath());
        BufferedOutputStream w = null;
        try {
            w = new BufferedOutputStream(new FileOutputStream(f));
            w.write(icon.decode());
        } finally {
            if (w != null) {
                w.close();
            }
        }
    }

    private File iconFile(Icon icon) {
        return new File(dir, Integer.toString(icon.id));
    }

    /**
     * Sync the remote icons with the icons on the local file system.
     * 
     * @throws Talk2MeException
     * @throws IOException
     */
    private void sync() throws Talk2MeException, IOException {
        List<Integer> notAvailable = new ArrayList<Integer>();
        List<Integer> changed = new ArrayList<Integer>();
        List<Integer> removed = new ArrayList<Integer>();

        Set<Integer> serverIds = iconsRemote.keySet();
        for (Integer id : serverIds) {
            Icon remote = iconsRemote.get(id);
            if (directory.containsKey(id)) {
                Icon local = directory.get(id);
                if (!(local.getName().equals(remote.getName()) && local.getModified() == remote.getModified() && local.getModifiedDate().equals(
                        remote.getModifiedDate()))) {
                    changed.add(id);
                }
            } else {
                notAvailable.add(id);
            }
        }

        Set<Integer> localIds = directory.keySet();
        for (Integer id : localIds) {
            if (!iconsRemote.containsKey(id)) {
                removed.add(id);
            }
        }

        // remove all changed and removed icons from the local file system and
        // directory
        removed.addAll(changed);
        for (Integer id : removed) {
            Icon i = directory.get(id);
            boolean deleteSuccessful = iconFile(i).delete();
            if (deleteSuccessful) {
                directory.remove(id);
            } else {
                System.err.println("Deleting icon " + i.getName() + " from local filesystem failed!");
            }
        }

        // retrieve all icons that are changed or not available locally from the
        // server.
        notAvailable.addAll(changed);
        try {
            for (Integer id : notAvailable) {
                Icon newIcon = client.getIcon(sha, id);
                saveIcon(newIcon);
                directory.put(id, newIcon);
            }
        } finally {
            // save no matter what; removing in the previous step of deleting
            // local files might still have been successful
            saveDirectory();
        }
    }

    private void saveDirectory() throws IOException {
        BufferedWriter w = null;
        try {
            w = new BufferedWriter(new FileWriter(directoryFile));
            Set<Integer> dirIds = directory.keySet();
            for (Integer id : dirIds) {
                Icon icon = directory.get(id);
                w.write(icon.id + "|" + icon.getName() + "|" + icon.getModifiedDate() + "|" + icon.getModified());
                w.newLine();
            }
        } finally {
            if (w != null) {
                w.close();
            }
        }
    }

    private Map<Integer, Icon> loadFromServer() throws Talk2MeException {

        List<Icon> icons = client.getIcons(sha, false);
        Map<Integer, Icon> map = new HashMap<Integer, Icon>();

        for (Icon icon : icons) {
            map.put(icon.id, icon);
        }

        return map;
    }

    private Map<Integer, Icon> loadDirectory() throws IOException {
        Map<Integer, Icon> map = new HashMap<Integer, Icon>();
        if (!dir.exists()) {
            dir.mkdirs();
            return map;
        }
        if (!directoryFile.exists()) {
            return map;
        }

        BufferedReader r = null;
        try {
            r = new BufferedReader(new FileReader(directoryFile));
            String line;
            while ((line = r.readLine()) != null) {
                String[] fields = line.split("\\|");
                if (fields.length == 4) {
                    int id = Integer.parseInt(fields[0]);
                    String name = fields[1];
                    String modifiedDate = fields[2];
                    int modified = Integer.parseInt(fields[3]);
                    Icon icon = new Icon(id, name, modifiedDate, modified);
                    if (iconFile(icon).exists()) {
                        map.put(id, icon);
                    }
                }
            }
        } finally {
            if (r != null) {
                r.close();
            }
        }

        return map;
    }
}
