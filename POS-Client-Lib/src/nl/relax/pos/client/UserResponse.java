package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

/**
 * The users that can log in
 * 
 * @author erik
 */
public class UserResponse {

    private final Map<Integer, User> userMap;
    private final List<User> users;

    UserResponse(SoapObject o) {
        this.userMap = new HashMap<Integer, User>();
        this.users = new ArrayList<User>();
        SoapObject rows = (SoapObject) o.getProperty(1);
        int count = rows.getPropertyCount();

        for (int i = 0; i < count; i++) {
            User u = new User((SoapObject) rows.getProperty(i));
            getUserMap().put(u.id, u);
            getUsers().add(u);
        }
    }

    /**
     * @return the userMap
     */
    public Map<Integer, User> getUserMap() {
        return userMap;
    }

    /**
     * @return the users
     */
    public List<User> getUsers() {
        return users;
    }
}
