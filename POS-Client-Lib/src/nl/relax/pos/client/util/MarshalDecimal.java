package nl.relax.pos.client.util;

import java.io.IOException;

import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class MarshalDecimal implements Marshal {

    private final String type;

    public MarshalDecimal(String type) {
        this.type = type;
    }

    public MarshalDecimal() {
        this("decimal");
    }

    @Override
    public Object readInstance(XmlPullParser parser, String namespace, String name, PropertyInfo expected) throws IOException, XmlPullParserException {
        return Double.parseDouble(parser.nextText());
    }

    @Override
    public void register(SoapSerializationEnvelope cm) {
        cm.addMapping(cm.xsd, type, Double.class, this);

    }

    @Override
    public void writeInstance(XmlSerializer writer, Object obj) throws IOException {
        writer.text(obj.toString());
    }
}
