package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

public class TableStatusResponse extends Talk2MeBaseType {

    private final List<Course> courses = new ArrayList<Course>();

    TableStatusResponse(SoapObject o, Courses allCourses) {
        String[] list = super.toArray(checkNIL(o.getProperty("p_lstID_POS_Course")));
        if (list != null && list.length > 0) {
            for (String item : list) {
                if (item != null && item.length() > 0) {
                    try {
                        int id = Integer.parseInt(item);
                        Course c = allCourses.get(id);
                        if (c != null) {
                            courses.add(c);
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public List<Course> getCourses() {
        return courses;
    }
}
