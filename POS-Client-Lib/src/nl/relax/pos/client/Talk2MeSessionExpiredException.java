package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * A specialization of a POSException in case of an expired session.
 * 
 * @author erik
 */
public class Talk2MeSessionExpiredException extends Talk2MeException {

    /**
     * 
     */
    private static final long serialVersionUID = 2488746998749575948L;

    public Talk2MeSessionExpiredException(SoapObject o) {
        super(o);
    }

    public Talk2MeSessionExpiredException(Exception e) {
        super(e);
    }

    public Talk2MeSessionExpiredException(String message) {
        super(message);
    }
}
