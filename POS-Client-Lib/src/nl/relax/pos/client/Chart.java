package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

public class Chart extends Talk2MeBaseType {

    public static final String SOAP_ELEMENT = "ttRES_Chart";

    public final int id;

    List<ChartRow> rows;
    Map<Talk2MeDate, List<ChartRow>> dateRows;

    public Chart(SoapObject o) {
        this.rows = new ArrayList<ChartRow>();
        this.dateRows = new LinkedHashMap<Talk2MeDate, List<ChartRow>>();

        this.id = 0;

        try {
            SoapObject rows = (SoapObject) o.getProperty(Chart.SOAP_ELEMENT);
            int count = rows.getPropertyCount();
            for (int i = 0; i < count; i++) {
                ChartRow row = new ChartRow((SoapObject) rows.getProperty(i));
                this.rows.add(row);
            }

            Collections.sort(this.rows, new Comparator<ChartRow>() {
                @Override
                public int compare(ChartRow lhs, ChartRow rhs) {
                    String t1 = lhs.getTimeBegin();
                    int tt1 = 0;
                    if (t1.length() == 5 && t1.charAt(2) == ':') {
                        t1 = t1.substring(0, 2) + t1.substring(3);
                        tt1 = Integer.parseInt(t1);
                    }
                    String t2 = rhs.getTimeBegin();
                    int tt2 = 0;
                    if (t2.length() == 5 && t2.charAt(2) == ':') {
                        t2 = t2.substring(0, 2) + t2.substring(3);
                        tt2 = Integer.parseInt(t2);
                    }

                    long l1 = lhs.getDatReservation().getCalendar().getTimeInMillis() + tt1;
                    long l2 = rhs.getDatReservation().getCalendar().getTimeInMillis() + tt2;

                    return (int) (l1 - l2);
                }
            });

            for (ChartRow row : this.rows) {
                Talk2MeDate date = row.getDatReservation();
                List<ChartRow> cr = dateRows.get(date);
                if (cr == null) {
                    cr = new ArrayList<ChartRow>();
                    dateRows.put(date, cr);
                }
                cr.add(row);
            }
        } catch (Throwable ignored) {
            // empty chart
        }
    }

    public List<ChartRow> getRows() {
        return rows;
    }

    public Map<Talk2MeDate, List<ChartRow>> getDateRows() {
        return dateRows;
    }
}
