package nl.relax.pos.client;

public interface RetryListener {
    void retrying(int retry, Exception e);
}
