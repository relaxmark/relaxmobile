package nl.relax.pos.client;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.ksoap2.serialization.SoapObject;

/**
 * An article that can be ordered.
 * 
 * @author erik
 */
public class Article extends Talk2MeBaseType {

    /**
     * The ID
     */
    public final int id;

    /**
     * The article number
     */
    private final int articleNumber;

    /**
     * The article group id.
     */
    @SuppressWarnings("unused")
    private final int idArticleGroup;

    /**
     * The article group number. Use this number to link the article with the
     * article group.
     */
    private final int groupNumber;

    /**
     * The name of the article
     */
    private final String name;

    /**
     * The order ID
     */
    public final int idOrder;

    private final int red, green, blue;

    private final int salesPrice;

    private final int idMenu;

    private final boolean available;

    private final int idIcon;

    private final String iconName;

    private final boolean isAskForName;

    private final boolean isAskForPrice;

    private final String description;

    private final int idCourse;

    private static final DecimalFormat FORMAT;
    static {
        FORMAT = new DecimalFormat("###0.00");
        FORMAT.setDecimalSeparatorAlwaysShown(true);
        FORMAT.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.FRANCE));
    }

    /**
     * Constructor
     * 
     * @param o
     */
    Article(SoapObject o) {
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.idArticleGroup = Integer.parseInt(o.getPropertyAsString("ID_RES_PLU_SubGroup"));
        this.groupNumber = Integer.parseInt(o.getPropertyAsString("intGroupNumber"));
        this.articleNumber = Integer.parseInt(o.getPropertyAsString("plu_nr_"));
        this.name = o.getPropertyAsString("strName");
        this.description = checkNIL(o.getProperty("strInfo"));

        this.idMenu = Integer.parseInt(o.getPropertyAsString("ID_POS_MenuPLU"));
        this.idOrder = Integer.parseInt(o.getPropertyAsString("intOrder"));
        this.red = Integer.parseInt(o.getPropertyAsString("intRed"));
        this.green = Integer.parseInt(o.getPropertyAsString("intGreen"));
        this.blue = Integer.parseInt(o.getPropertyAsString("intBlue"));
        this.salesPrice = Integer.parseInt(o.getPropertyAsString("intSalesPrice"));
        this.available = Integer.parseInt(o.getPropertyAsString("blnAvailable")) != 0;

        this.idIcon = Integer.parseInt(o.getPropertyAsString("ID_SYS_Icon"));
        this.iconName = o.getPropertyAsString("strIconName");

        this.isAskForName = Integer.parseInt(o.getPropertyAsString("blnAskForName")) != 0;
        this.isAskForPrice = Integer.parseInt(o.getPropertyAsString("blnAskForPrice")) != 0;

        this.idCourse = Integer.parseInt(o.getPropertyAsString("ID_POS_Course"));
    }

    Article(int price) {
        this.id = 0;
        this.idArticleGroup = 0;
        this.groupNumber = 0;
        this.articleNumber = 0;
        this.name = "Test";

        this.idMenu = 0;
        this.idOrder = 0;
        this.red = 0;
        this.green = 0;
        this.blue = 0;
        this.salesPrice = price;
        this.available = true;

        this.idIcon = 0;
        this.iconName = "";

        this.isAskForName = false;
        this.isAskForPrice = false;
        this.description = "";
        this.idCourse = 0;

    }

    @Override
    public String toString() {
        return getName() + " - " + FORMAT.format(getSalesPrice());
    }

    public String getPriceFormatted() {
        return FORMAT.format(getSalesPrice());
    }

    /**
     * The price of the article
     * 
     * @return The price of the article
     */
    public double getSalesPrice() {
        return salesPrice / 100d;
    }

    /**
     * The price of the article in cents
     * 
     * @return
     */
    public int getSalesPriceCents() {
        return salesPrice;
    }

    /**
     * The RGB values can be used to display the article with a color.
     * 
     * @return The red value (0-255)
     */
    public int getRed() {
        return red;
    }

    /**
     * The RGB values can be used to display the article with a color.
     * 
     * @return The green value (0-255)
     */
    public int getGreen() {
        return green;
    }

    /**
     * The RGB values can be used to display the article with a color.
     * 
     * @return The blue value (0-255)
     */
    public int getBlue() {
        return blue;
    }

    /**
     * The article number
     * 
     * @return The article number
     */
    public int getArticleNumber() {
        return articleNumber;
    }

    /**
     * The article group number. Use this number to link the article with the
     * article group.
     * 
     * @return The article group number
     */
    public int getGroupNumber() {
        return groupNumber;
    }

    /**
     * The name of the article
     * 
     * @return The name of the article
     */
    public String getName() {
        return name;
    }

    /**
     * Get the article description
     * 
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * The ID for a submenu related to this article (for example for 'rare'
     * 'medium' or 'well-done'. 0 means no submenu
     * 
     * @return
     */
    public int getIdArticleChoices() {
        return idMenu;
    }

    /**
     * Returns availability
     * 
     * @return
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * Get the Icon ID
     * 
     * @return
     */
    public int getIdIcon() {
        return idIcon;
    }

    /**
     * Get the name of the icon
     * 
     * @return
     */
    public String getIconName() {
        return iconName;
    }

    /**
     * Ask for the name of the article (for free text/user defined article
     * functionality)
     * 
     * @return
     */
    public boolean isAskForName() {
        return isAskForName;
    }

    /**
     * Get the course ID
     * 
     * @return
     */
    public int getIdCourse() {
        return idCourse;
    }

    /**
     * Ask for the price of the article (for free text/user defined article
     * functionality)
     * 
     * @return
     */
    public boolean isAskForPrice() {
        return isAskForPrice;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            Article a = new Article(i);
            int p1 = (int) (a.getSalesPrice() * 100);
            int p2 = a.getSalesPriceCents();
            if (p1 != p2)
                System.out.println(a.getSalesPrice() + " " + p2 + " " + p1);
        }

        System.out.println(new Article(230));
        System.out.println(new Article(230).getSalesPrice());
    }
}
