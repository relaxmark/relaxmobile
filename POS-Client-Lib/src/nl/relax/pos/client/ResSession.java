package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class ResSession extends Talk2MeBaseType {

    private final String sha;
    private final int firstOpeningHour;
    private final int lastOpeningHour;
    private final int orderNumber;
    private final boolean showCartOnBooking;
    final int lastControlId;

    //    <element name="p_intFirstOpeningHour" nillable="true" type="xsd:int"/>
    //    <element name="p_intLastClosingHour" nillable="true" type="xsd:int"/>
    //    <element name="p_intOrderNumber" nillable="true" type="xsd:int"/>
    //    <element name="p_blnShowChartOnBooking" nillable="true" type="xsd:int"/>
    //    <element name="p_strPSPID" nillable="true" type="xsd:string"/>
    //    <element name="p_strURLPayService" nillable="true" type="xsd:string"/>
    //    <element name="p_intTimesProposal" nillable="true" type="xsd:int"/>
    //    <element name="p_intProposal" nillable="true" type="xsd:int"/>
    public ResSession(SoapObject o) {
        this.sha = o.getProperty("p_strSHA").toString();
        this.firstOpeningHour = Integer.parseInt(o.getProperty("p_intFirstOpeningHour").toString());
        this.lastOpeningHour = Integer.parseInt(o.getProperty("p_intLastClosingHour").toString());
        this.orderNumber = Integer.parseInt(o.getProperty("p_intOrderNumber").toString());
        this.showCartOnBooking = !"0".equals(o.getProperty("p_blnShowChartOnBooking").toString());
        this.lastControlId = Integer.parseInt(o.getProperty("p_intControlID").toString());
    }

    public String getSha() {
        return sha;
    }

    public int getFirstOpeningHour() {
        return firstOpeningHour;
    }

    public int getLastOpeningHour() {
        return lastOpeningHour;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public boolean isShowCartOnBooking() {
        return showCartOnBooking;
    }
}
