package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * The response to a login request.
 * 
 * @author erik
 */
public class LoginResponse {

    /**
     * The SHA (session id) after a successful login
     */
    public final String sha;

    /**
     * Selected user
     */
    private final int selectedUser;

    LoginResponse(SoapObject o) {
        this.sha = o.getProperty("p_strSHA").toString();
        int su;
        try {
            su = Integer.parseInt(o.getProperty("p_ID_SYS_UserSelected").toString());
        } catch (Throwable t) {
            su = 0;
        }
        this.selectedUser = su;
    }

    public int getSelectedUser() {
        return selectedUser;
    }
}