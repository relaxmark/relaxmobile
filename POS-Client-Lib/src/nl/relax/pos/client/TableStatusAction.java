package nl.relax.pos.client;

public enum TableStatusAction {
    BUSY(1), FREE(2), CHANGE_COURSE(4);

    public final int id;

    TableStatusAction(int id) {
        this.id = id;
    }
}