package nl.relax.pos.client;

public interface SessionExpiredHandler {

    void notifySessionExpired(Talk2MeSessionExpiredException e);
}
