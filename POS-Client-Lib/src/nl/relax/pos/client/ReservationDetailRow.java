package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class ReservationDetailRow extends Talk2MeBaseType {

    //    <ttRES_ReservationDetailRow>
    //    <ID>695269</ID>
    //    <ID_RES_RES_DLN>413372</ID_RES_RES_DLN>
    //    <ID_RES_RES_PLU>2099460</ID_RES_RES_PLU>
    //    <ID_RES_ONL_INE>0</ID_RES_ONL_INE>
    //    <intType>6</intType>
    //    <aaa_kd_></aaa_kd_>
    //    <strNamePrimary>Dagentree (via hotel)</strNamePrimary>
    //    <strNameSecundary></strNameSecundary>
    //    <strNameFull></strNameFull>
    //    <strDurationShort></strDurationShort>
    //    <strDurationFull></strDurationFull>
    //    <tmc_beg>00:00</tmc_beg>
    //    <tmc_end></tmc_end>
    //    <intGuest>1</intGuest>
    //    <intObject>1</intObject>
    //    <strAdditionalInfo></strAdditionalInfo>
    //    <dblPrice>0</dblPrice>
    //    <intAvailableEmployee>0</intAvailableEmployee>
    //    <lstID_AvailableEmployee></lstID_AvailableEmployee>
    //    </ttRES_ReservationDetailRow>

    public final int id;
    private final int ID_RES_RES_DLN;
    private final int ID_RES_RES_PLU;
    private final int ID_RES_ONL_INE;
    private final int type;
    private final String aaa_kd_;
    private final String namePrimary;
    private final String nameSecundary;
    private final String nameFull;
    private final String durationShort;
    private final String durationFull;
    private final String timeBegin, timeEnd;
    private final int guest;
    private final int object;
    private final String additionalInfo;
    private final double price;
    private final int availableEmployees;
    private final String[] availableEmployeesIds;

    public ReservationDetailRow(SoapObject o) {
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.ID_RES_RES_DLN = Integer.parseInt(o.getPropertyAsString("ID_RES_RES_DLN"));
        this.ID_RES_RES_PLU = Integer.parseInt(o.getPropertyAsString("ID_RES_RES_PLU"));
        this.ID_RES_ONL_INE = Integer.parseInt(o.getPropertyAsString("ID_RES_ONL_INE"));
        this.type = Integer.parseInt(o.getPropertyAsString("intType"));
        this.aaa_kd_ = checkNIL(o.getProperty("aaa_kd_"));
        this.namePrimary = checkNIL(o.getProperty("strNamePrimary"));
        this.nameSecundary = checkNIL(o.getProperty("strNameSecundary"));
        this.nameFull = checkNIL(o.getProperty("strNameFull"));
        this.durationShort = checkNIL(o.getProperty("strDurationShort"));
        this.durationFull = checkNIL(o.getProperty("strDurationFull"));
        this.timeBegin = checkNILTime(o.getProperty("tmc_beg"));
        this.timeEnd = checkNILTime(o.getProperty("tmc_end"));
        this.guest = Integer.parseInt(o.getPropertyAsString("intGuest"));
        this.object = Integer.parseInt(o.getPropertyAsString("intObject"));
        this.additionalInfo = checkNIL(o.getProperty("strAdditionalInfo"));
        this.price = Double.parseDouble(checkNIL(o.getProperty("dblPrice")));
        this.availableEmployees = Integer.parseInt(checkNIL(o.getProperty("intAvailableEmployee")));
        this.availableEmployeesIds = checkNIL(o.getProperty("lstID_AvailableEmployee")).split(",");
    }

    public int getID_RES_RES_DLN() {
        return ID_RES_RES_DLN;
    }

    public int getID_RES_RES_PLU() {
        return ID_RES_RES_PLU;
    }

    public int getID_RES_ONL_INE() {
        return ID_RES_ONL_INE;
    }

    public int getType() {
        return type;
    }

    public String getAaa_kd_() {
        return aaa_kd_;
    }

    public String getNamePrimary() {
        return namePrimary;
    }

    public String getNameSecundary() {
        return nameSecundary;
    }

    public String getNameFull() {
        return nameFull;
    }

    public String getDurationShort() {
        return durationShort;
    }

    public String getDurationFull() {
        return durationFull;
    }

    public String getTimeBegin() {
        return timeBegin;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public int getGuest() {
        return guest;
    }

    public int getObject() {
        return object;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public double getPrice() {
        return price;
    }

    public int getAvailableEmployees() {
        return availableEmployees;
    }

    public String[] getAvailableEmployeesIds() {
        return availableEmployeesIds;
    }

    @Override
    public String toString() {
        return (timeBegin.length() > 0 || timeEnd.length() > 0) ? (timeBegin + " - " + timeEnd + "\n  " + nameFull) : nameFull;
    }
}
