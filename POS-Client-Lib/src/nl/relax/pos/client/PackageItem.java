package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class PackageItem extends Talk2MeBaseType {

    /**
     * The ID
     */
    public final int id;

    private final String name;

    private final int idArticle;

    private final int quantity;

    private final int amount;

    PackageItem(SoapObject o) {
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.idArticle = Integer.parseInt(o.getPropertyAsString("ID_RES_PLU"));
        this.name = checkNIL(o.getPropertyAsString("strName"));
        this.quantity = Integer.parseInt(o.getPropertyAsString("intQuantity"));
        this.amount = Integer.parseInt(o.getPropertyAsString("intdblAmount"));
    }

    public String getArticleName() {
        return name;
    }

    public int getIdArticle() {
        return idArticle;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getAmount() {
        return amount;
    }
}
