package nl.relax.pos.client;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.ksoap2.serialization.SoapObject;

/**
 * This defines sub-articles for articles. For example choices of sauces for a
 * certain dish, or arrangement options
 * 
 * @author erik
 * 
 */
public class ArticleOptions extends Talk2MeBaseType {

    /**
     * A menu defines one group of options that you can choose. For example
     * "well-done", "rare" and "medium" for a steak.
     * 
     * @author erik
     * 
     */
    public static class Menu {
        final List<Option> options = new ArrayList<Option>();
        private final int min, max;
        private final int level;
        private final int idMenu;
        private final int idArticle;

        public int getMaxChoosable() {
            return max;
        }

        public int getMinChoosable() {
            return min;
        }

        public int getIdMenu() {
            return idMenu;
        }

        public int getIdArticle() {
            return idArticle;
        }

        public Menu(Option c) {
            this.max = c.getMaxChoosable();
            this.min = c.getMinChoosable();
            this.level = c.level;
            this.idMenu = c.idMenu;
            this.idArticle = c.idArticle;
        }

        public List<Option> getOptions() {
            return options;
        }
    }

    /**
     * The options within a menu
     * 
     * @author erik
     * 
     */
    public static class Option {

        public final int id;
        final int level;
        private final int order;
        private final int idArticle;
        private final String name;
        private final boolean available;
        private final int max;
        private final double salesPrice;
        private final int min;
        private final int idMenu;

        private static final DecimalFormat FORMAT;
        static {
            FORMAT = new DecimalFormat("###0.00");
            FORMAT.setDecimalSeparatorAlwaysShown(true);
            FORMAT.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.FRANCE));
        }

        Option(SoapObject o) {
            this.id = Integer.parseInt(o.getPropertyAsString("ID"));
            this.level = Integer.parseInt(o.getPropertyAsString("intLevel"));
            this.order = Integer.parseInt(o.getPropertyAsString("intOrder"));
            this.idArticle = Integer.parseInt(o.getPropertyAsString("ID_RES_PLU"));
            this.name = o.getPropertyAsString("strName");
            this.available = Integer.parseInt(o.getPropertyAsString("blnAvailable")) != 0;
            this.min = Integer.parseInt(o.getPropertyAsString("intMinNumberToChoose"));
            this.max = Integer.parseInt(o.getPropertyAsString("intMaxNumberToChoose"));
            this.salesPrice = Double.parseDouble(o.getPropertyAsString("dblPLUPrice"));
            this.idMenu = Integer.parseInt(o.getPropertyAsString("ID_POS_MenuPLU"));
        }

        public String getName() {
            return name;
        }

        public boolean isAvailable() {
            return available;
        }

        int getMaxChoosable() {
            return max;
        }

        int getMinChoosable() {
            return min;
        }

        int getOrder() {
            return order;
        }

        @Override
        public String toString() {
            return getName() + (getSalesPrice() != 0 ? " - " + FORMAT.format(getSalesPrice()) : "");
        }

        public double getSalesPrice() {
            return salesPrice;
        }

        public int getIdArticle() {
            return idArticle;
        }

        public int getIdMenu() {
            return idMenu;
        }
    }

    private final List<Menu> menus = new ArrayList<Menu>();
    private final boolean type;
    private final String description;

    ArticleOptions(SoapObject ac) {
        this.type = Boolean.parseBoolean(ac.getPropertySafelyAsString("p_blnTypeOfMenu"));
        this.description = ac.getPropertySafelyAsString("p_blnTypeOfMenu");

        Object o = ac.getProperty("ttPOS_MenuPLUChoice");
        if (o instanceof SoapObject) {
            SoapObject rows = (SoapObject) o;
            int count = rows.getPropertyCount();
            int curLevel = -1;
            Menu curMenu = null;
            for (int i = 0; i < count; i++) {
                Option c = new Option((SoapObject) rows.getProperty(i));
                if (c.level != curLevel) {
                    curLevel = c.level;
                    curMenu = new Menu(c);
                    menus.add(curMenu);
                }
                curMenu.options.add(c);
            }

            // sort in reverse, because the last menu will pop up over the
            // previous ones
            Collections.sort(menus, new Comparator<Menu>() {
                @Override
                public int compare(Menu o1, Menu o2) {
                    return o2.level - o1.level;
                }
            });
        }
    }

    /**
     * Get the list of menus.
     * 
     * @return
     */
    public List<Menu> getMenus() {
        return menus;
    }

    /**
     * Returns true if this regards an arrangement (i.e. a 3-course menu).
     * 
     * @return
     */
    public boolean isArrangementType() {
        return type;
    }

    /**
     * Returns true if this regards preparation options (i.e.
     * rare/medium/well-done).
     * 
     * @return
     */
    public boolean isPreparationType() {
        return !type;
    }

    /**
     * Get the description.
     * 
     * @return
     */
    public String getDescription() {
        return description;
    }
}
