package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

public class ReservationDetailResponse {

    private final List<ReservationDetailRow> rows;

    ReservationDetailResponse(SoapObject o) {
        this.rows = new ArrayList<ReservationDetailRow>();

        try {
            SoapObject rows = (SoapObject) o.getProperty(1);
            int count = rows.getPropertyCount();
            for (int i = 0; i < count; i++) {
                ReservationDetailRow p = new ReservationDetailRow((SoapObject) rows.getProperty(i));
                this.rows.add(p);
            }
        } catch (Throwable t) {
            t.printStackTrace();
            // no rows
        }
    }

    /**
     * Get all rows
     * 
     * @return
     */
    public List<ReservationDetailRow> getRows() {
        return rows;
    }

    /**
     * Rows, filtered to only return type 3 and 5
     * 
     * @return
     */
    public List<ReservationDetailRow> getRowsFiltered() {
        List<ReservationDetailRow> result = new ArrayList<ReservationDetailRow>();
        for (ReservationDetailRow r : rows) {
            if (r.getType() == 3 || r.getType() == 5) {
                result.add(r);
            }
        }
        return result;
    }
}