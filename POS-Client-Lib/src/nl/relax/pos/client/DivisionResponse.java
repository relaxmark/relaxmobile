package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

/**
 * The Divisions
 * 
 * @author erik
 */
public class DivisionResponse {

    private final Map<Integer, Division> divisionMap;
    private final List<Division> divisions;

    DivisionResponse(SoapObject o) {
        this.divisions = new ArrayList<Division>();
        this.divisionMap = new HashMap<Integer, Division>();

        SoapObject rows = (SoapObject) o.getProperty(1);
        int count = rows.getPropertyCount();

        for (int i = 0; i < count; i++) {
            Division d = new Division((SoapObject) rows.getProperty(i));
            getDivisions().add(d);
            divisionMap.put(d.id, d);
        }
    }

    /**
     * Get the Division by ID
     * 
     * @param id
     * @return
     */
    public Division getByID(int id) {
        return divisionMap.get(id);
    }

    /**
     * Get the index of a Division by ID
     * 
     * @param id
     * @return
     */
    public int indexOf(int id) {
        return getDivisions().indexOf(getByID(id));
    }

    /**
     * Get the division
     * 
     * @return
     */
    public List<Division> getDivisions() {
        return divisions;
    }
}
