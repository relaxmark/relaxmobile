package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * An article group, for example "Deserts" or "Soft Drinks"
 * 
 * @author erik
 */
public class ArticleGroup {

    /**
     * The ID
     */
    public final int id;

    /**
     * The group number. Use this number to link articles to this group.
     */
    private final int groupNumber;

    /**
     * The name
     */
    private final String name;

    /**
     * The order
     */
    private final int order;

    /**
     * The RGB values can be used to display the article with a color. Each
     * color component (red, green, blue) can have value 0-255
     */
    private final int red, green, blue;

    ArticleGroup(SoapObject o) {
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.groupNumber = Integer.parseInt(o.getPropertyAsString("intGroupNumber"));
        this.name = o.getPropertyAsString("strName");
        this.order = Integer.parseInt(o.getPropertyAsString("intOrder"));
        this.red = Integer.parseInt(o.getPropertyAsString("intRed"));
        this.green = Integer.parseInt(o.getPropertyAsString("intGreen"));
        this.blue = Integer.parseInt(o.getPropertyAsString("intBlue"));
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof ArticleGroup) {
            ArticleGroup ag = (ArticleGroup) other;
            return ag.getGroupNumber() == getGroupNumber();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (getName() + id + getGroupNumber()).hashCode();
    }

    /**
     * The article group number. Use this number to link the article with the
     * article group.
     * 
     * @return The article group number
     */
    public int getGroupNumber() {
        return groupNumber;
    }

    /**
     * The name of the article group
     * 
     * @return The name of the article group
     */
    public String getName() {
        return name;
    }

    /**
     * A sequence number that can be used to sort the article groups for
     * displaying purposes
     * 
     * @return
     */
    public int getOrder() {
        return order;
    }

    /**
     * The RGB values can be used to display the article with a color.
     * 
     * @return The red value (0-255)
     */
    public int getRed() {
        return red;
    }

    /**
     * The RGB values can be used to display the article with a color.
     * 
     * @return The green value (0-255)
     */
    public int getGreen() {
        return green;
    }

    /**
     * The RGB values can be used to display the article with a color.
     * 
     * @return The blue value (0-255)
     */
    public int getBlue() {
        return blue;
    }
}
