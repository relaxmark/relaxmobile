package nl.relax.pos.client;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.ksoap2.serialization.SoapObject;

/**
 * An Activity that can be booked from a (SPA) Hotel-room. An Activity is for
 * example a 'Swedish Massage'.
 * 
 * @author erik
 */
public class Activity extends Talk2MeBaseType {

    /**
     * The ID
     */
    public final int id;

    private final String name;

    final int idGroup;

    private final int price;

    private ActivityGroup group;

    private final String description;
    private final int idIcon;

    private final String codeGroup;

    private final String code;

    private static final DecimalFormat FORMAT;
    static {
        FORMAT = new DecimalFormat("###0.00");
        FORMAT.setDecimalSeparatorAlwaysShown(true);
        FORMAT.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.FRANCE));
    }

    Activity(SoapObject o) {
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.name = o.getPropertyAsString("strNameFull");
        this.description = checkNIL(o.getProperty("strAdditionalInfo"));
        this.idGroup = Integer.parseInt(o.getPropertyAsString("ID_RES_AKT_GRP"));
        this.codeGroup = checkNIL(o.getProperty("akt_grp_kd_"));
        this.code = checkNIL(o.getProperty("akt_kd_"));
        this.price = (int) (Double.parseDouble(o.getPropertyAsString("dblPrice")) * 100);
        this.idIcon = Integer.parseInt(o.getPropertyAsString("ID_SYS_Icon"));
    }

    void setGroup(ActivityGroup g) {
        this.group = g;
    }

    /**
     * Get the ActivityGroup
     * 
     * @return
     */
    public ActivityGroup getActivityGroup() {
        return group;
    }

    /**
     * Get the name of the Activity
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get the long description
     * 
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * Get the price in cents (i.e. 2.52 EURO is 252 cents)
     * 
     * @return
     */
    public int getPriceCents() {
        return price;
    }

    /**
     * Get the Icon ID
     * 
     * @return
     */
    public int getIdIcon() {
        return idIcon;
    }

    public String getCodeGroup() {
        return codeGroup;
    }

    public String getCode() {
        return code;
    }

    /**
     * Get the price formatted to a String, for display purposes
     * 
     * @return
     */
    public String getPriceFormatted() {
        return FORMAT.format(price / 100d);
    }

    @Override
    public String toString() {
        return name;
    }
}
