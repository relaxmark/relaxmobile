package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class Setting extends Talk2MeBaseType {

    public static enum Key {
        // @formatter:off
        INTEGER_5_TAG_IDENTIFICATION(5, "intTAGIdentification"), 
        INTEGER_411_MAX_PROPOSAL_TIMES(411, "intTimesProposal"), 
        INTEGER_412_MAX_PROPOSALS(412, "intProposal"), 
        STRING_570_REFERENCE_TSPA(570, "strReferencetSPA");
        // @formatter:on

        public final int id;
        public final String key;

        Key(int id, String key) {
            this.id = id;
            this.key = key;
        }
    }

    public static enum Type {
        // @formatter:off
        STRING, 
        BOOLEAN, 
        INTEGER, 
        UNKNOWN;
        // @formatter:on

        static Type find(String type) {
            for (Type t : values()) {
                if (t.name().equalsIgnoreCase(type)) {
                    return t;
                }
            }
            return UNKNOWN;
        }
    }

    public final int id;
    final String key, value;
    public final Type type;

    Setting(SoapObject o) {
        this.id = Integer.parseInt(o.getProperty("ID").toString());
        this.key = checkNIL(o.getProperty("strKey").toString());
        this.value = o.getProperty("strValue") == null ? "" : checkNIL(o.getProperty("strValue").toString());
        this.type = Type.find(checkNIL(o.getProperty("strType").toString()));
    }

    public String getString() {
        return value;
    }

    public int getInteger() {
        return Integer.parseInt(value);
    }

    public boolean getBoolean() {
        return Boolean.parseBoolean(value);
    }

    @Override
    public String toString() {
        return value;
    }
}
