package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

/**
 * A group of Activities that can be booked from a SPA Hotel room. A group is
 * for example "Massages".
 * 
 * @author erik
 */
public class ActivityGroup {

    /**
     * The ID
     */
    public final int id;

    private final String name;

    private final List<Activity> activities;

    ActivityGroup(SoapObject o) {
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.name = o.getPropertyAsString("strName");
        this.activities = new ArrayList<Activity>();
    }

    void addActivity(Activity a) {
        activities.add(a);
        a.setGroup(this);
    }

    public List<Activity> getActivities() {
        return activities;
    }

    /**
     * Get the name of the group
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
