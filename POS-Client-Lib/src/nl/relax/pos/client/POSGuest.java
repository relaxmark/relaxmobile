package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * A Guest at a Table.
 * 
 * @author erik
 */
public class POSGuest implements Guest {

    public static final POSGuest UNKNOWN = null;

    /**
     * The ID
     */
    public final int id;

    /**
     * The ID of the Table where the guest is seated
     */
    private final int idTable;

    /**
     * The name of the Guest
     */
    private final String name;

    /**
     * The NFC tag of the guest
     */
    @SuppressWarnings("unused")
    private final int idTag;

    /**
     * The NFC tag of the guest
     */
    private final String tagCode;

    /**
     * The NFC tag of the guest
     */
    private final String tagIdentification;

    private final int id_RES_RES_KOP;

    POSGuest(SoapObject o) {
        this.id = Integer.parseInt(o.getProperty("ID").toString());
        this.id_RES_RES_KOP = Integer.parseInt(o.getProperty("ID_RES_RES_KOP").toString());
        this.name = o.getProperty("strName").toString();
        this.idTable = Integer.parseInt(o.getProperty("ID_POS_Table").toString());
        this.idTag = Integer.parseInt(o.getProperty("ID_RES_TAG").toString());
        this.tagCode = o.getProperty("strTagCode").toString();
        this.tagIdentification = o.getProperty("strTagIdentification").toString();
    }

    public POSGuest(int p_ID_RES_RES_KOP, int p_ID_RES_RES_DLN, String name, String tagId, String code) {
        this.id = p_ID_RES_RES_DLN;
        this.idTable = 0;
        this.idTag = 0;
        this.tagCode = code;
        this.tagIdentification = tagId;
        this.name = name;
        this.id_RES_RES_KOP = p_ID_RES_RES_KOP;
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * The ID of the table where this guest is seated
     * 
     * @return
     */
    public int getIdTable() {
        return idTable;
    }

    /**
     * The name
     * 
     * @return
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * The code of the NFC Tag that is associated with this Guest
     * 
     * @return
     */
    @Override
    public String getTagCode() {
        return tagCode;
    }

    /**
     * The ID of the NFC Tag that is associated with this Guest
     * 
     * @return
     */
    @Override
    public String getTagIdentification() {
        return tagIdentification;
    }

    @Override
    public int getId_RES_RES_KOP() {
        return id_RES_RES_KOP;
    }

    @Override
    public int getId_RES_RES_DLN() {
        return id;
    }
}
