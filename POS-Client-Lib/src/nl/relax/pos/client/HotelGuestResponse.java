package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

/**
 * This class holds all guests (HotelGuestInfo) that are booked per date. For
 * example 2 guests might be registered in the Hotel room on one day, but only 1
 * guest on the next day.
 * 
 * @author erik
 */
class HotelGuestResponse {

    private final Map<Talk2MeDate, List<HotelGuest>> guests;
    private final Map<Integer, HotelGuest> allGuests;

    HotelGuestResponse(SoapObject o) {
        this.guests = new LinkedHashMap<Talk2MeDate, List<HotelGuest>>();
        this.allGuests = new HashMap<Integer, HotelGuest>();
        if (o.getProperty(1) instanceof SoapObject) {
            SoapObject rows = (SoapObject) o.getProperty(1);
            int count = rows.getPropertyCount();
            for (int i = 0; i < count; i++) {
                HotelGuest g = new HotelGuest((SoapObject) rows.getProperty(i));
                add(g);
            }
            System.out.println("guests=" + count);
        }
    }

    private void add(HotelGuest g) {
        List<HotelGuest> list = guests.get(g.getReservationDate());
        if (list == null) {
            list = new ArrayList<HotelGuest>();
            guests.put(g.getReservationDate(), list);
        }
        list.add(g);
        allGuests.put(g.id, g);
    }

    /**
     * Get the guests that are booked on a certain date.
     * 
     * @param date
     *            The date
     * @return The list of guests. If no guest is found for this date, an empty
     *         list is returned.
     */
    List<HotelGuest> getHotelGuestInfo(Talk2MeDate date) {
        List<HotelGuest> result = new ArrayList<HotelGuest>();
        List<HotelGuest> list = guests.get(date);
        if (list != null) {
            for (HotelGuest hotelGuestInfo : list) {
                result.add(hotelGuestInfo);
            }
        }
        return result;
    }

    /**
     * Get the (sorted) collection of dates that there are guests in the room.
     * 
     * @return
     */
    List<Talk2MeDate> getBookedDates() {
        List<Talk2MeDate> dates = new ArrayList<Talk2MeDate>();
        dates.addAll(guests.keySet());

        Collections.sort(dates, new Comparator<Talk2MeDate>() {
            @Override
            public int compare(Talk2MeDate o1, Talk2MeDate o2) {
                return (int) (o1.getCalendar().getTimeInMillis() - o2.getCalendar().getTimeInMillis());
            }
        });

        return dates;
    }

    public HotelGuest getGuest(int id) {
        return allGuests.get(id);
    }
}
