package nl.relax.pos.client;

abstract class Talk2MeBaseType {

    public static final String NIL = "anyType{}";

    protected String checkNIL(String s) {
        return NIL.equals(s) ? "" : s;
    }

    protected String checkNIL(Object o) {
        if (o == null) {
            return "";
        }
        return checkNIL(o.toString());
    }

    protected String checkNILTime(Object o) {
        String s = checkNIL(o);
        if (s.length() == 4) {
            s = s.substring(0, 2) + ":" + s.substring(2);
        }
        return s;
    }

    protected String[] toArray(String s) {
        return checkNIL(s).split(",");
    }

    protected String fromArray(String[] s) {
        boolean first = true;
        StringBuilder sb = new StringBuilder("");
        for (String string : s) {
            if (!first) {
                sb.append(',');
            }
            sb.append(string);
        }
        return sb.toString();
    }

    protected int fromBoolean(boolean b) {
        return b ? -1 : 0;
    }
}
