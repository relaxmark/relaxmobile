package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * The License info. This contains information about the license of the revenue
 * that is using POS, such as the name of the licensee, the language and the
 * used currency.
 * 
 * @author erik
 */
public class License {

    /**
     * 
     */
    private final String idSysAdm;

    /**
     * 
     */
    private final String name;

    /**
     * The Language ID
     */
    private final int idLanguage;

    /**
     * The currency ID
     */
    private final int idCurrency;

    /**
     * For example EUR or DLR
     */
    private final String currencyCode;

    /**
     * For example Euro or DOLLAR
     */
    private final String currencyName;

    /**
     * For example € or $
     */
    private final String currencySymbol;

    License(SoapObject o) {
        this.idSysAdm = o.getProperty("p_ID_SYS_ADM").toString();
        this.name = o.getProperty("p_strLicensedName").toString();
        this.idLanguage = Integer.parseInt(o.getProperty("p_ID_SYS_Language").toString());
        this.idCurrency = Integer.parseInt(o.getProperty("p_ID_FIN_Currency").toString());
        this.currencyCode = o.getProperty("p_strCurrencyAbbreviation").toString();
        this.currencyName = o.getProperty("p_strCurrencyName").toString();
        this.currencySymbol = o.getProperty("p_strCurrencySymbol").toString();
    }

    /**
     * 
     * @return
     */
    public String getIdSysAdm() {
        return idSysAdm;
    }

    /**
     * Get the name
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get the language ID
     * 
     * @return
     */
    public int getIdLanguage() {
        return idLanguage;
    }

    /**
     * Get the currency ID
     * 
     * @return
     */
    public int getIdCurrency() {
        return idCurrency;
    }

    /**
     * Get the currency code (for example EUR or USD)
     * 
     * @return
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Get the currency name (for example Euro or Dollar)
     * 
     * @return
     */
    public String getCurrencyName() {
        return currencyName;
    }

    /**
     * Get the currency symbol (for example € or $)
     * 
     * @return
     */
    public String getCurrencySymbol() {
        return currencySymbol;
    }
}
