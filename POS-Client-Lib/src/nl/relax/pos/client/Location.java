package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * A LayoutRow represents a location (i.e. Restaurant, Bar etc). This
 * corresponds with Soap type "ttPOS_LayoutRow" and is retrieved with Soap
 * request "getPOS_Layout"
 * 
 * @author erik
 */
public class Location {

    public final int id;
    @SuppressWarnings("unused")
    private final String code;
    private final String name;
    private int numberOfTables;
    private int numberOfSeats;
    private int numberOfTableOccupied;

    /**
     * 
     */
    private final boolean guestRequired;

    /**
     * Constructor
     * 
     * @param o
     */
    Location(SoapObject o) {
        this.id = Integer.parseInt(o.getProperty("ID").toString());
        this.code = o.getProperty("strCode").toString();
        this.name = o.getProperty("strName").toString();
        this.numberOfTables = Integer.parseInt(o.getPropertyAsString("intNumberOfTables").toString());
        this.numberOfSeats = Integer.parseInt(o.getPropertyAsString("intNumberOfSeats").toString());
        this.guestRequired = !o.getProperty("blnGuestRequired").toString().equals("0");
        this.numberOfTableOccupied = Integer.parseInt(o.getPropertyAsString("intTablesOccupied").toString());
    }

    @Override
    public String toString() {
        return getName() + " (" + numberOfTableOccupied + "/" + numberOfTables + ")";
    }

    @Override
    public boolean equals(Object other) {
        return other != null && other instanceof Location && id == ((Location) other).id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    /**
     * Refresh the dynamic data with freshly loaded data.
     * 
     * @param fresh
     */
    public void refresh(Location fresh) {
        this.numberOfSeats = fresh.numberOfSeats;
        this.numberOfTableOccupied = fresh.numberOfTableOccupied;
        this.numberOfTables = fresh.numberOfTables;
    }

    /**
     * Get the name
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * The number of tables
     * 
     * @return
     */
    public int getNumberOfTables() {
        return numberOfTables;
    }

    /**
     * The number of seats
     * 
     * @return
     */
    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    /**
     * If <code>true</code> then you can only order if there is a Guest
     * registered at a table, otherwise anonymous ordering is possible.
     * 
     * @return
     */
    public boolean isGuestRequired() {
        return guestRequired;
    }
}
