package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class ProposalRow extends Talk2MeBaseType {

    public final int id;
    public final SoapObject soap;
    private final Talk2MeDate date;
    private final int proposal;
    private final int order;
    private final String activityCode;
    private final String namePrimary;
    private final String nameSecundary;
    private final String nameFull;
    private final String timeBegin, timeEnd;
    private final int guest;
    private final int object;
    private final String additionalInfo;
    private final double price;
    private final boolean initialRecord;
    private final boolean selected;
    private String[] guestIds;
    private final int idResOnline;
    private final int availableEmployees;
    private final String[] availableEmployeesIds;

    public ProposalRow(SoapObject o) {
        this.soap = o;
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.date = new Talk2MeDate(o.getPropertyAsString("datProposal"));
        this.proposal = Integer.parseInt(o.getPropertyAsString("intProposal"));
        this.order = Integer.parseInt(o.getPropertyAsString("intOrder"));
        this.activityCode = checkNIL(o.getProperty("akt_kd_"));
        this.namePrimary = checkNIL(o.getProperty("strNamePrimary"));
        this.nameSecundary = checkNIL(o.getProperty("strNameSecundary"));
        this.nameFull = checkNIL(o.getProperty("strNameFull"));
        this.timeBegin = checkNIL(o.getProperty("tmc_beg"));
        this.timeEnd = checkNIL(o.getProperty("tmc_end"));
        this.guest = Integer.parseInt(o.getPropertyAsString("intGuest"));
        this.object = Integer.parseInt(o.getPropertyAsString("intObject"));
        this.additionalInfo = checkNIL(o.getProperty("strAdditionalInfo"));
        this.price = Double.parseDouble(checkNIL(o.getProperty("dblPrice")));
        this.initialRecord = !checkNIL(o.getProperty("blnInitialRecord")).equals("0");
        this.selected = !checkNIL(o.getProperty("blnSelected")).equals("0");
        this.guestIds = checkNIL(o.getProperty("lstGuestID")).split(",");
        this.idResOnline = Integer.parseInt(checkNIL(o.getProperty("ID_RES_ONL_INE")));
        this.availableEmployees = Integer.parseInt(checkNIL(o.getProperty("intAvailableEmployee")));
        this.availableEmployeesIds = checkNIL(o.getProperty("lstID_AvailableEmployee")).split(",");
    }

    public void setGuestIds(String ids) {
        this.guestIds = ids.split(",");
    }

    public SoapObject createSoapObject() {
        System.out.println("ProposalRow createSoapObject " + soap.getNamespace() + " " + soap.getName());
        SoapObject o = new SoapObject(soap.getNamespace(), soap.getName());
        o.addProperty("ID", id);
        o.addProperty("datProposal", date.getValue());
        o.addProperty("intProposal", proposal);
        o.addProperty("intOrder", order);
        o.addProperty("akt_kd_", activityCode);
        o.addProperty("strNamePrimary", namePrimary);
        o.addProperty("strNameSecundary", nameSecundary);
        o.addProperty("strNameFull", nameFull);
        o.addProperty("tmc_beg", timeBegin);
        o.addProperty("tmc_end", timeEnd);
        o.addProperty("intGuest", guest);
        o.addProperty("intObject", object);
        o.addProperty("strAdditionalInfo", additionalInfo);
        o.addProperty("dblPrice", price);
        o.addProperty("blnInitialRecord", fromBoolean(initialRecord));
        o.addProperty("blnSelected", fromBoolean(selected));
        o.addProperty("lstGuestID", fromArray(guestIds));
        o.addProperty("ID_RES_ONL_INE", idResOnline);
        o.addProperty("intAvailableEmployee", availableEmployees);
        o.addProperty("lstID_AvailableEmployee", fromArray(availableEmployeesIds));
        return o;
    }

    public SoapObject createSoapObject(boolean selected) {
        System.out.println("ProposalRow createSoapObject " + soap.getNamespace() + " " + soap.getName());
        SoapObject o = new SoapObject(soap.getNamespace(), soap.getName());
        o.addProperty("ID", id);
        o.addProperty("datProposal", date.getValue());
        o.addProperty("intProposal", proposal);
        o.addProperty("intOrder", order);
        o.addProperty("akt_kd_", activityCode);
        o.addProperty("strNamePrimary", namePrimary);
        o.addProperty("strNameSecundary", nameSecundary);
        o.addProperty("strNameFull", nameFull);
        o.addProperty("tmc_beg", timeBegin);
        o.addProperty("tmc_end", timeEnd);
        o.addProperty("intGuest", guest);
        o.addProperty("intObject", object);
        o.addProperty("strAdditionalInfo", additionalInfo);
        o.addProperty("dblPrice", price);
        o.addProperty("blnInitialRecord", fromBoolean(initialRecord));
        o.addProperty("blnSelected", fromBoolean(selected));
        o.addProperty("lstGuestID", fromArray(guestIds));
        o.addProperty("ID_RES_ONL_INE", idResOnline);
        o.addProperty("intAvailableEmployee", availableEmployees);
        o.addProperty("lstID_AvailableEmployee", fromArray(availableEmployeesIds));
        return o;
    }

    public Talk2MeDate getDate() {
        return date;
    }

    public int getOrder() {
        return order;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public String getNamePrimary() {
        return namePrimary;
    }

    public String getNameSecundary() {
        return nameSecundary;
    }

    public String getNameFull() {
        return nameFull;
    }

    public String getTimeBegin() {
        return timeBegin;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public double getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }

    public int getGuest() {
        return guest;
    }

    public int getObject() {
        return object;
    }

    public boolean isInitialRecord() {
        return initialRecord;
    }

    public boolean isSelected() {
        return selected;
    }

    public String[] getGuestIds() {
        return guestIds;
    }

    public int getIdResOnline() {
        return idResOnline;
    }

    public int getAvailableEmployees() {
        return availableEmployees;
    }

    public String[] getAvailableEmployeesIds() {
        return availableEmployeesIds;
    }

    @Override
    public String toString() {
        return timeBegin + " - " + timeEnd;
    }

    /**
     * @return the proposal
     */
    public int getProposal() {
        return proposal;
    }
}
