package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class Note extends Talk2MeBaseType {

    public static final int PRIORITY_NONE = 0;
    public static final int PRIORITY_LOW = 1;
    public static final int PRIORITY_NORMAL = 2;
    public static final int PRIORITY_HIGH = 3;

    public final int id;
    private final Talk2MeDate date;
    private final int noteNr;
    private final int userIdFrom;
    private final String userNameFrom;
    private final String note;
    private final String subject;
    private final int priority;

    // I/System.out(28094): <ID>589</ID>
    // I/System.out(28094): <datNote>2013-06-12</datNote>
    // I/System.out(28094): <intNote>80457</intNote>
    // I/System.out(28094): <ID_SYS_UserFrom>9</ID_SYS_UserFrom>
    // I/System.out(28094): <strUserNameFrom>Systeembeheerder</strUserNameFrom>
    // I/System.out(28094): <ID_SYS_UserTo>209</ID_SYS_UserTo>
    // I/System.out(28094): <strUserNameTo>Erik Duijs</strUserNameTo>
    // I/System.out(28094): <ID_REL_Relation>216</ID_REL_Relation>
    // I/System.out(28094): <strRelationName></strRelationName>
    // I/System.out(28094): <ID_RES_RES_KOP>0</ID_RES_RES_KOP>
    // I/System.out(28094): <res_nr_>0</res_nr_>
    // I/System.out(28094): <ID_RES_RES_REG>0</ID_RES_RES_REG>
    // I/System.out(28094): <ID_RES_AKT>0</ID_RES_AKT>
    // I/System.out(28094): <aaa_kd_></aaa_kd_>
    // I/System.out(28094): <strActivityNamePrimary></strActivityNamePrimary>
    // I/System.out(28094):
    // <strActivityNameSecundary></strActivityNameSecundary>
    // I/System.out(28094): <strActivityNameFull></strActivityNameFull>
    // I/System.out(28094): <intActivityTime>0</intActivityTime>
    // I/System.out(28094): <strActivityTime></strActivityTime>
    // I/System.out(28094): <strSubject>De eerste</strSubject>
    // I/System.out(28094): <strNote>en/./ en.. en..</strNote>
    // I/System.out(28094): <ID_NOT_NoteKind>2</ID_NOT_NoteKind>
    // I/System.out(28094): <strNOT_NoteKind>Behandelbericht</strNOT_NoteKind>
    // I/System.out(28094): <ID_SYS_Priority>0</ID_SYS_Priority>
    // I/System.out(28094): <strSYS_Priority></strSYS_Priority>
    // I/System.out(28094): <ID_SYS_Status>0</ID_SYS_Status>
    // I/System.out(28094): <strSYS_Status></strSYS_Status>

    Note(SoapObject row) {
        super();
        this.id = Integer.parseInt(row.getPropertySafelyAsString("ID"));
        this.noteNr = Integer.parseInt(row.getPropertySafelyAsString("intNote"));
        this.date = new Talk2MeDate(row.getPropertySafelyAsString("datNote"), noteNr);
        this.userIdFrom = Integer.parseInt(row.getPropertySafelyAsString("ID_SYS_UserFrom"));
        this.userNameFrom = row.getPropertySafelyAsString("strUserNameFrom");
        this.subject = row.getPropertySafelyAsString("strSubject");
        this.note = row.getPropertySafelyAsString("strNote");
        this.priority = Integer.parseInt(row.getPropertySafelyAsString("ID_SYS_Priority"));
    }

    public Talk2MeDate getDateTime() {
        return date;
    }

    public int getNoteTime() {
        return noteNr;
    }

    public int getUserIdFrom() {
        return userIdFrom;
    }

    public String getUserNameFrom() {
        return userNameFrom;
    }

    public String getSubject() {
        return subject;
    }

    public String getNote() {
        return note;
    }

    public int getPriority() {
        return priority;
    }
}