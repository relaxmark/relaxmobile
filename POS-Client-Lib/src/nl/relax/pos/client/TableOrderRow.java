package nl.relax.pos.client;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.ksoap2.serialization.SoapObject;

/**
 * An Order row
 * 
 * @author erik
 */
public class TableOrderRow extends Talk2MeBaseType {

    // TODO: find a better solution for the initial id
    private static int genChoiceGroupId = (int) (System.currentTimeMillis() & 0x7fffff);

    /**
     * Generate a unique ID to link articles + choices (i.e. Steak +
     * 'well-done')
     * 
     * @return
     */
    static int generateChoiceGroupId() {
        genChoiceGroupId++;
        return genChoiceGroupId;
    }

    static final DecimalFormat FORMAT;
    static {
        FORMAT = new DecimalFormat("###0.00");
        FORMAT.setDecimalSeparatorAlwaysShown(true);
        FORMAT.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.FRANCE));
    }

    public final int id;
    private final int idLocation;
    private final int idTable;
    private int orderSession;
    private int orderSequence;

    private int ID_RES_RES_KOP;

    private int idGuest;
    private int processNumber;
    private final int idArticle;
    private final String articleName;
    private int quantity;
    private final int articlePrice;
    private final String datOrdered;
    private int intOrdered;
    private int deleted;
    private String remark;
    private int idTableOrder;
    private int idCourse;
    private String nameCourse;
    private int printed;
    private int idTicket;
    private int ticketNumber;

    private int idResArticle;
    private final int idArticleParent;
    private final int idChoiceGroup;
    private int idIcon;

    private final int level;

    /**
     * A new TableOrderRow created from a PackageItem
     * 
     * @param ID_POS_Layout
     * @param ID_POS_Table
     * @param item
     */
    public TableOrderRow(int ID_POS_Layout, int ID_POS_Table, PackageItem item) {
        this.id = 0;
        this.level = 0;
        this.idLocation = ID_POS_Layout;
        this.idTable = ID_POS_Table;
        this.idArticle = item.getIdArticle();
        this.idArticleParent = 0;
        this.articleName = item.getArticleName();
        this.setQuantity(item.getQuantity() == 0 ? 1 : item.getQuantity());
        this.idCourse = 0;
        this.nameCourse = "";
        this.idResArticle = item.id;
        this.articlePrice = 0;
        this.datOrdered = "";
        this.remark = "";
        this.idChoiceGroup = generateChoiceGroupId();
    }

    /**
     * A new TableOrderRow created by the user
     * 
     * @param ID_POS_Layout
     * @param ID_POS_Table
     * @param idArticle
     * @param articleName
     * @param quantity
     * @param price
     */
    public TableOrderRow(int ID_POS_Layout, int ID_POS_Table, int idArticle, String articleName, int quantity, int price, Course course) {
        this.id = 0;
        this.level = 0;
        this.idLocation = ID_POS_Layout;
        this.idTable = ID_POS_Table;
        this.idArticle = idArticle;
        this.setQuantity(quantity);
        this.articlePrice = price;
        this.remark = "";
        this.articleName = articleName;
        this.datOrdered = "";
        this.idArticleParent = 0;
        this.idChoiceGroup = generateChoiceGroupId();
        if (course == null) {
            this.idCourse = 0;
            this.nameCourse = "";
        } else {
            this.idCourse = course.id;
            this.nameCourse = course.getName();
        }
    }

    /**
     * Create a new (unordered) row from another TableOrderRow
     * 
     * @param tor
     */
    public TableOrderRow(TableOrderRow tor) {
        this.id = 0;
        this.level = tor.level;
        this.idLocation = tor.idLocation;
        this.idTable = tor.idTable;
        this.idArticle = tor.idArticle;
        this.setQuantity(tor.quantity);
        this.articlePrice = tor.articlePrice;
        this.remark = tor.remark;
        this.articleName = tor.articleName;
        this.datOrdered = "";
        this.idArticleParent = tor.idArticleParent;
        this.idChoiceGroup = tor.idChoiceGroup;
        this.idIcon = tor.idIcon;
        this.printed = 0;
        this.remark = "*";// "*" means "print again"
        this.idCourse = tor.idCourse;
        this.nameCourse = tor.nameCourse;
    }

    /**
     * A new TableOrderRow created by the user
     * 
     * @param ID_POS_Layout
     * @param ID_POS_Table
     * @param idArticle
     * @param articleName
     * @param quantity
     * @param price
     */
    public TableOrderRow(int ID_POS_Layout, int ID_POS_Table, int idArticle, String articleName, int quantity, int price, int idCourse, int iconId) {
        this.id = 0;
        this.level = 0;
        this.idLocation = ID_POS_Layout;
        this.idTable = ID_POS_Table;
        this.idArticle = idArticle;
        this.setQuantity(quantity);
        this.articlePrice = price;
        this.remark = "";
        this.articleName = articleName;
        this.datOrdered = "";
        this.idArticleParent = 0;
        this.idChoiceGroup = generateChoiceGroupId();
        this.idIcon = iconId;
        this.idCourse = idCourse;
    }

    /**
     * A new TableOrderRow created by the user. Use this constructor for article
     * choices
     * 
     * @param ID_POS_Layout
     * @param ID_POS_Table
     * @param idArticle
     * @param articleName
     * @param quantity
     * @param price
     * @param parent
     *            The parent is the
     */
    public TableOrderRow(int ID_POS_Layout, int ID_POS_Table, int idArticle, String articleName, int quantity, int price, int idCourse, TableOrderRow parent,
            int level) {
        this.id = 0;
        this.level = level;
        this.idLocation = ID_POS_Layout;
        this.idTable = ID_POS_Table;
        this.idArticle = idArticle;
        this.setQuantity(quantity);
        this.articlePrice = price;
        this.remark = "";
        this.articleName = articleName;
        this.datOrdered = "";
        this.idArticleParent = parent.getIdArticle();
        this.idChoiceGroup = parent.getIdChoiceGroup();
        this.idCourse = idCourse;

        parent.printed = 0;
        parent.remark = "*";// "*" means "print again"
    }

    /**
     * A new TableOrderRow as returned from the server object
     * 
     * @param o
     */
    TableOrderRow(SoapObject o) {
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.idLocation = Integer.parseInt(o.getPropertyAsString("ID_POS_Layout"));
        this.idTable = Integer.parseInt(o.getPropertyAsString("ID_POS_Table"));
        this.level = Integer.parseInt(o.getPropertyAsString("intLevel"));
        this.setOrderSession(Integer.parseInt(o.getPropertyAsString("intOrderSession")));
        this.setOrderSequence(Integer.parseInt(o.getPropertyAsString("intOrderSequence")));
        this.ID_RES_RES_KOP = Integer.parseInt(o.getPropertyAsString("ID_RES_RES_KOP"));
        this.setIdGuest(Integer.parseInt(o.getPropertyAsString("ID_RES_RES_DLN")));
        this.processNumber = Integer.parseInt(o.getPropertyAsString("intProcessNumber"));

        this.idArticle = Integer.parseInt(o.getPropertyAsString("ID_RES_PLU"));
        this.idArticleParent = Integer.parseInt(o.getPropertyAsString("ID_RES_MainPLU"));
        this.idChoiceGroup = Integer.parseInt(o.getPropertyAsString("intGroup"));
        this.idResArticle = Integer.parseInt(o.getPropertyAsString("ID_RES_RES_PLU"));

        this.articleName = o.getPropertyAsString("strPLU");

        this.setQuantity(Integer.parseInt(o.getPropertyAsString("intQuantity")));
        this.articlePrice = Integer.parseInt(o.getPropertyAsString("intdblPLUPrice"));

        this.datOrdered = o.getPropertyAsString("strdatOrdered");
        this.setOrdered(Integer.parseInt(o.getPropertyAsString("intOrdered")));
        this.setDeleted(Integer.parseInt(o.getPropertyAsString("blnDeleted")));
        this.remark = checkNIL(o.getPropertyAsString("strRemark"));

        this.setIdTableOrder(Integer.parseInt(o.getPropertyAsString("ID_POS_TableOrder")));
        this.setIdCourse(Integer.parseInt(o.getPropertyAsString("ID_POS_Course")));
        this.setPrinted(Integer.parseInt(o.getPropertyAsString("blnPrinted")));
        this.setIdTicket(Integer.parseInt(o.getPropertyAsString("ID_POS_Ticket")));
        this.setTicketNumber(Integer.parseInt(o.getPropertyAsString("intTicketNumber")));
        this.idIcon = Integer.parseInt(o.getPropertyAsString("ID_SYS_Icon"));
        this.idCourse = Integer.parseInt(o.getPropertyAsString("ID_POS_Course"));
        this.nameCourse = checkNIL(o.getProperty("strNameCourse"));
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof TableOrderRow) {
            TableOrderRow o = (TableOrderRow) other;
            return id == o.id && getDatOrdered().equals(o.getDatOrdered()) && idChoiceGroup == o.idChoiceGroup && idArticle == o.idArticle
                    && processNumber == o.processNumber && orderSession == o.orderSession;
        } else {
            return false;
        }
    }

    public int getIdIcon() {
        return idIcon;
    }

    @Override
    public int hashCode() {
        String s = id + "," + getDatOrdered() + "," + idChoiceGroup + "," + idArticle + "," + processNumber + "," + orderSession;
        return s.hashCode();
    }

    /**
     * Create a SoapObject
     */
    SoapObject createSoapObject(Talk2MeClient client) {
        SoapObject o = new SoapObject(client.namespace, "ttPOS_TableOrderRow");
        o.addProperty("ID", id);
        o.addProperty("ID_POS_Layout", getIdLocation());
        o.addProperty("ID_POS_Table", getIdTable());
        o.addProperty("intOrderSession", getOrderSession());
        o.addProperty("intOrderSequence", getOrderSequence());
        o.addProperty("ID_RES_RES_KOP", ID_RES_RES_KOP);
        o.addProperty("ID_RES_RES_DLN", getIdGuest());
        o.addProperty("intProcessNumber", processNumber);
        o.addProperty("ID_RES_PLU", getIdArticle());
        o.addProperty("ID_RES_MainPLU", getIdArticleParent());
        o.addProperty("intGroup", getIdChoiceGroup());
        o.addProperty("ID_RES_RES_PLU", idResArticle);
        o.addProperty("strPLU", getArticleName());
        o.addProperty("intQuantity", getQuantity());
        o.addProperty("intdblPLUPrice", getArticlePrice());
        o.addProperty("strdatOrdered", getDatOrdered());
        o.addProperty("intOrdered", getIntOrdered());
        o.addProperty("blnDeleted", getDeleted());
        o.addProperty("strRemark", getRemark());
        o.addProperty("ID_POS_TableOrder", getIdTableOrder());
        o.addProperty("ID_POS_Course", getIdCourse());
        o.addProperty("strNameCourse", getNameCourse());
        o.addProperty("blnPrinted", getPrinted());
        o.addProperty("ID_POS_Ticket", getIdTicket());
        o.addProperty("intTicketNumber", getTicketNumber());
        o.addProperty("ID_SYS_Icon", getIdIcon());
        o.addProperty("intLevel", level);
        o.addProperty("intOrder", 0);
        return o;
    }

    @Override
    public String toString() {
        return (idArticleParent > 0 ? "  - " : (getQuantity() + "x ")) + getArticleName() + " (" + FORMAT.format(getArticlePrice() / 100d) + ")";
    }

    /**
     * @return the idLocation
     */
    public int getIdLocation() {
        return idLocation;
    }

    /**
     * @return the idTable
     */
    public int getIdTable() {
        return idTable;
    }

    /**
     * @param orderSession
     *            the orderSession to set
     */
    public void setOrderSession(int orderSession) {
        this.orderSession = orderSession;
    }

    /**
     * @return the orderSession
     */
    public int getOrderSession() {
        return orderSession;
    }

    /**
     * @param orderSequence
     *            the orderSequence to set
     */
    public void setOrderSequence(int orderSequence) {
        this.orderSequence = orderSequence;
    }

    /**
     * @return the orderSequence
     */
    public int getOrderSequence() {
        return orderSequence;
    }

    /**
     * @param idGuest
     *            the idGuest to set
     */
    public void setIdGuest(int idGuest) {
        this.idGuest = idGuest;
    }

    /**
     * @return the idGuest
     */
    public int getIdGuest() {
        return idGuest;
    }

    /**
     * @return the idArticle
     */
    public int getIdArticle() {
        return idArticle;
    }

    /**
     * @return the articleName
     */
    public String getArticleName() {
        return articleName;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @return the articlePrice
     */
    public int getArticlePrice() {
        return articlePrice;
    }

    /**
     * @return the datOrdered
     */
    public String getDatOrdered() {
        return datOrdered;
    }

    /**
     * @param intOrdered
     *            the intOrdered to set
     */
    public void setOrdered(int intOrdered) {
        this.intOrdered = intOrdered;
    }

    /**
     * @return the intOrdered
     */
    public int getIntOrdered() {
        return intOrdered;
    }

    /**
     * @param deleted
     *            the deleted to set
     */
    public void setDeleted(int deleted) {
        this.deleted = deleted;
        if (deleted != 0) {
            setPrinted(0);
        }
    }

    /**
     * @return the deleted
     */
    public int getDeleted() {
        return deleted;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param idTableOrder
     *            the idTableOrder to set
     */
    public void setIdTableOrder(int idTableOrder) {
        this.idTableOrder = idTableOrder;
    }

    /**
     * This is the ID of the parent TableOrderRow. For example if this
     * TableOrderRow would be 'well-done', the idTableOrder would be of the
     * Steak.
     * 
     * @return the idTableOrder
     */
    public int getIdTableOrder() {
        return idTableOrder;
    }

    /**
     * @param idCourse
     *            the idCourse to set
     */
    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }

    public void setCourse(Course c) {
        setIdCourse(c.id);
        this.nameCourse = c.getName();
    }

    /**
     * @return the idCourse
     */
    public int getIdCourse() {
        return idCourse;
    }

    public String getNameCourse() {
        return nameCourse == null ? "" : nameCourse;
    }

    public String getNameCourseShort() {
        String name = getNameCourse().toUpperCase();
        if (name.length() > 3) {
            name = name.substring(0, 3);
        }
        return name;
    }

    /**
     * @param printed
     *            the printed to set
     */
    public void setPrinted(int printed) {
        this.printed = printed;
    }

    /**
     * @return the printed
     */
    public int getPrinted() {
        return printed;
    }

    /**
     * @param idTicket
     *            the idTicket to set
     */
    public void setIdTicket(int idTicket) {
        this.idTicket = idTicket;
    }

    /**
     * @return the idTicket
     */
    public int getIdTicket() {
        return idTicket;
    }

    /**
     * @param ticketNumber
     *            the ticketNumber to set
     */
    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    /**
     * @return the ticketNumber
     */
    public int getTicketNumber() {
        return ticketNumber;
    }

    public int getIdArticleParent() {
        return idArticleParent;
    }

    int getIdChoiceGroup() {
        return idChoiceGroup;
    }

    /**
     * Return the hierarchy level (main plu = 0, submenu's > 0)
     * 
     * @return
     */
    public int getLevel() {
        return level;
    }
}
