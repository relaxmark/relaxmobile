package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class ChartResponse extends Talk2MeBaseType {
    private final Chart chart;

    public ChartResponse(SoapObject o) {
        this.chart = new Chart(o);
    }

    /**
     * @return the chart
     */
    public Chart getChart() {
        return chart;
    }
}
