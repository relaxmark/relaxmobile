package nl.relax.pos.client;

import nl.relax.pos.client.util.Base64;

import org.ksoap2.serialization.SoapObject;

public class Icon {

	/**
	 * The ID
	 */
	public final int id;

	private final String name;
	private final String blob;

	private final String modifiedDate;
	private final int modified;
	private byte[] decoded;

	Icon(SoapObject o) {
		this.id = Integer.parseInt(o.getPropertyAsString("ID"));
		this.name = o.getPropertyAsString("strName");
		this.blob = o.hasProperty("blbImage") && o.getProperty("blbImage") != null ? o.getPropertyAsString("blbImage") : null;
		this.modifiedDate = o.getPropertyAsString("datModified");
		this.modified = Integer.parseInt(o.getPropertyAsString("intModified"));
	}
	
	Icon(int id, String name, String modifiedDate, int modified) {
		this.id = id;
		this.name = name;
		this.blob = null;
		this.modifiedDate = modifiedDate;
		this.modified = modified;
	}
	
	void setData(byte[] b) {
		this.decoded = b;
	}

	/**
	 * Get the name of the icon (typically the file name)
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the binary data of the image in BASE64 encoding.
	 * 
	 * @return
	 */
	public String getBlob() {
		return blob;
	}

	/**
	 * Decode the blob to a binary image
	 * 
	 * @return
	 */
	public byte[] decode() {
		if (blob != null && decoded == null) {
			return Base64.decodeFast(blob);
		} else {
			return decoded;
		}
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public int getModified() {
		return modified;
	}
}