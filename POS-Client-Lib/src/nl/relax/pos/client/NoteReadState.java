package nl.relax.pos.client;

public enum NoteReadState {
    ALL(1), READ(-1), NOT_READ(0);

    public static final String key = "p_intRead";
    public final int value;

    NoteReadState(int value) {
        this.value = value;
    }
}
