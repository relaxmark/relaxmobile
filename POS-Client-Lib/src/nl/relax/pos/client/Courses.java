package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

public class Courses {

    private final List<Course> courses = new ArrayList<Course>();
    private final Map<Integer, Course> map = new HashMap<Integer, Course>();

    Courses(SoapObject o) {
        SoapObject rows = (SoapObject) o.getProperty(1);
        int count = rows.getPropertyCount();
        for (int i = 0; i < count; i++) {
            Course s = new Course((SoapObject) rows.getProperty(i));
            map.put(s.id, s);
            courses.add(s);
        }
    }

    public Course get(int id) {
        return map.get(id);
    }

    public List<Course> getAll() {
        return courses;
    }
}
