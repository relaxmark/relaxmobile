package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * A Talk2MeException is thrown whenever a transaction using Talk2MeClient does
 * not end as expected. This can mean a functional error returned by the server,
 * a low-level SOAP fault or any other exception that happens when calling a
 * SOAP function.
 * 
 * @author erik
 */
public class Talk2MeException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 240403307651002904L;

    private static final String[] SESSION_EXPIRED_CODES = { "CheckSHA-003", "CheckSHA-004" };

    public final String code;

    Talk2MeException(SoapObject o) {
        super(o.getPropertyAsString("p_strErrorText"));
        this.code = o.getPropertyAsString("p_strErrorCode");
    }

    Talk2MeException(Exception e) {
        super(e);
        this.code = e.getMessage();
    }

    public Talk2MeException(String message) {
        super(message);
        this.code = message;
    }

    public String getCode() {
        return code;
    }

    public boolean isSessionExpired() {
        for (String e : SESSION_EXPIRED_CODES) {
            if (e.equalsIgnoreCase(code))
                return true;
        }

        return false;
    }

    @Override
    public String getMessage() {
        String m = super.getMessage();
        if (m != null) {
            int idx = m.lastIndexOf("Exception:");
            if (idx >= 0) {
                m = m.substring(idx + "Exception:".length()).trim();
            }
        }
        return m;
    }
}