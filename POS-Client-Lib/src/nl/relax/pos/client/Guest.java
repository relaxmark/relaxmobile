package nl.relax.pos.client;

public interface Guest {
    String getName();

    String getTagCode();

    String getTagIdentification();

    int getId_RES_RES_KOP();

    int getId_RES_RES_DLN();
}
