package nl.relax.pos.client;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

/**
 * Information related to a Table
 * 
 * @author erik
 */
public class Table extends Talk2MeBaseType {

    private static final DecimalFormat FORMAT;
    static {
        FORMAT = new DecimalFormat("###0.00");
        FORMAT.setDecimalSeparatorAlwaysShown(true);
        FORMAT.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.FRANCE));
    }

    public final int id;
    private final String name;
    private final int idStatus;
    private final String status;
    private final String guestName;
    private final int red;
    private final int green;
    private final int blue;
    private int idCourse;
    private final String strCourse;

    private final List<POSGuest> guests;

    /**
     * Order rows per guest
     */
    public final HashMap<POSGuest, List<TableOrderRow>> guestOrders;

    private final Map<Guest, PackageItems> guestPackageItems;

    /**
     * True if this table's orders are up to date with the server, false if new
     * orders have been added
     */
    private boolean upToDate;
    private int selectedGuestIndex;
    private final String strCourseShort;

    Table(SoapObject o) {
        this.id = Integer.parseInt(o.getProperty("ID").toString());
        this.name = o.getProperty("strTableName").toString();
        this.status = o.getProperty("strStatus").toString();
        this.idStatus = Integer.parseInt(o.getProperty("intStatus").toString());
        this.guestName = o.getProperty("strGuestName").toString();
        this.guests = new ArrayList<POSGuest>();
        this.guestOrders = new LinkedHashMap<POSGuest, List<TableOrderRow>>();
        this.guestPackageItems = new LinkedHashMap<Guest, PackageItems>();
        this.red = Integer.parseInt(o.getPropertyAsString("intRed"));
        this.green = Integer.parseInt(o.getPropertyAsString("intGreen"));
        this.blue = Integer.parseInt(o.getPropertyAsString("intBlue"));
        this.upToDate = false;
        this.selectedGuestIndex = -1;

        this.idCourse = Integer.parseInt(o.getPropertyAsString("intCourse"));
        this.strCourse = checkNIL(o.getProperty("strCourse"));
        this.strCourseShort = strCourse.length() > 3 ? strCourse.substring(0, 3).toUpperCase() : strCourse.toUpperCase();
    }

    /**
     * Clear all guests and orders, and get the guests and their orders from the
     * server. After calling this method, the 'upToDate' flag will be set to
     * <code>true</code>
     * 
     * @param client
     * @param sha
     * @param location
     * @throws Talk2MeException
     */
    public void initGuestsAndOrders(Talk2MeClient client, String sha, Location location) throws Talk2MeException {
        clear();
        List<POSGuest> guests = client.getTableGuests(sha, location, this);
        List<TableOrderRow> orders = client.getTableOrders(sha, location, this);
        linkOrdersToGuests(client, sha, guests, orders);
        upToDate = true;
    }

    private void linkOrdersToGuests(Talk2MeClient client, String sha, List<POSGuest> guests, List<TableOrderRow> orders) throws Talk2MeException {
        List<TableOrderRow> linked = new ArrayList<TableOrderRow>();
        for (TableOrderRow order : orders) {
            if (guests.size() > 0) {
                for (POSGuest guest : guests) {
                    if (guest != null) {
                        if (guest.id == order.getIdGuest()) {
                            addOrder(client, sha, guest, order, true, false);
                            linked.add(order);
                        }
                    }
                }
            } else {
                addOrder(client, sha, POSGuest.UNKNOWN, order, true, false);
            }
        }
        orders.removeAll(linked);
    }

    /**
     * Check if the table's orders/guests are up to date with the server
     * 
     * @return <code>true</code> if this table's orders are up to date with the
     *         server, <code>false</code> if new orders have been added
     */
    public boolean isUpToDate() {
        return upToDate;
    }

    /**
     * Set the table to changed (i.e. upToDate = false).
     */
    public void setChanged() {
        upToDate = false;
    }

    /**
     * Add a guest
     * 
     * @param g
     * @throws Talk2MeException
     */
    public void addGuest(Talk2MeClient client, String sha, POSGuest g) throws Talk2MeException {
        if (!guestOrders.containsKey(g)) {
            // upToDate = false;
            guests.add(g);
            guestOrders.put(g, new ArrayList<TableOrderRow>());
            if (g != POSGuest.UNKNOWN && !guestPackageItems.containsKey(g)) {
                loadPackageItems(client, sha, g);
            }

            List<TableOrderRow> unknownOrders = guestOrders.get(POSGuest.UNKNOWN);
            if (unknownOrders != null) {
                linkOrdersToGuests(client, sha, guests, unknownOrders);
            }
        }
    }

    private void loadPackageItems(Talk2MeClient client, String sha, POSGuest g) throws Talk2MeException {
        guestPackageItems.put(g, client.getPackageItems(sha, g));
    }

    /**
     * Add a TableOrderRow for a Guest
     * 
     * @param g
     * @param tor
     * @throws Talk2MeException
     */
    public void addOrder(Talk2MeClient client, String sha, POSGuest g, TableOrderRow tor) throws Talk2MeException {
        addOrder(client, sha, g, tor, true);
    }

    /**
     * Add an order
     * 
     * @param g
     * @param tor
     * @param forceNewRow
     *            Set to true if a new row has to be added, false if it could
     *            potentially only change the quantity of an existing order.
     * @throws Talk2MeException
     */
    public void addOrder(Talk2MeClient client, String sha, POSGuest g, TableOrderRow tor, boolean forceNewRow) throws Talk2MeException {
        addOrder(client, sha, g, tor, forceNewRow, true);
    }

    private void addOrder(Talk2MeClient client, String sha, POSGuest g, TableOrderRow tor, boolean forceNewRow, boolean isUserAction) throws Talk2MeException {
        upToDate = false;
        if (!guestOrders.containsKey(g)) {
            addGuest(client, sha, g);
        }

        List<TableOrderRow> orders = guestOrders.get(g);
        if (forceNewRow) {
            if (tor.getIdArticleParent() != 0) {
                insertAfterParent(orders, tor);
            } else {
                orders.add(tor);
            }
            if (isUserAction)
                resetPrintedForGroup(orders, tor);

        } else {
            TableOrderRow existing = null;
            for (TableOrderRow tableOrderRow : orders) {
                if (tableOrderRow.getIdArticle() == tor.getIdArticle() && tableOrderRow.id == 0) {
                    existing = tableOrderRow;
                    break;
                }
            }

            if (existing == null || existing.getIdArticleParent() > 0) {
                if (tor.getIdArticleParent() != 0) {
                    insertAfterParent(orders, tor);
                } else {
                    orders.add(tor);
                }
                if (isUserAction)
                    resetPrintedForGroup(orders, tor);

            } else {
                // if the same article is already added as a new order, only
                // change the quantity
                existing.setQuantity(existing.getQuantity() + tor.getQuantity());
            }
        }
    }

    private void insertAfterParent(List<TableOrderRow> orders, TableOrderRow tor) {
        for (int i = orders.size() - 1; i >= 0; i--) {
            TableOrderRow tableOrderRow = orders.get(i);
            if (tor.getIdChoiceGroup() == tableOrderRow.getIdChoiceGroup()) {
                orders.add(i + 1, tor);
                return;
            }
        }

        orders.add(tor);

    }

    private void resetPrintedForGroup(List<TableOrderRow> orders, TableOrderRow tor) {
        if (tor.getIdArticleParent() != 0) {
            for (TableOrderRow o : orders) {
                if (o.getIdChoiceGroup() == tor.getIdChoiceGroup()) {
                    o.setPrinted(0);
                }
            }
        }
    }

    /**
     * Get the guests
     * 
     * @return
     */
    public List<POSGuest> getGuests() {
        List<POSGuest> guests = new ArrayList<POSGuest>(guestOrders.keySet().size());
        for (POSGuest guest : guestOrders.keySet()) {
            guests.add(guest);
        }
        return guests;
    }

    /**
     * Get the guest by NFC tag ID
     * 
     * @param tagId
     * @return
     */
    public POSGuest getGuestByTagId(String tagId) {
        for (POSGuest guest : guests) {
            if (guest.getTagIdentification().equals(tagId)) {
                return guest;
            }
        }
        return null;
    }

    public void setSelectedGuestIndex(int i) {
        this.selectedGuestIndex = i;
    }

    public int getSelectedGuestIndex() {
        return this.selectedGuestIndex;
    }

    public POSGuest getSelectedGuest() {
        if (selectedGuestIndex >= 0 && selectedGuestIndex < guests.size()) {
            return guests.get(selectedGuestIndex);
        }
        return null;
    }

    public PackageItems getPackageItemsFromSelectedGuest() {
        if (selectedGuestIndex >= 0 && selectedGuestIndex < guests.size()) {
            Guest g = guests.get(selectedGuestIndex);
            if (g != null) {
                return guestPackageItems.get(g);
            }
        }

        return null;
    }

    /**
     * Clear the orders and guests
     */
    private void clear() {
        guestOrders.clear();
        guestPackageItems.clear();
        guests.clear();
        selectedGuestIndex = -1;
    }

    /**
     * Get the formatted total amount of articles linked to this Table in the
     * used currency (for example EUR).
     * 
     * @return
     */
    public String getTableTotalFormatted() {
        return FORMAT.format(getTableTotal() / 100d);
    }

    /**
     * Get the total of amount of articles in cents
     * 
     * @return
     */
    public int getTableTotal() {
        int total = 0;
        for (List<TableOrderRow> orders : guestOrders.values()) {
            for (TableOrderRow order : orders) {
                if (order.getDeleted() == 0) {
                    total += (order.getArticlePrice() * order.getQuantity());
                }
            }
        }
        return total;
    }

    @Override
    public String toString() {
        String s = getName() + " - " + getStatus();
        String g = strCourse;
        if (!"".equals(g) && (idStatus == TableStatus.STATUS_2 || idStatus == TableStatus.STATUS_BUSY || idStatus == TableStatus.STATUS_OCCUPIED)) {
            s += "\n" + g;
        }

        return s;
    }

    /**
     * Get the orders linked to a guest
     * 
     * @param g
     * @return
     */
    public List<TableOrderRow> getOrders(POSGuest g) {
        List<TableOrderRow> orders = new ArrayList<TableOrderRow>();
        if (guestOrders.get(g) != null) {
            orders.addAll(guestOrders.get(g));
        }
        return orders;
    }

    public String getTableTotalFormatted(POSGuest g) {
        List<TableOrderRow> orders = getOrders(g);
        int total = 0;
        for (TableOrderRow o : orders) {
            if (o.id != 0 && o.getDeleted() == 0) {
                total += o.getArticlePrice();
            }
        }
        return FORMAT.format(total / 100d);
    }

    public List<TableOrderRow> getAllOrders() {
        List<TableOrderRow> orders = new ArrayList<TableOrderRow>();
        for (POSGuest g : guests) {
            orders.addAll(getOrders(g));
        }
        return orders;
    }

    /**
     * Get the quantity of unordered articles.
     * 
     * @param g
     * @return
     */
    public int getUnorderedCount(POSGuest g) {
        List<TableOrderRow> orders = getOrders(g);
        int count = 0;
        for (TableOrderRow r : orders) {
            if (r.id == 0) {
                count += r.getQuantity();
            }
        }
        return count;
    }

    /**
     * Cancel an order. This can lead to either simply removing the order from
     * the list (if the item was not ordered yet), or to creating a 'do not
     * make'-ticket by the server if the item was already ordered.
     * 
     * @param client
     * @param sha
     * @param user
     * @param device
     * @param location
     * @param tor
     * @throws Talk2MeException
     */
    public void cancelOrder(Talk2MeClient client, String sha, User user, Device device, Location location, TableOrderRow tor) throws Talk2MeException {
        if (tor.id != 0) {
            // this is already ordered
            tor.setDeleted(-1);
            // also remove sub-items
            if (tor.getIdArticleParent() == 0) {
                for (List<TableOrderRow> orders : guestOrders.values()) {
                    for (TableOrderRow o : orders) {
                        if (o.getIdChoiceGroup() == tor.getIdChoiceGroup()) {
                            o.setDeleted(-1);
                        }
                    }
                }
            } else {
                for (List<TableOrderRow> orders : guestOrders.values()) {
                    resetPrintedForGroup(orders, tor);
                }
            }

            client.setTableOrders(sha, user, device, location, this, OrderAction.ORDER);
            initGuestsAndOrders(client, sha, location);
        } else {
            // this is not yet ordered, so we can just remove from our list
            remove(tor);
        }
    }

    /**
     * Place the current order. This will lead to sending all TableOrderRows in
     * this Table to the POS server. After this call, the table will be up to
     * date again with the state of the server (i.e. initGuestsAndOrders is
     * called as well).
     * 
     * @param client
     *            The client
     * @param sha
     *            The SHA string
     * @param user
     *            The User that is logged in
     * @param device
     *            The Device that is used
     * @param location
     *            The current Location
     * @throws Talk2MeException
     */
    public void placeOrder(Talk2MeClient client, String sha, User user, Device device, Location location) throws Talk2MeException {
        client.setTableOrders(sha, user, device, location, this, OrderAction.ORDER);
        initGuestsAndOrders(client, sha, location);
    }

    /**
     * Remove a TableOrderRow from this table
     * 
     * @param tor
     */
    private void remove(TableOrderRow tor) {
        for (List<TableOrderRow> orders : guestOrders.values()) {
            orders.remove(tor);
            if (tor.getIdArticleParent() == 0) {
                removeSubItems(orders, tor);
            }
        }
    }

    private void removeSubItems(List<TableOrderRow> orders, TableOrderRow tor) {
        List<TableOrderRow> remove = new ArrayList<TableOrderRow>();
        for (TableOrderRow sub : orders) {
            if (sub.getIdChoiceGroup() == tor.getIdChoiceGroup()) {
                remove.add(sub);
            }
        }
        orders.removeAll(remove);
    }

    /**
     * Get the name
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the guestName
     */
    public String getGuestName() {
        return guestName;
    }

    public String getCourse() {
        return strCourse;
    }

    public int getIdCourse() {
        return idCourse;
    }

    public int getIdStatus() {
        return idStatus;
    }

    /**
     * @return the red
     */
    public int getRed() {
        return red;
    }

    /**
     * @return the green
     */
    public int getGreen() {
        return green;
    }

    /**
     * @return the blue
     */
    public int getBlue() {
        return blue;
    }

    public void setIdCourse(int id) {
        this.idCourse = id;
        setChanged();
    }
}
