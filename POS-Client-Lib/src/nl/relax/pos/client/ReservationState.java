package nl.relax.pos.client;

public enum ReservationState {

    PAID(1), DEBIT_TO_ACCOUNT(2), CANCELED(3);

    public final int id;

    ReservationState(int id) {
        this.id = id;
    }
}
