package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

public class ProposalResponse extends Talk2MeBaseType {

    private final List<ProposalRow> rows;
    private final Map<Integer, Proposal> proposalsGrouped;

    public ProposalResponse(SoapObject o) {
        this.rows = new ArrayList<ProposalRow>();
        this.proposalsGrouped = new LinkedHashMap<Integer, Proposal>();
        try {
            SoapObject rows = (SoapObject) o.getProperty(1);
            int count = rows.getPropertyCount();
            for (int i = 0; i < count; i++) {
                ProposalRow p = new ProposalRow((SoapObject) rows.getProperty(i));
                this.rows.add(p);

                int propId = p.getProposal();
                Proposal list = proposalsGrouped.get(propId);
                if (list == null) {
                    list = new Proposal(propId);
                    proposalsGrouped.put(propId, list);
                }
                list.addProposalRow(p);
            }
        } catch (Throwable t) {
            t.printStackTrace();
            // no proposals
        }
    }

    /**
     * Get all proposal-rows
     * 
     * @return
     */
    public List<ProposalRow> getProposalRows() {
        return rows;
    }

    /**
     * Get proposals grouped by the proposal-id.
     * 
     * @return
     */
    public Map<Integer, Proposal> getProposals() {
        return proposalsGrouped;
    }
}
