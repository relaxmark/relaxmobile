package nl.relax.pos.client;

public enum ErrorCode {

    Duplicate_Control_ID("CONTROL-CHECK"), POS_Session_Expired("CheckSHA-003"), RES_Session_Expired("CheckSHA-004");

    public final String code;

    ErrorCode(String code) {
        this.code = code;
    }

    public static boolean isSessionExpired(String errorCode) {
        return (POS_Session_Expired.code.equalsIgnoreCase(errorCode) || RES_Session_Expired.code.equalsIgnoreCase(errorCode));
    }
}
