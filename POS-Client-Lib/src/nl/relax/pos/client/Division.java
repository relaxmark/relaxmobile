package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * The Division
 * 
 * @author erik
 * 
 */
public class Division {

    /**
     * The ID
     */
    public final int id;

    /**
     * The name
     */
    private final String name;

    /**
     * The article Main group
     */
    private final int idPosPluMainGroup;

    Division(SoapObject o) {
        this.id = Integer.parseInt(o.getProperty("ID").toString());
        this.name = o.getProperty("strName").toString();
        this.idPosPluMainGroup = Integer.parseInt(o.getProperty("ID_POS_PLU_MainGroup").toString());
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * Get the name
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * The main article group of this division
     * 
     * @return
     */
    public int getIdPosPluMainGroup() {
        return idPosPluMainGroup;
    }
}
