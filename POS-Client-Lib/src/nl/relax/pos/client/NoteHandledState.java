package nl.relax.pos.client;

public enum NoteHandledState {
    ALL(1), HANDLED(-1), NOT_HANDLED(0);
    public static final String key = "p_intHandled";

    public final int value;

    NoteHandledState(int value) {
        this.value = value;
    }
}
