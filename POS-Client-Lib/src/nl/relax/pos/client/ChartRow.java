package nl.relax.pos.client;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.ksoap2.serialization.SoapObject;

public class ChartRow extends Talk2MeBaseType {

    public static final String SOAP_ELEMENT = "ttRES_ChartRow";

    private static final DecimalFormat FORMAT;
    static {
        FORMAT = new DecimalFormat("###0.00");
        FORMAT.setDecimalSeparatorAlwaysShown(true);
        FORMAT.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.FRANCE));
    }
    public final int id;
    private final Talk2MeDate datReservation;
    private int idGuest;
    private final int section;
    private final int order;
    private final String activityCode;
    private final String activityName1;
    private final String activityName2;
    private final String activityName;
    private String timeBegin;
    private String timeEnd;
    private final String additionalInfo;
    private final int quantity;
    private final double price;
    private final int idResArr;
    private final int idResAkt;
    private final int idResOpt;
    private final int idResOnline;

    public ChartRow(SoapObject o) {
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.datReservation = new Talk2MeDate(o.getProperty("datReservation").toString());
        this.idGuest = Integer.parseInt(o.getPropertyAsString("intGuestID"));
        this.section = Integer.parseInt(o.getPropertyAsString("intSection"));
        this.order = Integer.parseInt(o.getPropertyAsString("intOrder"));
        this.activityCode = o.getProperty("aaa_kd_").toString();
        this.activityName1 = o.getProperty("strNamePrimary").toString();
        this.activityName2 = o.getProperty("strNameSecundary").toString();
        this.activityName = o.getProperty("strNameFull").toString();
        this.timeBegin = formatTime(checkNIL(o.getProperty("tmc_beg")));
        this.timeEnd = formatTime(checkNIL(o.getProperty("tmc_end")));
        this.additionalInfo = o.getProperty("strAdditionalInfo").toString();
        this.quantity = Integer.parseInt(o.getPropertyAsString("intQuantity"));
        this.price = Double.parseDouble(o.getPropertyAsString("dblPrice"));
        this.idResArr = Integer.parseInt(o.getPropertyAsString("ID_RES_ARR"));
        this.idResAkt = Integer.parseInt(o.getPropertyAsString("ID_RES_AKT"));
        this.idResOpt = Integer.parseInt(o.getPropertyAsString("ID_RES_OPT"));
        this.idResOnline = Integer.parseInt(o.getPropertyAsString("ID_RES_ONL_INE"));
    }

    private String formatTime(String s) {
        if (s != null && s.length() == 4) {
            return s.substring(0, 3) + ":" + s.substring(3);
        }
        return s;
    }

    @Override
    public String toString() {
        return activityName1 + "\n " + datReservation + " " + timeBegin + "-" + timeEnd;
    }

    public Talk2MeDate getDatReservation() {
        return datReservation;
    }

    public int getIdGuest() {
        return idGuest;
    }

    public int getSection() {
        return section;
    }

    public int getOrder() {
        return order;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public String getActivityName1() {
        return activityName1;
    }

    public String getActivityName2() {
        return activityName2;
    }

    public String getActivityName() {
        return activityName;
    }

    public String getTimeBegin() {
        return timeBegin;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getPriceFormatted() {
        return FORMAT.format(getPrice());
    }

    public int getIdResArr() {
        return idResArr;
    }

    public int getIdActivity() {
        return idResAkt;
    }

    public int getIdResOpt() {
        return idResOpt;
    }

    public int getIdResAkt() {
        return idResAkt;
    }

    public int getIdResOnline() {
        return idResOnline;
    }

    public void setTimeBegin(String timeBegin) {
        this.timeBegin = timeBegin;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public void setIdGuest(int id) {
        this.idGuest = id;
    }
}
