package nl.relax.pos.client;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

public class NoteResponse extends Talk2MeTable<Note> {

    public NoteResponse(SoapObject o) {
        super(o, "ttNOT_Note");
    }

    @Override
    protected Note createRow(SoapObject row) {
        return new Note(row);
    }

    public void sort() {
        List<Note> rows = getRows();
        Collections.sort(rows, new Comparator<Note>() {
            @Override
            public int compare(Note o1, Note o2) {
                int diff = o2.getPriority() - o1.getPriority();
                if (diff == 0) {
                    diff = o2.getDateTime().getCalendar().compareTo(o1.getDateTime().getCalendar());
                }
                return diff;
            }
        });
    }
}
