package nl.relax.pos.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * DivisionData is a helper class to easily access all data linked to a Division
 * (such as Locations, ArticleGroups and Articles). Caching is performed locally
 * for improved responsiveness.<br/>
 * This class is to be instanciated after logging on.
 * 
 * @author erik
 */
public class DivisionData {

    private final List<Location> locations;

    /**
     * All article groups per location
     */
    private final Map<Location, List<ArticleGroup>> groups;

    /**
     * All article groups
     */
    private final List<ArticleGroup> allGroups;

    /**
     * All locations with all groups and articles
     */
    private final Map<Location, Map<ArticleGroup, List<Article>>> all;

    /**
     * A map with all articles
     */
    private final Map<Integer, Article> articles;

    private final boolean lazy;
    private final Division division;
    private final Talk2MeClient client;

    /**
     * Constructor for a single location
     * 
     * @param sha
     *            The SHA String
     * @param client
     *            The POSClient to use for doing the server transactions
     * @param division
     *            The Division
     * @throws Talk2MeException
     */
    public DivisionData(String sha, Talk2MeClient client, Division division, Location location) throws Talk2MeException {
        this.client = client;
        this.division = division;
        this.articles = new HashMap<Integer, Article>();
        this.locations = new ArrayList<Location>();
        this.groups = new LinkedHashMap<Location, List<ArticleGroup>>();
        this.allGroups = new ArrayList<ArticleGroup>();

        locations.add(location);

        List<ArticleGroup> g = client.getArticleGroups(sha, division, location.id);
        groups.put(location, g);
        allGroups.addAll(g);

        this.all = new HashMap<Location, Map<ArticleGroup, List<Article>>>();
        this.lazy = false;

        Map<ArticleGroup, List<Article>> articleGroups = new HashMap<ArticleGroup, List<Article>>();
        List<Article> articles = client.getArticles(sha, division, location, null);
        int cg = -1;
        ArticleGroup curGroup = null;
        List<Article> curGroupArticles = null;
        for (Article article : articles) {
            if (article.getGroupNumber() != cg) {
                curGroupArticles = new ArrayList<Article>();
                curGroup = getGroup(article.getGroupNumber());
                cg = article.getGroupNumber();
                articleGroups.put(curGroup, curGroupArticles);
            }
            curGroupArticles.add(article);
            this.articles.put(article.id, article);
        }

        all.put(location, articleGroups);
    }

    private ArticleGroup getGroup(int id) {
        for (ArticleGroup g : allGroups) {
            if (g.getGroupNumber() == id) {
                return g;
            }
        }
        return null;
    }

    /**
     * Constructor
     * 
     * @param sha
     *            The SHA String
     * @param client
     *            The POSClient to use for doing the server transactions
     * @param division
     *            The Division
     * @param lazy
     *            If <code>true</code> (recommended), Articles will only be
     *            loaded and cached on demand, otherwise it will pre-load
     *            everything (which will lead to a long initialisation time).
     * @throws Talk2MeException
     */
    public DivisionData(String sha, Talk2MeClient client, Division division, boolean lazy) throws Talk2MeException {
        this.client = client;
        this.division = division;
        this.articles = new HashMap<Integer, Article>();
        this.locations = client.getLocations(sha, division);
        this.groups = new LinkedHashMap<Location, List<ArticleGroup>>();
        this.allGroups = client.getArticleGroups(sha, division, 0);

        for (Location loc : locations) {
            groups.put(loc, client.getArticleGroups(sha, division, loc.id));
        }

        this.all = new HashMap<Location, Map<ArticleGroup, List<Article>>>();
        this.lazy = lazy;

        for (Location location : locations) {
            Map<ArticleGroup, List<Article>> articleGroups = new HashMap<ArticleGroup, List<Article>>();
            for (ArticleGroup group : allGroups) {
                if (!lazy) {
                    List<Article> articles = client.getArticles(sha, division, location, group);
                    for (Article article : articles) {
                        this.articles.put(article.id, article);
                    }
                    articleGroups.put(group, articles);
                } else {
                    articleGroups.put(group, null);
                }
            }
            all.put(location, articleGroups);
        }
    }

    /**
     * Get the locations
     * 
     * @return
     */
    public List<Location> getLocations() {
        return locations;
    }

    public void refreshLocations(String sha) throws Talk2MeException {
        List<Location> fresh = client.getLocations(sha, division);
        for (Location location : fresh) {
            refreshLocation(location);
        }
    }

    private void refreshLocation(Location toFind) {
        for (Location existing : locations) {
            if (existing.id == toFind.id) {
                existing.refresh(toFind);
                return;
            }
        }
    }

    /**
     * Get the article groups
     * 
     * @param location
     * @return
     */
    public List<ArticleGroup> getArticleGroups(Location location) {
        return groups.get(location);
    }

    public Article getArticle(int id) {
        return articles.get(id);
    }

    /**
     * Get the articles
     * 
     * @param sha
     *            The SHA String
     * @param group
     *            The ArticleGroup
     * @param location
     *            The Location
     * @return
     * @throws Talk2MeException
     */
    public List<Article> getArticles(final String sha, final ArticleGroup group, final Location location) throws Talk2MeException {
        List<Article> articles = all.get(location).get(group);
        if (!lazy) {
            return articles;
        } else {
            if (articles == null) {
                final List<Article> newArticles = new ArrayList<Article>();
                all.get(location).put(group, newArticles);
                final Talk2MeException[] ex = new Talk2MeException[1];
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            newArticles.addAll(client.getArticles(sha, division, location, group));

                            for (Article a : newArticles) {
                                DivisionData.this.articles.put(a.id, a);
                            }
                        } catch (Talk2MeException e) {
                            ex[0] = e;
                        }
                    }
                });
                t.start();
                try {
                    t.join();
                } catch (InterruptedException e) {
                }
                if (ex[0] != null) {
                    throw ex[0];
                }

                return newArticles;
            } else {
                return articles;
            }
        }
    }
}
