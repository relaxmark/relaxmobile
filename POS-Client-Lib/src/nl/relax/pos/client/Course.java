package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

public class Course extends Talk2MeBaseType {

    // <element name="ID" nillable="true" type="xsd:int"/>
    // <element name="strName" nillable="true" type="xsd:string"/>

    public final int id;
    private final String name;
    private final String nameShort;

    Course(SoapObject o) {
        this.id = Integer.parseInt(o.getProperty("ID").toString());
        this.name = checkNIL(o.getProperty("strName"));
        this.nameShort = (name.length() > 3 ? name.substring(0, 3) : name).toUpperCase();
    }

    public String getName() {
        return name;
    }

    public String getNameShort() {
        return nameShort;
    }

    @Override
    public String toString() {
        return name;
    }
}
