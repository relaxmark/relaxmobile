package nl.relax.pos.client;

import java.util.HashMap;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

/**
 * This class represents all server-defined settings.
 * 
 * @author erik
 */
public class Settings {

    private final Map<Integer, Setting> settings = new HashMap<Integer, Setting>();

    final String sha;

    Settings(String sha, SoapObject o) {
        this.sha = sha;
        SoapObject rows = (SoapObject) o.getProperty(1);
        int count = rows.getPropertyCount();
        for (int i = 0; i < count; i++) {
            Setting s = new Setting((SoapObject) rows.getProperty(i));
            settings.put(s.id, s);
        }
    }

    /**
     * Get a Setting
     * 
     * @param key
     *            The unique ID of the Setting
     * @return The Setting, or null if the Setting doesn't exist
     */
    public Setting get(Setting.Key key) {
        return settings.get(key.id);
    }

    /**
     * Get a Setting
     * 
     * @param key
     *            The unique ID of the Setting
     * @return The Setting, or null if the Setting doesn't exist
     */
    public Setting get(int key) {
        return settings.get(key);
    }
}
