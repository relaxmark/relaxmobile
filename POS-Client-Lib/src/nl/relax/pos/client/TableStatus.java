package nl.relax.pos.client;

public class TableStatus {

    public static final int STATUS_FREE = 1;

    public static final int STATUS_2 = 2;

    public static final int STATUS_BUSY = 3;
    public static final int STATUS_OCCUPIED = 4;

    public static final int STATUS_5 = 5;

    public static final int STATUS_WAITING = 6;
    public static final int STATUS_LONG_WAITING = 7;

}
