package nl.relax.pos.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.relax.pos.client.util.MarshalDecimal;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

/**
 * This is the main class to interact with the POS Server.
 * 
 * @author erik
 */
public class Talk2MeClient {

    public static final String NIL = "anyType{}";

    // SYS methods
    private static final String METHOD_GET_SYS_LICENSE = "getSYS_License";
    private static final String METHOD_GET_SYS_DEVICE = "getSYS_Device";
    private static final String METHOD_GET_SYS_USER = "getSYS_User";
    private static final String METHOD_GET_SYS_DIVISION = "getSYS_Division";
    private static final String METHOD_GET_SYS_LOGIN = "setSYS_UserLogin";
    private static final String METHOD_GET_SYS_LOGOUT = "setSYS_UserLogout";
    private static final String METHOD_GET_SYS_ICONS = "getSYS_Icon";

    // POS methods (Point Of Sale)
    private static final String METHOD_GET_POS_LAYOUT = "getPOS_Layout";
    private static final String METHOD_GET_POS_TABLE = "getPOS_Table";
    private static final String METHOD_GET_POS_TABLEORDER = "getPOS_TableOrder";
    private static final String METHOD_GET_POS_TABLESTATUS = "getPOS_TableStatus";
    private static final String METHOD_GET_POS_GUEST = "getPOS_Guest";
    private static final String METHOD_GET_POS_PACKAGEITEM = "getPOS_PackageItem";
    private static final String METHOD_GET_POS_GROUP = "getPOS_SubGroup";
    private static final String METHOD_GET_POS_PLU = "getPOS_PLU";
    private static final String METHOD_GET_POS_PLU_CHOICES = "getPOS_MenuPLUChoice";
    private static final String METHOD_GET_POS_COURSE = "getPOS_Course";
    private static final String METHOD_SET_POS_TABLE_ORDER = "setPOS_TableOrder";
    private static final String METHOD_UPD_POS_TABLE = "updPOS_TableStatus";

    // HTL methods (Hotel)
    private static final String METHOD_GET_HTL_ROOMINFO = "getHTL_RoomInfo";
    private static final String METHOD_GET_HTL_GUEST = "getHTL_Guest";

    // RES methods (Reservations)
    private static final String METHOD_GET_RES_SETTING = "getRES_Setting";
    private static final String METHOD_GET_RES_SESSION = "getRES_Session";
    private static final String METHOD_GET_RES_RESERVATION_DETAILS = "getRES_ReservationDetail";
    private static final String METHOD_GET_RES_ACTIVITY = "getRES_Activity";
    private static final String METHOD_GET_RES_GUEST = "getRES_Guest";

    // ROL methods (Online Reservations)
    private static final String METHOD_GET_RES_PROPOSAL = "getROL_Proposal";
    private static final String METHOD_UPD_RES_PROPOSAL = "updROL_Proposal";
    private static final String METHOD_ADD_RES_AAA2CHART = "addROL_AAA2Chart";
    private static final String METHOD_GET_RES_RESERVATION = "getROL_Reservation";
    private static final String METHOD_UPD_RES_RESERVATION = "updROL_Reservation";
    private static final String METHOD_GET_RES_CHART = "getROL_Chart";
    private static final String METHOD_DEL_RES_CHART = "delROL_Chart";
    private static final String METHOD_ADD_RES_PRO2CHART = "addROL_PRO2Chart";

    // NOT methods (Notes)
    private static final String METHOD_GET_NOT_NOTE = "getNOT_Note";
    private static final String METHOD_ADD_NOT_NOTE = "addNOT_Note";
    private static final String METHOD_UPD_NOT_NOTE = "updNOT_Note";

    public static final String LOGIN_TYPE_MANUAL = "PDA_LOGIN";
    public static final String LOGIN_TYPE_MIFARE = "CARD_LOGIN";

    /**
     * This is just an arbitrary value that is not actually used server-side in
     * our case, but it's mandatory to send an IP address to the server as a
     * back-up if a client can't provide a MAC address. This client can however
     * always provide a MAC address, so the IP address is not used in practice.
     */
    private static final String STR_IP_ADDRESS = "0.0.0.0";

    private final HttpTransportSE transport;
    final String namespace;

    private final int retries;

    private final RetryListener retryListener;

    private final SessionExpiredHandler sessionExpiredHandler;

    private final long retryInterval;

    private Settings settings;

    /**
     * Sequence number to avoid server side duplicates in case of time-outs when
     * getting the response (for some reason it's not possible to detect this
     * condition server-side).
     */
    private int controlIdPOS = 1;

    private int controlIdRES = 1;

    /**
     * Constructor
     * 
     * @param url
     *            The URL of the SOAP webservice to connect to
     * @param namespace
     *            The name-space of the web service
     * @param debug
     *            If <code>true</code>, the client will output verbose debug
     *            information. Set to <code>false</code> in production releases
     *            to prevent logging of security sensitive information such as
     *            passwords.
     */
    public Talk2MeClient(String url, String namespace, boolean debug) {
        this(url, namespace, debug, 20000, 0, 0, null, null);
    }

    /**
     * 
     * @param url
     * @param namespace
     * @param debug
     * @param socketTimeOut
     * @param retries
     * @param retryInterval
     * @param listener
     */
    public Talk2MeClient(String url, String namespace, boolean debug, int socketTimeOut, int retries, int retryInterval, RetryListener listener) {
        this(url, namespace, debug, socketTimeOut, retries, retryInterval, listener, null);
    }

    /**
     * 
     * @param url
     * @param namespace
     * @param debug
     * @param socketTimeOut
     * @param retries
     * @param retryInterval
     * @param listener
     * @param sessionExpiredHandler
     */
    public Talk2MeClient(String url, String namespace, boolean debug, int socketTimeOut, int retries, int retryInterval, RetryListener listener,
            SessionExpiredHandler sessionExpiredHandler) {
        this.transport = new HttpTransportSE(url, socketTimeOut);
        this.transport.debug = debug;
        this.namespace = namespace;
        this.retries = retries;
        this.retryListener = listener;
        this.retryInterval = retryInterval;
        this.sessionExpiredHandler = sessionExpiredHandler;
    }

    /**
     * Perform a SOAP request with the server
     * 
     * @param request
     * @param method
     * @return
     * @throws Talk2MeException
     */
    private synchronized SoapObject call(SoapObject request, String method, Marshal... m) throws Talk2MeException {
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        envelope.dotNet = false;
        for (Marshal marshal : m) {
            marshal.register(envelope);
        }

        IOException ee = null;

        for (int i = 0; i <= retries; i++) {
            try {
                if (transport.debug) {
                    System.out.println("//// SENDING");
                    System.out.println(envelope.bodyOut);
                }
                try {
                    transport.call(this.namespace + "/" + method, envelope);
                } catch (XmlPullParserException ppe) {
                    System.out.println(ppe.getMessage() + " retrying...");
                    transport.call(this.namespace + "/" + method, envelope);
                }
                if (transport.debug) {
                    System.out.println("//// SOAP SENT");
                    System.out.println(transport.requestDump);
                    System.out.println("\n//// RECEIVED:");
                    System.out.println(transport.responseDump);
                }

                Object response = envelope.bodyIn;

                if (response instanceof SoapObject) {
                    SoapObject so = (SoapObject) response;

                    if (!so.hasProperty("p_oke") || "true".equals(so.getProperty("p_oke").toString())) {
                        return so;
                    } else if (i > 0 && so.hasProperty("p_strErrorCode")
                            && ErrorCode.Duplicate_Control_ID.code.equals(so.getPropertyAsString("p_strErrorCode"))) {
                        // This is to handle sent and processed requests that
                        // still resulted in a time-out.
                        // We can ignore this error in this case
                        return so;
                    } else if (so.hasProperty("p_strErrorCode") && ErrorCode.isSessionExpired(so.getPropertyAsString("p_strErrorCode"))) {
                        if (sessionExpiredHandler != null) {
                            sessionExpiredHandler.notifySessionExpired(new Talk2MeSessionExpiredException(so));
                        } else {
                            throw new Talk2MeSessionExpiredException(so);
                        }
                    } else {
                        throw new Talk2MeException(so);
                    }
                } else {
                    SoapFault fault = (SoapFault) response;
                    throw new Talk2MeException(fault.getMessage() + " (" + method + ")");
                }
            } catch (final IOException e) {
                e.printStackTrace();
                ee = e;
                if (i == retries) {
                    throw new Talk2MeException(e);
                } else if (retryListener != null) {
                    retryListener.retrying(i + 1, e);
                    try {
                        Thread.sleep(retryInterval);
                    } catch (InterruptedException e1) {
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Talk2MeException(e);
            }
        }

        if (ee != null) {
            throw new Talk2MeException(ee);
        } else {
            throw new Talk2MeException("Retries exhausted");
        }
    }

    /**
     * Get the License
     * 
     * @param strDB_IDENTIFIER
     * @return
     * @throws Talk2MeException
     */
    public License getLicense(String strDB_IDENTIFIER) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_LICENSE);
        request.addProperty("p_strDBIdentifier", strDB_IDENTIFIER);

        return new License(call(request, METHOD_GET_SYS_LICENSE));
    }

    /**
     * Get the Device
     * 
     * @param p_ID_SYS_ADM
     * @param mac
     * @return
     * @throws Talk2MeException
     */
    public Device getDevice(String p_ID_SYS_ADM, String mac) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_DEVICE);
        request.addProperty("p_ID_SYS_ADM", p_ID_SYS_ADM);
        request.addProperty("p_strIP_Address", STR_IP_ADDRESS);
        request.addProperty("p_strMAC_Address", mac);

        Device d = new Device(call(request, METHOD_GET_SYS_DEVICE));

        controlIdPOS = d.lastControlId + 1;

        return d;
    }

    /**
     * Get all the users
     * 
     * @param p_ID_SYS_ADM
     * @param p_ID_SYS_Group
     * @return
     * @throws Talk2MeException
     */
    public UserResponse getSYS_User(String p_ID_SYS_ADM, int p_ID_SYS_Group) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_USER);
        request.addProperty("p_ID_SYS_ADM", p_ID_SYS_ADM);
        request.addProperty("p_ID_SYS_Group", p_ID_SYS_Group);

        return new UserResponse(call(request, METHOD_GET_SYS_USER));
    }

    /**
     * Get divisions
     * 
     * @return
     * @throws Talk2MeException
     */
    public DivisionResponse getDivisions() throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_DIVISION);
        request.addProperty("p_POS_Only", 1);

        return new DivisionResponse(call(request, METHOD_GET_SYS_DIVISION));
    }

    /**
     * Do a logon request
     * 
     * @param user
     * @param pass
     * @return
     * @throws Talk2MeException
     * 
     * @deprecated Use the login method without ref_can_cd parameter
     */
    @Deprecated
    public LoginResponse login(Division division, User user, String pass, License license, String macAddress, String ref_can_cd, int id_sys_module)
            throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_LOGIN);
        request.addProperty("p_id_sys_adm", Integer.parseInt(license.getIdSysAdm()));
        request.addProperty("p_id_sys_language", user.getLanguage());
        request.addProperty("p_id_sys_module", id_sys_module);
        request.addProperty("p_ref_can_cd_", ref_can_cd);
        request.addProperty("p_id_sys_user", user.id);
        request.addProperty("p_strMAC_Address", macAddress);
        request.addProperty("p_strIP_Address", STR_IP_ADDRESS);
        request.addProperty("p_strPassword", pass);
        request.addProperty("p_id_sys_division", division.id);

        return new LoginResponse(call(request, METHOD_GET_SYS_LOGIN));
    }

    /**
     * Login with user-name and password (manual log-in)
     * 
     * @param division
     * @param user
     * @param pass
     * @param license
     * @param macAddress
     * @param id_sys_module
     * @return
     * @throws Talk2MeException
     */
    public LoginResponse login(Division division, User user, String pass, License license, String macAddress, int id_sys_module) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_LOGIN);
        request.addProperty("p_id_sys_adm", Integer.parseInt(license.getIdSysAdm()));
        request.addProperty("p_id_sys_language", user.getLanguage());
        request.addProperty("p_id_sys_module", id_sys_module);
        request.addProperty("p_ref_can_cd_", LOGIN_TYPE_MANUAL);
        request.addProperty("p_id_sys_user", user.id);
        request.addProperty("p_strMAC_Address", macAddress);
        request.addProperty("p_strIP_Address", STR_IP_ADDRESS);
        request.addProperty("p_strPassword", pass);
        request.addProperty("p_id_sys_division", division.id);

        return new LoginResponse(call(request, METHOD_GET_SYS_LOGIN));
    }

    /**
     * Login with user-name with Mifare tag id
     * 
     * @param division
     * @param tagIdentification
     * @param license
     * @param macAddress
     * @param id_sys_module
     * @return
     * @throws Talk2MeException
     */
    public LoginResponse login(Division division, String tagIdentification, License license, String macAddress, int id_sys_module) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_LOGIN);
        request.addProperty("p_id_sys_adm", Integer.parseInt(license.getIdSysAdm()));
        request.addProperty("p_id_sys_language", "");
        request.addProperty("p_id_sys_module", id_sys_module);
        request.addProperty("p_ref_can_cd_", LOGIN_TYPE_MIFARE);
        request.addProperty("p_id_sys_user", 0);
        request.addProperty("p_strMAC_Address", macAddress);
        request.addProperty("p_strIP_Address", STR_IP_ADDRESS);
        request.addProperty("p_strPassword", tagIdentification);
        request.addProperty("p_id_sys_division", division.id);

        return new LoginResponse(call(request, METHOD_GET_SYS_LOGIN));
    }

    /**
     * Do a logout request
     * 
     * @param sha
     * @param division
     * @param user
     * @return
     * @throws Talk2MeException
     */
    public LogoutResponse logout(String sha, Division division, User user) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_LOGOUT);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_intControlID", controlIdPOS);
        request.addProperty("p_id_sys_user", user.id);
        request.addProperty("p_id_sys_division", division.id);

        controlIdPOS++;

        return new LogoutResponse(call(request, METHOD_GET_SYS_LOGOUT));
    }

    /**
     * Get the locations
     * 
     * @param sha
     *            The SHA string
     * @param division
     *            The Division
     * @return
     * @throws Talk2MeException
     */
    public List<Location> getLocations(String sha, Division division) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_LAYOUT);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_ID_SYS_Division", division.id);

        SoapObject result = call(request, METHOD_GET_POS_LAYOUT);
        int count = result.getPropertyCount();
        final List<Location> rows = new ArrayList<Location>(count);

        Object o = result.getProperty(1);
        if (o instanceof SoapObject) {
            SoapObject so0 = (SoapObject) result.getProperty(1);
            count = so0.getPropertyCount();

            for (int i = 0; i < count; i++) {
                SoapObject so = (SoapObject) so0.getProperty(i);
                Location row = new Location(so);
                rows.add(row);
            }
        }

        return rows;
    }

    /**
     * Get the icons
     * 
     * @param sha
     *            The SHA String
     * @param withImages
     *            If <code>true</code> the binary image data will be included
     * @return
     * @throws Talk2MeException
     */
    public List<Icon> getIcons(String sha, boolean withImages) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_ICONS);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_blnSendImage", withImages ? -1 : 0);
        request.addProperty("p_ID_SYS_Icon", 0);

        SoapObject result = call(request, METHOD_GET_SYS_ICONS);

        SoapObject so0 = (SoapObject) result.getProperty(1);
        final int count = so0.getPropertyCount();
        final List<Icon> icons = new ArrayList<Icon>(count);

        for (int i = 0; i < count; i++) {
            SoapObject o = (SoapObject) so0.getProperty(i);
            icons.add(new Icon(o));
        }

        return icons;
    }

    /**
     * Get an icon (including BLOB data)
     * 
     * @param sha
     * @param idIcon
     * @return
     * @throws Talk2MeException
     */
    public Icon getIcon(String sha, int idIcon) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_SYS_ICONS);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_blnSendImage", -1);
        request.addProperty("p_ID_SYS_Icon", idIcon);

        SoapObject result = call(request, METHOD_GET_SYS_ICONS);

        SoapObject so0 = (SoapObject) result.getProperty(1);
        final int count = so0.getPropertyCount();
        Icon icon = null;

        for (int i = 0; i < count; i++) {
            SoapObject o = (SoapObject) so0.getProperty(i);
            icon = new Icon(o);
        }

        return icon;
    }

    /**
     * Get the tables from a Location
     * 
     * @param sha
     *            The SHA string
     * @param keepAlive
     *            Set to 'false' if calling this method shouln't bump the
     *            session-timer. For example if this method is polled in the
     *            background, the session should still expire over time.
     * @param location
     *            The location
     * @return
     * @throws Talk2MeException
     */
    public List<Table> getTables(String sha, boolean keepAlive, Location location) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_TABLE);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_logAutoPolling", !keepAlive);
        request.addProperty("p_ID_POS_Layout", location.id);

        SoapObject result = call(request, METHOD_GET_POS_TABLE);

        SoapObject so0 = (SoapObject) result.getProperty(1);
        final int count = so0.getPropertyCount();
        final List<Table> tables = new ArrayList<Table>(count);

        for (int i = 0; i < count; i++) {
            SoapObject o = (SoapObject) so0.getProperty(i);
            Table table = new Table(o);
            tables.add(table);
        }

        return tables;
    }

    /**
     * Get the tables from a location
     * 
     * @param sha
     *            The SHA string
     * @param location
     *            The location
     * @return
     * @throws Talk2MeException
     */
    public List<Table> getTables(String sha, Location location) throws Talk2MeException {
        return getTables(sha, true, location);
    }

    /**
     * Get the table status, which contains the choosable next Courses
     * 
     * @param sha
     * @param location
     * @param table
     * @param allCourses
     * @return
     * @throws Talk2MeException
     */
    public TableStatusResponse getTableStatus(String sha, Location location, Table table, Courses allCourses) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_TABLESTATUS);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_ID_POS_Layout", location.id);
        request.addProperty("p_ID_POS_Table", table.id);

        return new TableStatusResponse(call(request, METHOD_GET_POS_TABLESTATUS), allCourses);
    }

    /**
     * Get all the orders for a Table
     * 
     * @param sha
     * @param location
     * @param table
     * @return
     * @throws Talk2MeException
     */
    public List<TableOrderRow> getTableOrders(String sha, Location location, Table table) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_TABLEORDER);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_ID_POS_Layout", location.id);
        request.addProperty("p_ID_POS_Table", table.id);

        SoapObject tableOrder = call(request, METHOD_GET_POS_TABLEORDER);
        List<TableOrderRow> orders = new ArrayList<TableOrderRow>();

        Object rows = tableOrder.getProperty(1);
        if (rows instanceof SoapObject) {
            SoapObject so0 = (SoapObject) rows;
            int count = so0.getPropertyCount();

            for (int i = 0; i < count; i++) {
                SoapObject o = (SoapObject) so0.getProperty(i);
                orders.add(new TableOrderRow(o));
            }
        }

        return orders;
    }

    /**
     * Get the guests at a table
     * 
     * @param sha
     * @param location
     * @param table
     * @return
     * @throws Talk2MeException
     */
    public List<POSGuest> getTableGuests(String sha, Location location, Table table) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_GUEST);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_ID_POS_Layout", location.id);
        request.addProperty("p_ID_POS_Table", table.id);

        GregorianCalendar cal = new GregorianCalendar();
        int year = cal.get(GregorianCalendar.YEAR);
        int month = cal.get(GregorianCalendar.MONTH) + 1;
        int day = cal.get(GregorianCalendar.DAY_OF_MONTH);
        String date = year + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);
        request.addProperty("p_datDate", new SoapPrimitive("xsd", "date", date));

        SoapObject resDLN = call(request, METHOD_GET_POS_GUEST);
        Object soap = resDLN.getProperty(1);
        SoapObject so0 = soap instanceof SoapObject ? (SoapObject) resDLN.getProperty(1) : null;
        int count = (so0 != null) ? so0.getPropertyCount() : 0;
        final List<POSGuest> guests = new ArrayList<POSGuest>();
        //
        for (int i = 0; i < count; i++) {
            System.out.println("i=" + i);

            SoapObject o = (SoapObject) so0.getProperty(i);
            POSGuest g = new POSGuest(o);
            if (g != null) {
                guests.add(g);
            }
        }
        return guests;
    }

    /**
     * Get the guest that is linked to an NFC tag ID
     * 
     * @param sha
     * @param tagId
     * @return
     * @throws Talk2MeException
     */
    public POSGuest getGuestByTag(String sha, String tagId, Table table) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_GUEST);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_strIdentification", tagId);
        request.addProperty("p_strCode", "");
        request.addProperty("p_ID_POS_Table", table.id);

        SoapObject o = call(request, METHOD_GET_RES_GUEST);
        int p_ID_RES_RES_KOP = Integer.parseInt(o.getProperty("p_ID_RES_RES_KOP").toString());
        int p_ID_RES_RES_DLN = Integer.parseInt(o.getProperty("p_ID_RES_RES_DLN").toString());
        String name = o.getProperty("p_strGuestName").toString();
        return new POSGuest(p_ID_RES_RES_KOP, p_ID_RES_RES_DLN, name, tagId, "");
    }

    /**
     * Get Guest by manual input code
     * 
     * @param sha
     * @param code
     * @return
     * @throws Talk2MeException
     */
    public POSGuest getGuestByCode(String sha, String code, Table table) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_GUEST);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_strIdentification", "");
        request.addProperty("p_strCode", code);
        request.addProperty("p_ID_POS_Table", table.id);

        SoapObject o = call(request, METHOD_GET_RES_GUEST);
        int p_ID_RES_RES_KOP = Integer.parseInt(o.getProperty("p_ID_RES_RES_KOP").toString());
        int p_ID_RES_RES_DLN = Integer.parseInt(o.getProperty("p_ID_RES_RES_DLN").toString());
        String name = o.getProperty("p_strGuestName").toString();
        return new POSGuest(p_ID_RES_RES_KOP, p_ID_RES_RES_DLN, name, "", code);
    }

    /**
     * Get the PackageItems for a guest, which contains items that are part of
     * an arrangement (for example a free coffee).
     * 
     * @param sha
     * @param guest
     * @return
     * @throws Talk2MeException
     */
    public PackageItems getPackageItems(String sha, Guest guest) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_PACKAGEITEM);
        request.addProperty("p_strSHA", sha);
        request.addProperty("P_ID_RES_RES_KOP", guest.getId_RES_RES_KOP());
        request.addProperty("p_ID_RES_RES_DLN", guest.getId_RES_RES_DLN());

        SoapObject o = call(request, METHOD_GET_POS_PACKAGEITEM);
        return new PackageItems(o);
    }

    /**
     * Get the article groups
     * 
     * @return
     * @throws Talk2MeException
     */
    public List<ArticleGroup> getArticleGroups(String sha, Division division, int locationId) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_GROUP);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_ID_POS_PLU_MainGroup", division.getIdPosPluMainGroup());
        request.addProperty("p_ID_POS_Layout", locationId);

        SoapObject groups = call(request, METHOD_GET_POS_GROUP);
        ArrayList<ArticleGroup> subGroups = new ArrayList<ArticleGroup>();

        Object o = groups.getProperty(1);
        if (o instanceof SoapObject) {
            SoapObject rows = (SoapObject) o;
            int count = rows.getPropertyCount();

            for (int i = 0; i < count; i++) {
                subGroups.add(new ArticleGroup((SoapObject) rows.getProperty(i)));
            }
        }

        Collections.sort(subGroups, new Comparator<ArticleGroup>() {
            @Override
            public int compare(ArticleGroup o1, ArticleGroup o2) {
                return o1.getOrder() - o2.getOrder();
            }
        });

        return subGroups;
    }

    /**
     * 
     * @param location
     * @param group
     * @return
     * @throws Talk2MeException
     */
    public List<Article> getArticles(String sha, Division division, Location location, ArticleGroup group) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_PLU);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_ID_POS_PLU_MainGroup", division.getIdPosPluMainGroup());
        request.addProperty("p_ID_POS_Layout", location.id);
        request.addProperty("p_intGroupNumber", group == null ? "0" : group.getGroupNumber());

        SoapObject articleGroup = call(request, METHOD_GET_POS_PLU);
        ArrayList<Article> articles = new ArrayList<Article>();

        Object o = articleGroup.getProperty(1);
        if (o instanceof SoapObject) {
            SoapObject articleRows = (SoapObject) o;
            int count = articleRows.getPropertyCount();

            for (int i = 0; i < count; i++) {
                Article article = new Article((SoapObject) articleRows.getProperty(i));
                articles.add(article);
            }
        }

        return articles;
    }

    /**
     * Get the article choices (for example Medium/Rare/Well-Done
     * 
     * @param sha
     * @param article
     * @return
     * @throws Talk2MeException
     */
    public ArticleOptions getArticleOptions(String sha, Article article) throws Talk2MeException {
        return getArticleOptions(sha, article.id);
    }

    /**
     * Get the article choices (for example Medium/Rare/Well-Done
     * 
     * @param sha
     * @param article
     * @return
     * @throws Talk2MeException
     */
    public ArticleOptions getArticleOptions(String sha, int idArticle) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_PLU_CHOICES);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_ID_POS_PLU", idArticle);
        SoapObject o = call(request, METHOD_GET_POS_PLU_CHOICES);
        return new ArticleOptions(o);
    }

    /**
     * 
     * @param location
     * @param table
     * @param article
     * @throws Talk2MeException
     */
    public void setTableOrders(String sha, User user, Device device, Location location, Table table, OrderAction action) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_SET_POS_TABLE_ORDER);

        request.addProperty("p_strSHA", sha);
        request.addProperty("p_intControlID", controlIdPOS);
        request.addProperty("p_Action", action.id);
        request.addProperty("p_ID_SYS_Division", user.getIdDefaultDivision());
        request.addProperty("p_ID_SYS_UserRevenue", user.id);
        request.addProperty("p_ID_POS_Layout", location.id);
        request.addProperty("p_ID_POS_Table", table.id);
        request.addProperty("p_ID_SYS_Device", device.id);

        SoapObject order = new SoapObject(this.namespace, "ttPOS_TableOrder");

        List<POSGuest> guests = table.getGuests();
        for (POSGuest g : guests) {
            List<TableOrderRow> orders = table.getOrders(g);
            for (TableOrderRow row : orders) {
                if (g != null) {
                    row.setIdGuest(g.id);
                }
                order.addSoapObject(row.createSoapObject(this));
                System.out.println(row.id + " " + row);
            }
        }
        request.addSoapObject(order);

        controlIdPOS++;

        call(request, METHOD_SET_POS_TABLE_ORDER);
    }

    /**
     * Update the status of the table
     * 
     * @param sha
     *            The SHA string
     * @param device
     *            The Device
     * @param location
     *            The Location
     * @param table
     *            The Table
     * @param status
     *            The status to set the Table to
     * @throws Talk2MeException
     */
    public void updateTableStatus(String sha, Device device, Location location, Table table, TableStatusAction status, Division division)
            throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_UPD_POS_TABLE);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_intControlID", controlIdPOS);
        request.addProperty("p_Action", status.id);
        request.addProperty("p_ID_SYS_Division", division.id);
        request.addProperty("p_ID_SYS_Device", device.id);
        request.addProperty("p_ID_POS_Layout", location.id);
        request.addProperty("p_ID_POS_Table", table.id);
        request.addProperty("p_ID_POS_Course", table.getIdCourse());

        controlIdPOS++;

        call(request, METHOD_UPD_POS_TABLE);
    }

    /**
     * Get general information about the hotel room
     * 
     * @param sha
     * @param device
     * @return
     * @throws Talk2MeException
     */
    public HotelRoomInfo getHotelRoomInfo(String sha, Device device) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_HTL_ROOMINFO);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_ID_SYS_Device", device.id);
        SoapObject o = call(request, METHOD_GET_HTL_ROOMINFO);
        HotelRoomInfo hotelRoomInfo = new HotelRoomInfo(o);

        if (hotelRoomInfo.isBooked()) {
            request = new SoapObject(this.namespace, METHOD_GET_HTL_GUEST);
            request.addProperty("p_strSHA", sha);
            request.addProperty("p_ID_HTL_OVR_KOP", hotelRoomInfo.getIdBooking());
            o = call(request, METHOD_GET_HTL_GUEST);
            HotelGuestResponse hgr = new HotelGuestResponse(o);
            hotelRoomInfo.setHotelGuestResponse(hgr);
        }

        return hotelRoomInfo;
    }

    /**
     * Get the activities that are available on a certain date
     * 
     * @param resSession
     * @param date
     * @param reference
     * @return
     * @throws Talk2MeException
     */
    public ActivitiesResponse getActivities(ResSession resSession, String reference, Talk2MeDate date) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_ACTIVITY);
        request.addProperty("p_strSHA", resSession.getSha());
        request.addProperty("p_strReference", reference);
        request.addProperty("p_date", date.getValue());
        request.addProperty("p_blnDisciplines", -1);
        SoapObject o = call(request, METHOD_GET_RES_ACTIVITY);
        return new ActivitiesResponse(o);
    }

    /**
     * Get general settings
     * 
     * @param sha
     * @return
     * @throws Talk2MeException
     */
    public Settings getSettings(String sha) throws Talk2MeException {
        if (settings == null || !sha.equalsIgnoreCase(settings.sha)) {
            SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_SETTING);
            request.addProperty("p_strSHA", sha);
            SoapObject o = call(request, METHOD_GET_RES_SETTING);
            settings = new Settings(sha, o);
        }
        return settings;
    }

    /**
     * Get the known (dinner-) courses
     * 
     * @param sha
     * @return
     * @throws Talk2MeException
     */
    public Courses getCourses(String sha) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_POS_COURSE);
        request.addProperty("p_strSHA", sha);
        SoapObject o = call(request, METHOD_GET_POS_COURSE);
        return new Courses(o);
    }

    /**
     * Get the session for SPA functionality
     * 
     * @param p_ID_SYS_ADM
     * @return
     * @throws Talk2MeException
     */
    public ResSession getResSession(int p_ID_SYS_ADM, Device device) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_SESSION);
        request.addProperty("p_ID_SYS_ADM", p_ID_SYS_ADM);
        request.addProperty("p_ID_SYS_Device", device.id);

        ResSession rs = new ResSession(call(request, METHOD_GET_RES_SESSION));

        controlIdRES = rs.lastControlId + 1;

        return rs;
    }

    /**
     * Get a time proposal for 1 activity on a chosen date
     * 
     * @param resSession
     * @param date
     * @param activity
     * @return
     * @throws Talk2MeException
     */
    public ProposalResponse getProposal(ResSession resSession, Talk2MeDate date, Activity activity) throws Talk2MeException {

        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_PROPOSAL);
        request.addProperty("p_strSHA", resSession.getSha());
        request.addProperty("p_res_dat", date.getValue());
        request.addProperty("p_ref_tmc_beg", "");
        request.addProperty("p_ref_tmc_end", "");
        request.addProperty("p_blnSameUser", -1);
        request.addProperty("p_lstID_RES_ONL_INE", "0");
        request.addProperty("p_lstID_RES_AKT", Integer.toString(activity.id));
        request.addProperty("p_lstTMC_BEG", "");
        request.addProperty("p_lstTMC_END", "");
        request.addProperty("p_lstQUANTITYGUEST", "1");
        request.addProperty("p_lstQUANTITYOBJECT", "1");
        request.addProperty("p_lstID_RES_ZLN", "");
        request.addProperty("p_lstADDITIONALTEXT", "");

        SoapObject table = new SoapObject(this.namespace, "ttRES_Proposal");
        request.addSoapObject(table);

        SoapObject o = call(request, METHOD_GET_RES_PROPOSAL);

        return new ProposalResponse(o);
    }

    public List<ProposalResponse> getProposals(ResSession resSession, Chart chart) throws Talk2MeException {
        List<ProposalResponse> proposals = new ArrayList<ProposalResponse>();

        Map<Talk2MeDate, List<ChartRow>> map = chart.getDateRows();
        Set<Talk2MeDate> dates = map.keySet();
        for (Talk2MeDate posDate : dates) {
            List<ChartRow> rows = map.get(posDate);

            ProposalResponse o = getProposal(resSession, posDate, rows);
            proposals.add(o);
        }

        return proposals;
    }

    public ProposalResponse updateProposal(ResSession resSession, Talk2MeDate posDate, Proposal proposal) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_UPD_RES_PROPOSAL);
        request.addProperty("p_strSHA", resSession.getSha());
        request.addProperty("p_intControlID", controlIdRES);
        request.addProperty("p_res_dat", posDate.getValue());

        SoapObject table = new SoapObject(this.namespace, "ttRES_Proposal");
        for (ProposalRow row : proposal.getProposalRows()) {
            if (row.getDate().equals(posDate)) {
                table.addSoapObject(row.createSoapObject(true));
            }
        }

        request.addSoapObject(table);

        controlIdRES++;

        return new ProposalResponse(call(request, METHOD_UPD_RES_PROPOSAL, new MarshalDecimal()));
    }

    public ProposalResponse getProposal(ResSession resSession, Talk2MeDate posDate, List<ChartRow> rows) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_PROPOSAL);
        String p_lstID_RES_ONL_INE = "";
        String p_lstID_RES_AKT = "";
        String p_lstTMC_BEG = "";
        String p_lstTMC_END = "";
        String p_lstQUANTITYGUEST = "";
        String p_lstQUANTITYOBJECT = "";
        String p_lstID_RES_ZLN = "";
        String p_lstADDITIONALTEXT = "";

        boolean notFirst = false;
        for (ChartRow row : rows) {
            if (notFirst) {
                p_lstID_RES_ONL_INE += ",";
            }
            p_lstID_RES_ONL_INE += row.getIdResOnline();

            if (notFirst) {
                p_lstID_RES_AKT += ",";
            }
            p_lstID_RES_AKT += row.getIdActivity();

            if (notFirst) {
                p_lstTMC_BEG += ",";
            }
            // p_lstTMC_BEG += row.getTimeBegin();

            if (notFirst) {
                p_lstTMC_END += ",";
            }
            // p_lstTMC_END += row.getTimeEnd();

            if (notFirst) {
                p_lstQUANTITYGUEST += ",";
            }
            p_lstQUANTITYGUEST += "1";

            if (notFirst) {
                p_lstQUANTITYOBJECT += ",";
            }
            p_lstQUANTITYOBJECT += "1";

            if (notFirst) {
                p_lstID_RES_ZLN += ",";
            }
            if (notFirst) {
                p_lstADDITIONALTEXT += ",";
            }
            p_lstADDITIONALTEXT += "-";

            notFirst = true;
        }

        request.addProperty("p_strSHA", resSession.getSha());
        request.addProperty("p_res_dat", posDate.getValue());
        request.addProperty("p_ref_tmc_beg", "");
        request.addProperty("p_ref_tmc_end", "");
        request.addProperty("p_blnSameUser", -1);
        request.addProperty("p_lstID_RES_ONL_INE", p_lstID_RES_ONL_INE);
        request.addProperty("p_lstID_RES_AKT", p_lstID_RES_AKT);
        request.addProperty("p_lstTMC_BEG", p_lstTMC_BEG);
        request.addProperty("p_lstTMC_END", p_lstTMC_END);
        request.addProperty("p_lstQUANTITYGUEST", p_lstQUANTITYGUEST);
        request.addProperty("p_lstQUANTITYOBJECT", p_lstQUANTITYOBJECT);
        request.addProperty("p_lstID_RES_ZLN", p_lstID_RES_ZLN);
        request.addProperty("p_lstADDITIONALTEXT", p_lstADDITIONALTEXT);

        SoapObject table = new SoapObject(this.namespace, "ttRES_Proposal");
        request.addSoapObject(table);

        ProposalResponse o = new ProposalResponse(call(request, METHOD_GET_RES_PROPOSAL));
        return o;
    }

    public void addProposalsToChart(ResSession session, Talk2MeDate date, List<ProposalRow> rows) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_ADD_RES_PRO2CHART);
        request.addProperty("p_strSHA", session.getSha());
        request.addProperty("p_intControlID", controlIdRES);
        request.addProperty("p_intProposal", rows.get(0).getProposal());
        request.addProperty("p_date", date.getValue());
        request.addProperty("p_intRepeat", 0);
        request.addProperty("p_ID_SYS_Repetence", 0);

        SoapObject resProposal = new SoapObject(this.namespace, "ttRES_Proposal");
        {
            for (ProposalRow r : rows) {
                if (r.getDate().equals(date)) {
                    resProposal.addSoapObject(r.createSoapObject());
                }
            }
        }
        request.addSoapObject(resProposal);

        controlIdRES++;

        call(request, METHOD_ADD_RES_PRO2CHART, new MarshalDecimal());
    }

    public AAA2ChartResponse addAAA2Chart(ResSession session, HotelGuest guest, Activity activity, Talk2MeDate date) throws Talk2MeException {

        SoapObject request = new SoapObject(this.namespace, METHOD_ADD_RES_AAA2CHART);
        request.addProperty("p_strSHA", session.getSha());
        request.addProperty("p_intControlID", controlIdRES);
        request.addProperty("p_date", date.getValue());
        request.addSoapObject(new SoapObject(this.namespace, "ttRES_Package"));
        request.addSoapObject(new SoapObject(this.namespace, "ttRES_PackageComposition"));

        SoapObject resActivity = new SoapObject(this.namespace, "ttRES_Activity");
        {
            SoapObject row = new SoapObject(this.namespace, "ttRES_ActivityRow");
            row.addProperty("ID", 0);
            row.addProperty("ID_RES_AKT_GRP", activity.idGroup);
            row.addProperty("akt_grp_kd_", activity.getCodeGroup());
            row.addProperty("akt_kd_", activity.getCode());
            row.addProperty("strNamePrimary", "");
            row.addProperty("strNameSecundary", "");
            row.addProperty("strNameFull", activity.getName());
            row.addProperty("tmc_beg", "");
            row.addProperty("tmc_end", "");
            row.addProperty("intQuantity", 1);
            row.addProperty("strAdditionalInfo", "-");
            row.addProperty("dblPrice", activity.getPriceCents() / 100d);
            row.addProperty("datValidFrom", date.getValue());
            row.addProperty("datValidTo", date.getValue());
            row.addProperty("intEmployee", 0);
            row.addProperty("blnSelected", -1);
            row.addProperty("lstGuestID", "" + guest.id);
            row.addProperty("strDuration", 0);
            row.addProperty("strDurationFull", 0);
            row.addProperty("ID_SYS_Icon", 0);
            resActivity.addSoapObject(row);

            request.addSoapObject(resActivity);
        }

        request.addSoapObject(new SoapObject(this.namespace, "ttRES_ActivityComposition"));
        request.addSoapObject(new SoapObject(this.namespace, "ttRES_Addition"));

        controlIdRES++;

        SoapObject o = call(request, METHOD_ADD_RES_AAA2CHART, new MarshalDecimal());

        return new AAA2ChartResponse(o);
    }

    public void updateReservation(ResSession session, ReservationState state, int idRelation, int contactId) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_UPD_RES_RESERVATION);
        request.addProperty("p_strSHA", session.getSha());
        request.addProperty("p_intControlID", controlIdRES);
        request.addProperty("p_intOrderNumber", session.getOrderNumber());
        request.addProperty("p_intState", state.id);
        request.addProperty("p_ID_REL_Relation", idRelation);
        request.addProperty("p_ID_REL_Contact", contactId);

        controlIdRES++;

        call(request, METHOD_UPD_RES_RESERVATION);
    }

    public ReservationResponse getReservation(ResSession session, ReservationState state) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_RESERVATION);
        request.addProperty("p_strSHA", session.getSha());
        request.addProperty("p_intOrderNumber", session.getOrderNumber());
        request.addProperty("p_intState", state.id);

        return new ReservationResponse(call(request, METHOD_GET_RES_RESERVATION));
    }

    public ReservationDetailResponse getReservationDetails(String sha, Guest guest) throws Talk2MeException {
        return getReservationDetails(sha, new Talk2MeDate(), guest);
    }

    public ReservationDetailResponse getReservationDetails(String sha, Talk2MeDate date, Guest guest) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_RESERVATION_DETAILS);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_intOrderNumber", 0);
        request.addProperty("p_ID_HTL_OVR_KOP", 0);
        request.addProperty("p_res_dat", date.getValue());
        request.addProperty("p_ID_RES_RES_KOP", guest == null ? 0 : guest.getId_RES_RES_KOP());
        request.addProperty("P_ID_RES_RES_DLN", guest == null ? 0 : guest.getId_RES_RES_DLN());

        return new ReservationDetailResponse(call(request, METHOD_GET_RES_RESERVATION_DETAILS));
    }

    public ReservationDetailResponse getReservationDetails(ResSession session, HotelRoomInfo hotelRoomInfo, Talk2MeDate date, Guest guest)
            throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_RESERVATION_DETAILS);
        request.addProperty("p_strSHA", session.getSha());
        request.addProperty("p_intOrderNumber", session.getOrderNumber());
        request.addProperty("p_ID_HTL_OVR_KOP", hotelRoomInfo == null ? 0 : hotelRoomInfo.getIdBooking());
        request.addProperty("p_res_dat", date.getValue());
        request.addProperty("p_ID_RES_RES_KOP", guest == null ? 0 : guest.getId_RES_RES_KOP());
        request.addProperty("P_ID_RES_RES_DLN", guest == null ? 0 : guest.getId_RES_RES_DLN());

        return new ReservationDetailResponse(call(request, METHOD_GET_RES_RESERVATION_DETAILS));
    }

    public ChartResponse getChart(ResSession session) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_RES_CHART);
        request.addProperty("p_strSHA", session.getSha());

        return new ChartResponse(call(request, METHOD_GET_RES_CHART));
    }

    public void delChart(ResSession session, List<ChartRow> rows) throws Talk2MeException {
        for (ChartRow row : rows) {
            delChart(session, row);
        }
    }

    public void delChart(ResSession session, ChartRow row) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_DEL_RES_CHART);
        request.addProperty("p_strSHA", session.getSha());
        request.addProperty("p_intControlID", controlIdRES);
        request.addProperty("p_ID_ttRES_Chart", row.id);

        controlIdRES++;

        call(request, METHOD_DEL_RES_CHART);
    }

    public void delChart(ResSession session) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_DEL_RES_CHART);
        request.addProperty("p_strSHA", session.getSha());
        request.addProperty("p_intControlID", controlIdRES);
        request.addProperty("p_ID_ttRES_Chart", 0);

        controlIdRES++;

        call(request, METHOD_DEL_RES_CHART);
    }

    /**
     * Receive notes
     * 
     * @param sha
     * @param guest
     * @param idRelation
     * @param idResResReg
     * @param read
     * @param handled
     * @return
     * @throws Talk2MeException
     */
    public NoteResponse getNote(String sha, boolean keepAlive, Guest guest, int idRelation, int idResResReg, NoteReadState read, NoteHandledState handled)
            throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_GET_NOT_NOTE);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_logAutoPolling", !keepAlive);
        request.addProperty("p_ID_REL_Relation", idRelation);
        request.addProperty("p_ID_RES_RES_KOP", guest == null ? 0 : guest.getId_RES_RES_KOP());
        request.addProperty("P_ID_RES_RES_REG", idResResReg);
        request.addProperty(NoteReadState.key, read.value);
        request.addProperty(NoteHandledState.key, handled.value);

        return new NoteResponse(call(request, METHOD_GET_NOT_NOTE));
    }

    /**
     * Send a note
     * 
     * @param sha
     * @param action
     * @param p_ID_SYS_Module
     * @param priority
     * @param subject
     * @param body
     * @param idNoteKind
     * @param idUserFrom
     * @param idUserTo
     * @param idRelation
     * @param p_ID_RES_RES_KOP
     * @param p_ID_RES_RES_REG
     * @param p_ID_RES_RES_PLU
     * @param p_ID_RES_RES_DLN
     * @param p_ID_NOT_Note
     * @return
     * @throws Talk2MeException
     */
    public AddNoteResponse addNote(String sha, int action, int p_ID_SYS_Module, int priority, String subject, String body, int idNoteKind, int idUserFrom,
            int idUserTo, int idRelation, Guest guest, int idResResReg, int isResResPlu, int idResResDln, int idNote) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_ADD_NOT_NOTE);

        request.addProperty("p_strSHA", sha);
        request.addProperty("p_intAction", action); // 1 - toevoegen, 2
                                                    // verwijderen, 3 - wijzig
        request.addProperty("p_ID_SYS_Module", p_ID_SYS_Module); // 4 - voor POS
        request.addProperty("p_ID_SYS_Priority", priority);
        request.addProperty("p_strSubject", subject);
        request.addProperty("p_strBody", body);
        request.addProperty("p_ID_NOT_NoteKind", idNoteKind); // 0
        request.addProperty("p_ID_SYS_UserFrom", idUserFrom);
        request.addProperty("p_ID_SYS_UserTo", idUserTo);
        request.addProperty("p_ID_REL_Relation", idRelation); // 0
        request.addProperty("p_ID_RES_RES_KOP", guest == null ? 0 : guest.getId_RES_RES_KOP()); // 0
        request.addProperty("p_ID_RES_RES_REG", idResResReg); // 0
        request.addProperty("p_ID_RES_RES_PLU", isResResPlu); // 0
        request.addProperty("p_ID_RES_RES_DLN", idResResDln); // 0
        request.addProperty("p_ID_NOT_Note", idNote); // 0 bij nieuwe note

        return new AddNoteResponse(call(request, METHOD_ADD_NOT_NOTE));
    }

    /**
     * Send a note.
     * 
     * @param sha
     * @param p_ID_SYS_Module
     * @param priority
     * @param subject
     * @param body
     * @param idUserFrom
     * @param idUserTo
     * @return
     * @throws Talk2MeException
     */
    public AddNoteResponse addNote(String sha, int p_ID_SYS_Module, int priority, String subject, String body, int idUserFrom, int idUserTo)
            throws Talk2MeException {
        return addNote(sha, 1, p_ID_SYS_Module, priority, subject, body, 0, idUserFrom, idUserTo, 0, null, 0, 0, 0, 0);
    }

    /**
     * Update Note status (read, handled, etc)
     * 
     * @param sha
     * @param note
     * @param read
     * @param handled
     * @throws Talk2MeException
     */
    public void updateNote(String sha, Note note, NoteReadState read, NoteHandledState handled) throws Talk2MeException {
        SoapObject request = new SoapObject(this.namespace, METHOD_UPD_NOT_NOTE);
        request.addProperty("p_strSHA", sha);
        request.addProperty("p_ID_NOT_Note", note.id);
        request.addProperty(NoteReadState.key, read.value);
        request.addProperty(NoteHandledState.key, handled.value);

        call(request, METHOD_UPD_NOT_NOTE);
    }

}
