package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * This class represents booking info about a guest on a date.
 * 
 * @author erik
 */
public class HotelGuest extends Talk2MeBaseType implements Guest {

    public final int id;

    private final Talk2MeDate datReservation;
    private final int id_RES_RES_KOP;
    private final String name;
    private final int id_RES_TAG;
    private final String tagCode;
    private final String tagIdentification;

    //    private final int intSessionGuestID;

    public HotelGuest(SoapObject o) {
        this.id = Integer.parseInt(o.getPropertyAsString("ID"));
        this.datReservation = new Talk2MeDate(o.getPropertyAsString("datReservation"));
        this.id_RES_RES_KOP = Integer.parseInt(o.getPropertyAsString("ID_RES_RES_KOP"));
        this.name = o.getPropertyAsString("strName");
        this.id_RES_TAG = Integer.parseInt(o.getPropertyAsString("ID_RES_TAG"));
        this.tagCode = o.getPropertyAsString("strTagCode");
        this.tagIdentification = o.getPropertyAsString("strTagIdentification");
        //        this.intSessionGuestID = Integer.parseInt(o.getPropertyAsString("intSessionGuestID"));
    }

    /**
     * Get the date of the booking for this guest
     * 
     * @return
     */
    public Talk2MeDate getReservationDate() {
        return datReservation;
    }

    @Override
    public int getId_RES_RES_KOP() {
        return id_RES_RES_KOP;
    }

    @Override
    public int getId_RES_RES_DLN() {
        return id;
    }

    //    public int getIdSessionGuest() {
    //        return intSessionGuestID;
    //    }

    /**
     * Get the name of the guest
     */
    @Override
    public String getName() {
        return name;
    }

    public int getId_RES_TAG() {
        return id_RES_TAG;
    }

    /**
     * Get the key Tag code (manual)
     */
    @Override
    public String getTagCode() {
        return tagCode;
    }

    /**
     * Get the NFC tag id
     */
    @Override
    public String getTagIdentification() {
        return tagIdentification;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && (o instanceof HotelGuest)) {
            return ((HotelGuest) o).id == id;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
