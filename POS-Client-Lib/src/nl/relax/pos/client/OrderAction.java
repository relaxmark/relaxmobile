package nl.relax.pos.client;

public enum OrderAction {

    ORDER(1), FREE(2), MOVE(4);

    public final int id;

    OrderAction(int id) {
        this.id = id;
    }
}