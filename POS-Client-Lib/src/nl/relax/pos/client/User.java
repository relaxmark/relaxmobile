package nl.relax.pos.client;

import org.ksoap2.serialization.SoapObject;

/**
 * A user of the application
 * 
 * @author erik
 */
public class User {
    public final int id;
    private final String name;
    private final int idDefaultDivision;
    private final int language;
    private final boolean leftHanded;

    User(SoapObject o) {
        this.id = Integer.parseInt(o.getProperty("ID").toString());
        this.name = o.getProperty("strName").toString();
        this.idDefaultDivision = Integer.parseInt(o.getProperty("ID_SYS_Division").toString());
        this.language = Integer.parseInt(o.getProperty("ID_SYS_Language").toString());
        this.leftHanded = !"0".equals(o.getProperty("blnLeftHanded").toString());
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the idDefaultDivision
     */
    public int getIdDefaultDivision() {
        return idDefaultDivision;
    }

    /**
     * @return the language
     */
    public int getLanguage() {
        return language;
    }

    /**
     * @return the leftHanded
     */
    public boolean isLeftHanded() {
        return leftHanded;
    }
}
