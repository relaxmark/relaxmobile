package nl.relax.pos;

import nl.relax.pos.client.Article;
import nl.relax.pos.client.ArticleGroup;
import nl.relax.pos.client.Device;
import nl.relax.pos.client.Division;
import nl.relax.pos.client.DivisionData;
import nl.relax.pos.client.DivisionResponse;
import nl.relax.pos.client.Guest;
import nl.relax.pos.client.License;
import nl.relax.pos.client.Location;
import nl.relax.pos.client.POSClient;
import nl.relax.pos.client.Table;
import nl.relax.pos.client.User;
import nl.relax.pos.client.UserResponse;

/**
 * Global constants and variables to keep track of the current session.
 * 
 * @author erik
 */
class SessionContext {

	// ------------------------------------------------------------------------------
	// Set at EntreeActivity
	// ------------------------------------------------------------------------------

	/**
	 * The client to connect to the server.
	 */
	volatile static POSClient client;

	/**
	 * The License info
	 */
	volatile static License license;

	/**
	 * The Device info
	 */
	volatile static Device device;

	/**
	 * The list of users
	 */
	volatile static UserResponse users;

	/**
	 * The list of divisions
	 */
	volatile static DivisionResponse divisions;

	/**
	 * The MAC address of the device's WIFI adapter
	 */
	volatile static String macAddress;

	// ------------------------------------------------------------------------------
	// Set at LoginActivity (or EntreeActivity if login is skipped)
	// ------------------------------------------------------------------------------
	/**
	 * Your session ID, which you will be assigned after logging on
	 */
	volatile static String sha;

	/**
	 * The user id that is logged in
	 */
	volatile static User user;

	/**
	 * The user is logged into this division
	 */
	volatile static Division division;

	// ------------------------------------------------------------------------------
	// Set at LocationActivity
	// ------------------------------------------------------------------------------
	/**
	 * The LayoutRow we're currently in (for example "Restaurant")
	 */
	volatile static Location curLocation;

	/**
	 * The division data has access to all locations, article groups and
	 * articles
	 */
	volatile static DivisionData divisionData;

	// ------------------------------------------------------------------------------
	// Set at TablesActivity
	// ------------------------------------------------------------------------------
	/**
	 * The Table we're currently in (for example table 3 in the Restaurant)
	 */
	volatile static Table curTable;

	// ------------------------------------------------------------------------------
	// Set at TableActivity
	// ------------------------------------------------------------------------------
	/**
	 * The Guest on the table we've selected
	 */
	volatile static Guest curGuest;

	// ------------------------------------------------------------------------------
	// Set at ArticleGroupsActivity
	// ------------------------------------------------------------------------------
	/**
	 * The SubGroup (article group) we're currently in
	 */
	volatile static ArticleGroup curSubGroup;

	// ------------------------------------------------------------------------------
	// Set at AddOrderActivity
	// ------------------------------------------------------------------------------
	volatile static Article curArticle;
}
