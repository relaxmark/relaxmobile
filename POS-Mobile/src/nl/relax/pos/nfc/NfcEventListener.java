package nl.relax.pos.nfc;

public interface NfcEventListener {

    public void notify(NfcEvent e);
}