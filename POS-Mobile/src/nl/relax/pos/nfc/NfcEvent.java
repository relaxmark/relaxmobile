package nl.relax.pos.nfc;

public class NfcEvent {

    final long tagId;

    public NfcEvent(long id) {
        this.tagId = id;
    }

    @Override
    public String toString() {
        return Long.toString(tagId);
    }
}
