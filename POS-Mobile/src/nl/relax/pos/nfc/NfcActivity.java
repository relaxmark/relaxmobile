package nl.relax.pos.nfc;

import nl.relax.pos.PosActivity;
import nl.relax.pos.PosApplication;
import nl.relax.pos.client.Setting;
import nl.relax.pos.client.Settings;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NfcAdapter;
import android.nfc.tech.MifareClassic;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.Toast;

public abstract class NfcActivity extends PosActivity implements NfcEventListener {

    private NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;
    private IntentFilter[] filters;
    private String[][] techLists;
    private boolean mifare = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initNfc();
    }

    private void initNfc() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());

        Settings settings = ((PosApplication) getApplication()).settings;

        if (settings != null) {
            Setting setting = ((PosApplication) getApplication()).settings.get(Setting.Key.INTEGER_5_TAG_IDENTIFICATION);
            mifare = setting.getInteger() == 3 || setting.getInteger() == 5;
        } else {
            mifare = true;
        }

        try {
            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

            IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
            try {
                ndef.addDataType("*/*");
            } catch (MalformedMimeTypeException e) {
                throw new RuntimeException("fail", e);
            }

            filters = new IntentFilter[] { ndef, };
            techLists = new String[][] { new String[] { MifareClassic.class.getName() } };
        } catch (Throwable t) {
            System.out.println("Throwable " + t + ": NFC not supported?");
            t.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, filters, techLists);
        } catch (Throwable t) {
            System.out.println("Throwable " + t + ": NFC not supported?");
            t.printStackTrace();
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        resolveIntent(intent);
    }

    protected boolean isTagIntent() {
        return isTagIntent(getIntent());
    }

    protected boolean isTagIntent(Intent intent) {
        try {
            return mifare && NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction());
        } catch (Throwable t) {
            System.out.println("Throwable " + t + ": NFC not supported?");
            t.printStackTrace();
            return false;
        }
    }

    private void resolveIntent(Intent intent) {

        if (mifare && isTagIntent(intent)) {
            Bundle b = intent.getExtras();
            if (b != null) {
                byte[] bid = b.getByteArray(NfcAdapter.EXTRA_ID);
                long id = 0;
                for (int i = bid.length - 1; i >= 0; i--) {
                    id <<= 8;
                    id |= bid[i] & 0xff;
                }
                final long _id = id;
                System.out.println("**** ID=" + id);

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        _notify(new NfcEvent(_id));
                        return null;
                    }
                }.execute();

            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            nfcAdapter.disableForegroundDispatch(this);
        } catch (Throwable t) {
            System.out.println("Throwable " + t + ": NFC not supported?");
            t.printStackTrace();
        }
    }

    private void _notify(NfcEvent e) {
        notify(e);
    }

    @Override
    public void notify(NfcEvent e) {
        Toast.makeText(getApplicationContext(), e + " herkend.", Toast.LENGTH_SHORT).show();
    }
}
