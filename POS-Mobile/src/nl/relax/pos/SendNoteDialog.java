package nl.relax.pos;

import nl.relax.pos.client.Note;
import nl.relax.pos.client.Talk2MeException;
import nl.relax.pos.client.User;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SendNoteDialog extends Dialog {

    /**
     * The context
     */
    private final PosActivity activity;

    /**
     * The Note that's being answered to
     */
    private final Note answerTo;

    /**
     * Constructor to send a new note
     * 
     * @param context
     */
    public SendNoteDialog(final PosActivity context) {
        this(context, null);
    }

    /**
     * Constructor to send a note
     * 
     * @param context
     *            The Context
     * @param answerTo
     *            The Note to answer or <code>null</code> to send a new note.
     */
    public SendNoteDialog(final PosActivity context, final Note answerTo) {
        super(context);
        this.activity = context;
        this.answerTo = answerTo;

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        final boolean isAnwering = answerTo != null;

        if (isAnwering) {
            setContentView(R.layout.answer_note_dialog);

            TextView tt = (TextView) findViewById(R.id.txtTitle);
            TextView td = (TextView) findViewById(R.id.txtDateTime);
            td.setText(answerTo.getDateTime().getDateTimeFormat());

            if (answerTo.getPriority() >= 3) {
                tt.setBackgroundColor(0xff800000);
                tt.setText(answerTo.getSubject() + " (HOGE PRIORITEIT!)");
            } else {
                tt.setBackgroundColor(activity.getResources().getColor(R.color.bg_center));
                tt.setText(answerTo.getSubject());
            }

            TextView tm = (TextView) findViewById(R.id.txtMessage);
            tm.setText("Afzender: " + answerTo.getUserNameFrom() + "\n" + answerTo.getNote());

        } else {
            setContentView(R.layout.new_note_dialog);

            final Spinner spinner = (Spinner) findViewById(R.id.spinner);
            final ArrayAdapter<User> adapter = new ArrayAdapter<User>(activity, R.layout.dropdown_item, activity.app().users.getUsers());
            spinner.setAdapter(adapter);
        }

        setCancelable(true);

        Button btnClose = (Button) findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText edit = (EditText) findViewById(R.id.txtNewMessage);
                String body = edit.getText().toString();
                int from = activity.app().user.id;
                int idSysModule = activity.app().config.getID_SYS_MODULE();
                if (body == null || body.trim().length() == 0) {
                    Toast.makeText(activity, "Bericht mag niet leeg zijn", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isAnwering) {
                    String subject = answerTo.getSubject();
                    if (subject == null || subject.trim().length() == 0) {
                        subject = "(geen onderwerp)";
                    }

                    if (!subject.startsWith("RE:")) {
                        subject = "RE:" + subject;
                    }

                    try {
                        activity.app().client.addNote(activity.app().sha, idSysModule, 1, subject, body, from, answerTo.getUserIdFrom());
                        dismiss();
                        Toast.makeText(activity, "Bericht verstuurd", Toast.LENGTH_SHORT).show();
                    } catch (Talk2MeException e) {
                        activity.showException(e);
                    }
                } else {
                    final Spinner spinner = (Spinner) findViewById(R.id.spinner);
                    int to = ((User) spinner.getSelectedItem()).id;
                    String subject = ((TextView) findViewById(R.id.txtSubject)).getText().toString();
                    if (subject == null || subject.trim().length() == 0) {
                        subject = "(geen onderwerp)";
                    }

                    try {
                        activity.app().client.addNote(activity.app().sha, idSysModule, 1, subject, body, from, to);
                        dismiss();
                        Toast.makeText(activity, "Bericht verstuurd", Toast.LENGTH_SHORT).show();
                    } catch (Talk2MeException e) {
                        activity.showException(e);
                    }
                }
            }
        });
    }

    @Override
    public void show() {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.FILL_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        super.show();
        getWindow().setAttributes(lp);
    }
}
