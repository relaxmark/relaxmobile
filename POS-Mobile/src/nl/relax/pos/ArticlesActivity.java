package nl.relax.pos;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import nl.relax.pos.client.Article;
import nl.relax.pos.client.ArticleOptions;
import nl.relax.pos.client.POSGuest;
import nl.relax.pos.client.Table;
import nl.relax.pos.client.TableOrderRow;
import nl.relax.pos.client.Talk2MeException;
import nl.relax.pos.nfc.NfcActivity;
import nl.relax.pos.nfc.NfcEvent;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class ArticlesActivity extends NfcActivity {
    final Handler fatal = new ErrorHandler(this, true);

    private List<Article> articles;
    // private final Handler fatal = new ErrorHandler(this, true);
    // private final Handler nonfatal = new ErrorHandler(this, false);

    private TextView txtUnordered;

    private ArticlesAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app().curActivity = this;
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        setContentView(R.layout.orderactivity2_grid);
        super.initBackgroundWorker(true, true, true);
        setTitle(app().license.getName() + " | " + app().user.getName());

        TextView tv1 = (TextView) findViewById(R.id.curLoc);
        tv1.setText(app().curSubGroup.getName());

        final Button backToTable = (Button) findViewById(R.id.btnBackToTable);
        backToTable.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                app().backToTable = true;
                finish();
            }
        });

        final EditText search = (EditText) findViewById(R.id.editSearch);
        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String ss = search.getText().toString().trim();
                changeFilter(ss);
            }
        });
        search.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(search.getWindowToken(), 0);

                }
                return false;
            }
        });

        txtUnordered = (TextView) findViewById(R.id.unordered);

        try {
            this.articles = app().divisionData.getArticles(app().sha, app().curSubGroup, app().curLocation);
            Collections.sort(articles, new Comparator<Article>() {
                @Override
                public int compare(Article article1, Article article2) {
                    return article1.getName().compareToIgnoreCase(article2.getName());
                }
            });
        } catch (Talk2MeException e) {
            showExceptionAndFinish(e);
        }

        AbsListView lv1 = (AbsListView) findViewById(R.id.articles_grid);
        adapter = new ArticlesAdapter(this);

        changeFilter("");

        lv1.setClickable(true);
        lv1.setAdapter(adapter);

        lv1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                if (articles.get(position).isAvailable()) {
                    app().curArticle = articles.get(position);
                    order((Article) adapter.getItem(position), 1);
                } else {
                    showNotAvailable();
                }
            }
        });
    }

    protected void changeFilter(String s) {
        if (articles == null) {
            return;
        }

        adapter.clear();
        if (s == null || s.length() == 0) {
            for (int i = 0; i < articles.size(); i++) {
                Article a = articles.get(i);
                if (a.getGroupNumber() == app().curSubGroup.getGroupNumber()) {
                    adapter.add(a);
                }
            }
        } else {
            s = s.toUpperCase();
            String _s = " " + s;
            for (int i = 0; i < articles.size(); i++) {
                Article a = articles.get(i);
                if (a.getGroupNumber() == app().curSubGroup.getGroupNumber()) {
                    String upper = a.getName().toUpperCase();
                    if (upper.startsWith(s) || upper.indexOf(_s) >= 0) {
                        adapter.add(a);
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        app().curActivity = this;
        updateUnorderedCount();
    }

    private void updateUnorderedCount() {
        if (txtUnordered != null) {
            txtUnordered.setText("(" + app().curTable.getUnorderedCount(app().curGuest) + ")");
        }
    }

    protected void order(final Article article, final int count) {
        if (article.isAskForName() || article.isAskForPrice()) {

            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.freearticle);
            dialog.setTitle("Vrije artikelinvoer");

            final EditText aName = (EditText) dialog.findViewById(R.id.freearticlename);
            aName.setEnabled(article.isAskForName());

            final EditText aPrice = (EditText) dialog.findViewById(R.id.freearticleprice);
            aPrice.setText("0,00");
            aPrice.setEnabled(article.isAskForPrice());

            Button button = (Button) dialog.findViewById(R.id.btnFreeArticleOk);
            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO:
                    try {
                        String name = aName.getText().toString();
                        if (article.isAskForName() && (name == null || name.trim().length() == 0)) {
                            throw new Exception("Naam is leeg");
                        }
                        Double price = Double.parseDouble(aPrice.getText().toString().replace(',', '.'));
                        TableOrderRow row = new TableOrderRow(app().curLocation.id, app().curTable.id, article.id, name, count, (int) (price * 100), null);
                        order2(article, row, true);
                        dialog.cancel();
                    } catch (Exception e) {
                        showException(e);
                    }
                }
            });
            dialog.setCancelable(true);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            dialog.show();
        } else {
            try {
                TableOrderRow row = new TableOrderRow(app().curLocation.id, app().curTable.id, article.id, article.getName(), count,
                        article.getSalesPriceCents(), app().courses.get(article.getIdCourse()));
                order2(article, row, false);
            } catch (Exception e1) {
                showException(e1);
            }
        }
    }

    private void order2(Article article, TableOrderRow row, boolean forceNewRow) throws Talk2MeException {
        if (app().curGuest != null) {
            row.setIdGuest(app().curGuest.id);
        }

        if (article.getIdArticleChoices() > 0) {
            app().curTable.addOrder(app().client, app().sha, app().curGuest, row, true);
            Toast.makeText(getApplicationContext(), article + " toegevoegd.", Toast.LENGTH_SHORT).show();

            showSubMenus(article.id, row, 1);
        } else {
            app().curTable.addOrder(app().client, app().sha, app().curGuest, row, forceNewRow);
            Toast.makeText(getApplicationContext(), article + " toegevoegd.", Toast.LENGTH_SHORT).show();
        }

        updateUnorderedCount();
    }

    /**
     * Show the submenu dialogs.
     * 
     * @param idArticle
     *            The article ID
     * @param parent
     *            The parent row
     * @param level
     *            The hierarchy level
     */
    private void showSubMenus(final int idArticle, final TableOrderRow parent, final int level) {
        try {
            ArticleOptions options = app().client.getArticleOptions(app().sha, idArticle);

            for (final ArticleOptions.Menu menu : options.getMenus()) {
                boolean single = menu.getMaxChoosable() == 1;
                final int idMenu = menu.getIdMenu();

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                if (menu.getMaxChoosable() <= 1) {
                    builder.setTitle("Maak een keuze");
                } else {
                    builder.setTitle("Kies " + menu.getMinChoosable() + " tot " + menu.getMaxChoosable() + " opties");
                }

                if (single) {
                    ArrayAdapter<ArticleOptions.Option> aa = new ArrayAdapter<ArticleOptions.Option>(this, android.R.layout.select_dialog_singlechoice,
                            menu.getOptions());
                    builder.setSingleChoiceItems(aa, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            try {
                                ArticleOptions.Option option = menu.getOptions().get(item);
                                TableOrderRow newParent = new TableOrderRow(app().curLocation.id, app().curTable.id, option.getIdArticle(), option.getName(),
                                        1, (int) (option.getSalesPrice() * 100), 0, parent, level);
                                app().curTable.addOrder(app().client, app().sha, app().curGuest, newParent);
                                dialog.dismiss();
                                if (idMenu > 0) {
                                    showSubMenus(menu.getIdArticle(), parent, level + 1);
                                }
                            } catch (Exception e1) {
                                showException(e1);
                            }
                        }
                    });
                } else {
                    String[] items = new String[menu.getOptions().size()];
                    for (int i = 0; i < items.length; i++) {
                        items[i] = menu.getOptions().get(i).getName();
                    }
                    final boolean[] checked = new boolean[items.length];
                    builder.setMultiChoiceItems(items, checked, new OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                            checked[which] = isChecked;
                        }
                    });
                    builder.setPositiveButton("Ok", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                for (int i = 0; i < checked.length; i++) {
                                    if (checked[i]) {
                                        ArticleOptions.Option option = menu.getOptions().get(i);
                                        TableOrderRow newParent = new TableOrderRow(app().curLocation.id, app().curTable.id, option.getIdArticle(), option
                                                .getName(), 1, (int) (option.getSalesPrice() * 100), 0, parent, level);
                                        app().curTable.addOrder(app().client, app().sha, app().curGuest, newParent);

                                        if (idMenu > 0) {
                                            showSubMenus(menu.getIdArticle(), parent, level + 1);
                                        }

                                    }
                                }
                            } catch (Exception e1) {
                                showException(e1);
                            }

                        }
                    });
                }
                builder.setCancelable(false);

                AlertDialog alert = builder.create();
                alert.show();

            }

        } catch (Talk2MeException e) {
            showExceptionAndFinish(e);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void notify(NfcEvent e) {
        if (!app().curLocation.isGuestRequired()) {
            // only react to NFC if guestRequired
            return;
        }

        Table table = app().curTable;

        final List<POSGuest> guests = table.getGuests();
        for (int i = 0; i < guests.size(); i++) {
            final POSGuest g = guests.get(i);
            if (g != null && g.getTagIdentification().equals(e.toString())) {
                fatal.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), g.getName() + " geselecteerd.", Toast.LENGTH_SHORT).show();
                    }
                });
                table.setSelectedGuestIndex(i);
                app().curGuest = g;

                return;
            }
        }

        try {
            final POSGuest newGuest = app().client.getGuestByTag(app().sha, e.toString(), app().curTable);
            fatal.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), newGuest.getName() + " geselecteerd.", Toast.LENGTH_SHORT).show();
                }
            });

            table.addGuest(app().client, app().sha, newGuest);
            table.setSelectedGuestIndex(table.getGuests().size() - 1);
            app().curGuest = newGuest;

        } catch (Exception e1) {
            showException(e1);
        }
    }

    private void showNotAvailable() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ArticlesActivity.this);
        builder.setMessage("Dit artikel is niet beschikbaar.").setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
