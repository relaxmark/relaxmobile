package nl.relax.pos;

import nl.relax.pos.client.Article;
import nl.relax.pos.client.ArticleGroup;
import nl.relax.pos.client.Courses;
import nl.relax.pos.client.Device;
import nl.relax.pos.client.Division;
import nl.relax.pos.client.DivisionData;
import nl.relax.pos.client.DivisionResponse;
import nl.relax.pos.client.License;
import nl.relax.pos.client.Location;
import nl.relax.pos.client.NoteResponse;
import nl.relax.pos.client.POSGuest;
import nl.relax.pos.client.Talk2MeSessionExpiredException;
import nl.relax.pos.client.SessionExpiredHandler;
import nl.relax.pos.client.Settings;
import nl.relax.pos.client.Table;
import nl.relax.pos.client.Talk2MeClient;
import nl.relax.pos.client.User;
import nl.relax.pos.client.UserResponse;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.widget.Toast;

public class PosApplication extends Application implements SessionExpiredHandler {

    /**
     * The configuration
     */
    volatile POSConfig config;

    // ------------------------------------------------------------------------------
    // Set at EntreeActivity
    // ------------------------------------------------------------------------------

    /**
     * The client to connect to the server.
     */
    volatile Talk2MeClient client;

    /**
     * The License info
     */
    volatile License license;

    /**
     * The Device info
     */
    volatile Device device;

    /**
     * The list of users
     */
    volatile UserResponse users;

    /**
     * The list of divisions
     */
    volatile DivisionResponse divisions;

    /**
     * The MAC address of the device's WIFI adapter
     */
    volatile String macAddress;

    /**
     * The settings
     */
    public volatile Settings settings;

    // ------------------------------------------------------------------------------
    // Set at LoginActivity (or EntreeActivity if login is skipped)
    // ------------------------------------------------------------------------------
    /**
     * Your session ID, which you will be assigned after logging on
     */
    volatile String sha;

    /**
     * The user id that is logged in
     */
    volatile User user;

    /**
     * The user is logged into this division
     */
    volatile Division division;

    // ------------------------------------------------------------------------------
    // Set at LocationActivity
    // ------------------------------------------------------------------------------
    /**
     * The LayoutRow we're currently in (for example "Restaurant")
     */
    volatile Location curLocation;

    /**
     * The division data has access to all locations, article groups and
     * articles
     */
    volatile DivisionData divisionData;

    // ------------------------------------------------------------------------------
    // Set at TablesActivity
    // ------------------------------------------------------------------------------
    /**
     * The Table we're currently in (for example table 3 in the Restaurant)
     */
    volatile Table curTable;

    // ------------------------------------------------------------------------------
    // Set at TableActivity
    // ------------------------------------------------------------------------------
    /**
     * The Guest on the table we've selected
     */
    volatile POSGuest curGuest;

    // ------------------------------------------------------------------------------
    // Set at ArticleGroupsActivity
    // ------------------------------------------------------------------------------
    /**
     * The SubGroup (article group) we're currently in
     */
    volatile ArticleGroup curSubGroup;

    // ------------------------------------------------------------------------------
    // Set at AddOrderActivity
    // ------------------------------------------------------------------------------
    volatile Article curArticle;

    // ------------------------------------------------------------------------------
    // Set when 'back to table' button is pressed
    // ------------------------------------------------------------------------------
    volatile boolean backToTable;

    volatile Activity curActivity;

    volatile Courses courses;

    volatile NoteResponse curNotes;

    @Override
    public void notifySessionExpired(final Talk2MeSessionExpiredException e) {
        curActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(curActivity, e.getMessage(), Toast.LENGTH_LONG).show();
                Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }
}
