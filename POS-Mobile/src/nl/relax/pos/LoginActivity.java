package nl.relax.pos;

import nl.relax.pos.client.Division;
import nl.relax.pos.client.DivisionData;
import nl.relax.pos.client.LoginResponse;
import nl.relax.pos.client.Talk2MeException;
import nl.relax.pos.client.User;
import nl.relax.pos.nfc.NfcEvent;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class LoginActivity extends PosActivity {

    User user;
    Division division;

    Handler handler = new ErrorHandler(this);

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginactivity);
        System.out.println("############################# app()=" + app());
        System.out.println("############################# app().license=" + app().license);

        setTitle(app().license.getName());
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());

        final TextView txtDevice = (TextView) findViewById(R.id.textApparaat);
        txtDevice.setText(check(app().device.getName()));

        final Button btnLogon = (Button) findViewById(R.id.btnLogon);
        btnLogon.setEnabled(false);

        final Spinner spnDivisions = (Spinner) findViewById(R.id.divisionSpinner);
        final ArrayAdapter<Division> adpDivisions = new ArrayAdapter<Division>(this, R.layout.dropdown_item, app().divisions.getDivisions());
        spnDivisions.setAdapter(adpDivisions);

        spnDivisions.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                division = (Division) arg0.getSelectedItem();
                System.out.println(">>>>>> division.setOnItemSelectedListener " + division);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                btnLogon.setEnabled(false);
                division = null;
            }
        });

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        final ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, R.layout.dropdown_item, app().users.getUsers());
        spinner.setAdapter(adapter);

        // if (app().user != null) {
        // // choose the previously logged in user
        // spinner.setSelection(getSelectedUserPosition(app().user));
        // }

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                btnLogon.setEnabled(true);
                user = (User) arg0.getSelectedItem();
                int divisionIndex = app().divisions.indexOf(user.getIdDefaultDivision());
                System.out.println(">>>>> users.setOnItemSelectedListener " + user.getName() + ", d=" + user.getIdDefaultDivision() + ", idx=" + divisionIndex);
                spnDivisions.setSelection(divisionIndex);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                btnLogon.setEnabled(false);
                user = null;
            }
        });

        final EditText txtPass = (EditText) findViewById(R.id.password);
        txtPass.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        doLogon(txtPass);
                        return true;
                    default:
                        break;
                    }
                }
                return false;
            }
        });
        btnLogon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogon(txtPass);
            }
        });
    }

    private void doLogon(final EditText txtPass) {
        System.out.println(">>>>>>>>>>>>> LOGON " + division);

        app().user = user;
        app().division = division;

        final ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "", "Initialiseren...", true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    LoginResponse logonResponse = app().client.login(app().division, user, txtPass.getText().toString().trim(), app().license,
                            app().macAddress, app().config.getID_SYS_MODULE());

                    app().sha = logonResponse.sha;
                    app().courses = app().client.getCourses(app().sha);
                    app().settings = app().client.getSettings(logonResponse.sha);

                    try {
                        app().divisionData = new DivisionData(app().sha, app().client, division, true);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                dialog.cancel();
                                Intent intent = new Intent(getBaseContext(), LocationsActivity.class);
                                startActivity(intent);
                            }
                        });
                    } catch (Talk2MeException e) {
                        handler.post(new Runnable() {

                            @Override
                            public void run() {
                                dialog.cancel();
                            }
                        });

                        showException(e);
                    }
                } catch (final Talk2MeException e) {
                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            dialog.cancel();
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setMessage(e.getMessage()).setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    txtPass.setText("");
                                    dialog.cancel();
                                }
                            }).setTitle("Inloggen is niet gelukt");
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    });
                }
            }
        }).start();
    }

    private int getSelectedUserPosition(User user) {
        int idx = 0;
        for (User u : app().users.getUsers()) {
            if (user != null && u != null && user.id == u.id) {
                return idx;
            }
            idx++;
        }
        return 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        app().curActivity = this;
        ((EditText) findViewById(R.id.password)).setText("");

        if (app().user != null) {
            final Spinner spinner = (Spinner) findViewById(R.id.spinner);
            // choose the previously logged in user
            spinner.setSelection(getSelectedUserPosition(app().user));
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        System.exit(0);
    }

    // @Override
    public void notify(final NfcEvent e) {
        app().division = division;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    LoginResponse logonResponse = app().client.login(app().division, e.toString(), app().license, app().macAddress,
                            app().config.getID_SYS_MODULE());
                    app().sha = logonResponse.sha;

                    User user = app().users.getUserMap().get(logonResponse.getSelectedUser());
                    if (user == null) {
                        throw new Talk2MeException("Geen gebruiker gevonden met deze tag");
                    } else {
                        app().user = user;
                    }

                    try {
                        app().divisionData = new DivisionData(app().sha, app().client, division, true);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(getBaseContext(), LocationsActivity.class);
                                startActivity(intent);
                            }
                        });
                    } catch (Talk2MeException e) {
                        showException(e);
                    }
                } catch (final Talk2MeException e) {
                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setMessage(e.getMessage()).setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }).setTitle("Inloggen is niet gelukt");
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    });
                }
            }
        }).start();
    }
}