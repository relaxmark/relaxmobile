package nl.relax.pos;

import nl.relax.pos.client.Note;
import nl.relax.pos.client.NoteHandledState;
import nl.relax.pos.client.NoteReadState;
import nl.relax.pos.client.NoteResponse;
import nl.relax.pos.client.Talk2MeException;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class NotesDialog extends Dialog {

    private final NoteResponse notes;
    private final PosActivity activity;

    class NotesAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return notes.size();
        }

        @Override
        public Object getItem(int i) {
            return notes.getRows().get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View cv = convertView;
            if (cv == null) {
                LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                cv = vi.inflate(R.layout.note_item, null);
            }
            final Note note = notes.getRows().get(position);

            TextView tt = (TextView) cv.findViewById(R.id.txtTitle);
            TextView td = (TextView) cv.findViewById(R.id.txtDateTime);
            td.setText(note.getDateTime().getDateTimeFormat());

            if (note.getPriority() >= 3) {
                tt.setBackgroundColor(0xff800000);
                tt.setText(note.getSubject() + " (HOGE PRIORITEIT!)");
            } else {
                tt.setBackgroundColor(activity.getResources().getColor(R.color.bg_center));
                tt.setText(note.getSubject());
            }

            TextView tm = (TextView) cv.findViewById(R.id.txtMessage);
            tm.setText("Afzender: " + note.getUserNameFrom() + "\n" + note.getNote());

            ((Button) cv.findViewById(R.id.btnHandled)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        activity.app().client.updateNote(activity.app().sha, notes.getRows().get(position), NoteReadState.READ, NoteHandledState.HANDLED);
                        notes.getRows().remove(position);
                        activity.app().curNotes = notes;

                        notifyDataSetChanged();
                        if (notes.size() == 0) {
                            dismiss();
                            activity.forceBackgroundWorker();
                        }
                    } catch (Talk2MeException e) {
                        e.printStackTrace();
                        activity.showException(e);
                    }
                }
            });

            ((Button) cv.findViewById(R.id.btnReply)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new SendNoteDialog(activity, note).show();
                }
            });

            return cv;
        }
    }

    public NotesDialog(PosActivity context, NoteResponse notes) {
        super(context);
        this.activity = context;
        this.notes = notes;

        notes.sort();

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.notes_dialog);
        setCancelable(true);

        ListView lv = (ListView) findViewById(R.id.lstMessages);
        lv.setAdapter(new NotesAdapter());

        Button btn = (Button) findViewById(R.id.btnClose);
        btn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button btnNewMessage = (Button) findViewById(R.id.btnNewMessage);
        btnNewMessage.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendNoteDialog(activity).show();
            }
        });

        context.setNotesAdapter((BaseAdapter) lv.getAdapter());
    }

    @Override
    public void dismiss() {
        activity.setNotesAdapter(null);
        super.dismiss();
    }

    @Override
    public void show() {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.FILL_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        super.show();
        getWindow().setAttributes(lp);
    }
}
