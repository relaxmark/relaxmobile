package nl.relax.pos;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import android.os.Environment;

public class POSConfig extends Properties {

    private static final String KEY_TABLES_REFRESH_INTERVAL = "tables_refresh_interval";

    private static final String KEY_RETRY_INTERVAL = "retry_interval";

    private static final String KEY_RETRIES = "retries";

    private static final String KEY_TIMEOUT = "timeout";

    private static final String KEY_STR_DB_IDENTIFIER = "STR_DB_IDENTIFIER";

    private static final String KEY_ID_SYS_MODULE = "ID_SYS_MODULE";

    private static final String KEY_NAMESPACE = "namespace";

    private static final String KEY_URL = "url";
    private static final String KEY_KEY = "key";

    private static final int DEFAULT_TABLES_REFRESH_INTERVAL = 15000;

    private static final int DEFAULT_RETRY_INTERVAL = 5000;

    private static final int DEFAULT_RETRIES = 4;

    private static final int DEFAULT_TIMEOUT = 5000;

    private static final String DEFAULT_STR_DB_IDENTIFIER = "TEST";

    private static final int DEFAULT_ID_SYS_MODULE = 0;

    private static final String DEFAULT_NAMESPACE = "urn:relaxsoftware:wsaPOS";

    private static final String DEFAULT_URL = "http://pda.relaxsoftware.nl:4080/wsa/wsa1";

    /**
     * 
     */
    private static final long serialVersionUID = 2888850734684792502L;

    /**
     * The file
     */
    private final File file;

    private final String key;

    public POSConfig(String mac) {
        super();
        this.key = genKey(mac.replace("-", "").replace(":", ""));
        this.file = new File(Environment.getExternalStorageDirectory(), "posconfig.properties");
        setDefaults();
    }

    @Override
    public String getProperty(String name, String def) {
        String p = super.getProperty(name, def);
        if (p != null) {
            p = p.trim();
        }
        return p;
    }

    protected int getInt(String key, int def) {
        try {
            return Integer.parseInt(super.getProperty(key, Integer.toString(def)));
        } catch (Exception e) {
            e.printStackTrace();
            return def;
        }
    }

    protected void setInt(String name, int value) {
        setProperty(name, Integer.toString(value));
    }

    private String genKey(String mac) {
        StringBuilder sb = new StringBuilder();
        for (int i = mac.length() - 1; i >= 0; i--) {
            char c = mac.charAt(i);
            int ii = Integer.parseInt(Character.toString(c), 16) & 0xf;
            sb.append(Integer.toHexString(ii));
        }

        Long l = Long.parseLong(sb.toString(), 16);

        return Long.toHexString(l) + mac.hashCode();
    }

    public boolean exists() {
        return file.exists();
    }

    public boolean isValid() {
        return key.equals(getProperty(KEY_KEY));
    }

    public void load() throws IOException {
        InputStream in = null;

        try {
            in = new BufferedInputStream(new FileInputStream(file));
            super.loadFromXML(in);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void save() throws IOException {
        OutputStream os = null;
        setProperty(KEY_KEY, key);
        try {
            os = new BufferedOutputStream(new FileOutputStream(file));
            super.storeToXML(os, "POS Config");
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getURL() {
        return getProperty(KEY_URL, DEFAULT_URL).trim();
    }

    public void setURL(String url) {
        setProperty(KEY_URL, url);
    }

    public String getNamespace() {
        return getProperty(KEY_NAMESPACE, DEFAULT_NAMESPACE).trim();
    }

    public void setNamespace(String url) {
        setProperty(KEY_NAMESPACE, url);
    }

    public int getID_SYS_MODULE() {
        return getInt(KEY_ID_SYS_MODULE, DEFAULT_ID_SYS_MODULE);
    }

    public void setID_SYS_MODULE(int i) {
        setInt(KEY_ID_SYS_MODULE, i);
    }

    public String getSTR_DB_IDENTIFIER() {
        return getProperty(KEY_STR_DB_IDENTIFIER, DEFAULT_STR_DB_IDENTIFIER);
    }

    public void setSTR_DB_IDENTIFIER(String s) {
        setProperty(KEY_STR_DB_IDENTIFIER, s);
    }

    public int getTimeout() {
        return getInt(KEY_TIMEOUT, DEFAULT_TIMEOUT);
    }

    public void setTimeout(int i) {
        setInt(KEY_TIMEOUT, i);
    }

    public int getRetries() {
        return getInt(KEY_RETRIES, DEFAULT_RETRIES);
    }

    public void setRetries(int i) {
        setInt(KEY_RETRIES, i);
    }

    public int getRetryInterval() {
        return getInt(KEY_RETRY_INTERVAL, DEFAULT_RETRY_INTERVAL);
    }

    public void setRetryInterval(int i) {
        setInt(KEY_RETRY_INTERVAL, i);
    }

    public int getTablesRefreshInterval() {
        return getInt(KEY_TABLES_REFRESH_INTERVAL, DEFAULT_TABLES_REFRESH_INTERVAL);
    }

    public void setTablesRefreshInterval(int i) {
        setInt(KEY_TABLES_REFRESH_INTERVAL, i);
    }

    void setDefaults() {
        setID_SYS_MODULE(DEFAULT_ID_SYS_MODULE);
        setNamespace(DEFAULT_NAMESPACE);
        setRetries(DEFAULT_RETRIES);
        setRetryInterval(DEFAULT_RETRY_INTERVAL);
        setSTR_DB_IDENTIFIER(DEFAULT_STR_DB_IDENTIFIER);
        setTablesRefreshInterval(DEFAULT_TABLES_REFRESH_INTERVAL);
        setTimeout(DEFAULT_TIMEOUT);
        setURL(DEFAULT_URL);
        setProperty(KEY_KEY, key);
    }
}
