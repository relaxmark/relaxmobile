package nl.relax.pos;

import nl.relax.pos.client.ArticleOptions;
import nl.relax.pos.client.Talk2MeException;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.CheckBox;
import android.widget.LinearLayout;

public class ArticleOptionsActivity extends PosActivity {

    //    Handler handler = new ErrorHandler(this, true);
    private ArticleOptions options;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app().curActivity = this;
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());

        setContentView(R.layout.articleoptionsactivity);
        setTitle(((PosApplication) getApplication()).license.getName() + " | " + ((PosApplication) getApplication()).user.getName());

        try {
            this.options = ((PosApplication) getApplication()).client.getArticleOptions(((PosApplication) getApplication()).sha,
                    ((PosApplication) getApplication()).curArticle);
        } catch (Talk2MeException e) {
            showExceptionAndFinish(e);
        }

        LinearLayout ll = (LinearLayout) findViewById(R.id.aoLayout);
        for (ArticleOptions.Menu menu : options.getMenus()) {
            for (ArticleOptions.Option option : menu.getOptions()) {
                CheckBox cb = new CheckBox(this);
                cb.setText(option.getName());
                ll.addView(cb);
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        app().curActivity = this;
    }
}
