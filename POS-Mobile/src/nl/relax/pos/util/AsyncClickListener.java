package nl.relax.pos.util;

import nl.relax.pos.PosActivity;
import android.view.View;
import android.view.View.OnClickListener;

public class AsyncClickListener implements OnClickListener {

    public static interface Task {
        void run() throws Exception;
    }

    private final PosActivity activity;
    private final boolean finishOnError;
    private final Task task;

    public AsyncClickListener(PosActivity activity, Task task) {
        this(activity, false, task);
    }

    public AsyncClickListener(PosActivity activity, boolean finishOnError, Task task) {
        this.activity = activity;
        this.finishOnError = finishOnError;
        this.task = task;
    }

    @Override
    public void onClick(final View v) {
        v.setEnabled(false);

        new PosAsyncTask<Void, Void, Void>(activity, finishOnError) {
            @Override
            protected Void task(Void... params) throws Exception {
                task.run();
                return null;
            }

            @Override
            protected void done(Void p) {
                v.setEnabled(true);
            }

            @Override
            protected void onPostError() {
                v.setEnabled(true);
            }
        };
    }
}
