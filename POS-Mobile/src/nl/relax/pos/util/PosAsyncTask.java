package nl.relax.pos.util;

import nl.relax.pos.PosActivity;
import android.os.AsyncTask;

public abstract class PosAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private final PosActivity activity;
    private Exception e;
    private final boolean finishOnError;

    public PosAsyncTask(PosActivity activity) {
        this(activity, false);
    }

    public PosAsyncTask(PosActivity activity, boolean finishOnError) {
        super();
        this.activity = activity;
        this.finishOnError = finishOnError;
    }

    @Override
    protected final Result doInBackground(Params... params) {
        try {
            return task(params);
        } catch (Exception e) {
            this.e = e;
            e.printStackTrace();
            return null;
        }
    }

    protected abstract Result task(Params... params) throws Exception;

    @Override
    protected final void onPostExecute(Result v) {
        if (e != null) {
            if (finishOnError) {
                activity.showExceptionAndFinish(e);
            } else {
                activity.showException(e);
                onPostError();
            }
            return;
        }
        done(v);
    }

    protected abstract void done(Result v);

    protected void onPostError() {
    }
}
