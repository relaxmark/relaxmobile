package nl.relax.pos;

import java.util.List;

import nl.relax.pos.client.ArticleGroup;
import nl.relax.pos.client.POSGuest;
import nl.relax.pos.client.Table;
import nl.relax.pos.nfc.NfcActivity;
import nl.relax.pos.nfc.NfcEvent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This activity shows the
 * 
 * @author erik
 */
public class ArticleGroupsActivity extends NfcActivity {
    final Handler fatal = new ErrorHandler(this, true);

    private List<ArticleGroup> groups;

    // final Handler handler = new ErrorHandler(this, true);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orderactivity_grid);
        super.initBackgroundWorker(true, true, true);
        setTitle(((PosApplication) getApplication()).license.getName() + " | " + ((PosApplication) getApplication()).user.getName());
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
    }

    @Override
    public void onResume() {
        super.onResume();

        app().curActivity = this;

        if (((PosApplication) getApplication()).backToTable) {
            ((PosApplication) getApplication()).backToTable = false;
            finish();
        }

        TextView tv1 = (TextView) findViewById(R.id.curLoc);
        tv1.setText(((PosApplication) getApplication()).curLocation.getName() + ", tafel " + ((PosApplication) getApplication()).curTable.getName());

        this.groups = ((PosApplication) getApplication()).divisionData.getArticleGroups(((PosApplication) getApplication()).curLocation);

        AbsListView lv1 = (AbsListView) findViewById(R.id.gridview);
        ArrayAdapter<ArticleGroup> adapter = new ArrayAdapter<ArticleGroup>(this, R.layout.subgroup_item, groups) {
            LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View cv = convertView;
                if (cv == null) {
                    cv = vi.inflate(R.layout.subgroup_item, null);
                }

                View view = super.getView(position, cv, parent);

                ArticleGroup sg = getItem(position);
                int start = (sg.getRed() << 16) | (sg.getGreen() << 8) | sg.getBlue() | 0xff000000;
                GradientDrawable g = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[] { start, 0xff181818 });
                g.setGradientCenter(0.5f, 0.5f);
                g.setCornerRadius(5);
                view.setBackgroundDrawable(g);
                return view;
            }
        };

        lv1.setClickable(true);
        lv1.setAdapter(adapter);

        lv1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                ((PosApplication) getApplication()).curSubGroup = groups.get(position);
                Intent intent = new Intent(getBaseContext(), ArticlesActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void notify(NfcEvent e) {
        if (!((PosApplication) getApplication()).curLocation.isGuestRequired()) {
            // only react to NFC if guestRequired
            return;
        }

        Table table = ((PosApplication) getApplication()).curTable;

        final List<POSGuest> guests = table.getGuests();
        for (int i = 0; i < guests.size(); i++) {
            final POSGuest g = guests.get(i);
            if (g != null && g.getTagIdentification().equals(e.toString())) {
                fatal.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), g.getName() + " geselecteerd.", Toast.LENGTH_SHORT).show();
                    }
                });
                table.setSelectedGuestIndex(i);
                ((PosApplication) getApplication()).curGuest = g;

                return;
            }
        }

        try {
            final POSGuest newGuest = ((PosApplication) getApplication()).client.getGuestByTag(((PosApplication) getApplication()).sha, e.toString(),
                    ((PosApplication) getApplication()).curTable);
            fatal.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), newGuest.getName() + " geselecteerd.", Toast.LENGTH_SHORT).show();
                }
            });
            table.addGuest(app().client, app().sha, newGuest);
            table.setSelectedGuestIndex(table.getGuests().size() - 1);
            ((PosApplication) getApplication()).curGuest = newGuest;

        } catch (Exception e1) {
            showException(e1);
        }
    }
}
