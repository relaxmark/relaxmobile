package nl.relax.pos;

import nl.relax.pos.client.DivisionData;
import nl.relax.pos.client.RetryListener;
import nl.relax.pos.client.Talk2MeClient;
import nl.relax.pos.client.Talk2MeException;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("HandlerLeak")
public class EntreeActivity extends PosActivity {

    /**
     * The minimum time this activity is active
     */
    private static final int MINIMUM_DISPLAY_TIME = 2000;

    /**
     * The worker Thread for background initialization
     */
    private Thread initializer;

    /**
     * The handler for going to the next activity when background initialization
     * is finished
     */
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            final String sha = app().device.getSha();
            System.out.println("SHA = " + sha);

            if (!Talk2MeClient.NIL.equals(sha)) {

                // we can skip the logon activity in this case and take the
                // strSHA and user id from the Device info we got from the
                // server.
                app().user = app().users.getUserMap().get(app().device.getIdUser());
                app().division = app().divisions.getByID(app().user.getIdDefaultDivision());

                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            app().courses = app().client.getCourses(sha);
                            app().settings = app().client.getSettings(sha);
                            app().divisionData = new DivisionData(sha, app().client, app().division, true);
                        } catch (Talk2MeException e) {
                            e.printStackTrace();
                            // we've seen that getDevice can return an invalid SHA
                            app().device.resetSha();
                        }
                    }
                });

                t.start();

                try {
                    t.join();
                } catch (InterruptedException e1) {
                    System.out.println("############################################## interrupted");
                    e1.printStackTrace();
                }

                if (app().device.getSha() != null) {
                    app().sha = app().device.getSha();
                    startActivity(new Intent(getBaseContext(), LocationsActivity.class));
                } else {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
            } else {
                startActivity(new Intent(getBaseContext(), LoginActivity.class));
            }
            finish();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("#################################################### EntreeActivity.onCreate");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.entreeactivity);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());

        // get the Device
        final WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (!wifi.isWifiEnabled()) {
            wifi.setWifiEnabled(true);
        }

        final WifiInfo info = wifi.getConnectionInfo();
        final String mac = info.getMacAddress().replace(':', '-');

        initializer = new Thread(new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();

                // initialize the client
                RetryListener retryListener = new RetryListener() {
                    @Override
                    public void retrying(final int retry, final Exception e) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                String ssid = info == null ? null : info.getSSID();
                                System.out.println("SSID=" + ssid);
                                if (ssid == null || ssid.length() == 0) {
                                    Toast.makeText(getApplicationContext(), "verbinden... (" + retry + ")", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "verbinden met " + ssid + "... (" + retry + ")", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                };
                try {

                    app().config = new POSConfig(mac);
                    if (!app().config.exists()) {
                        throw new Exception("Geen configuratie gevonden!");
                    }
                    app().config.load();
                    if (!app().config.isValid()) {
                        throw new Exception("Configuratie is niet geldig voor dit apparaat!");
                    }

                    app().client = new Talk2MeClient(app().config.getURL(), app().config.getNamespace(), true, app().config.getTimeout(),
                            app().config.getRetries(), app().config.getRetryInterval(), retryListener);

                    // get the License
                    app().license = app().client.getLicense(app().config.getSTR_DB_IDENTIFIER());

                    System.out.println("############################# app().license=" + app().license);

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            TextView tv = (TextView) findViewById(R.id.txtEntreeLicense);
                            tv.setText(app().license.getName());
                        }
                    });

                    // mac = "98-0c-82-84-2a-6e";

                    app().macAddress = mac;
                    app().device = app().client.getDevice(app().license.getIdSysAdm(), mac);

                    // get the Divisions
                    app().divisions = app().client.getDivisions();

                    // get the Users
                    app().users = app().client.getSYS_User(app().license.getIdSysAdm(), app().device.getIdSysGroup());
                    app().user = app().users.getUserMap().get(app().device.getIdUser());

                    // wait some more to prevent the splash screen to disappear too
                    // quickly
                    long elapsed = System.currentTimeMillis() - start;
                    if (elapsed < MINIMUM_DISPLAY_TIME) {
                        try {
                            Thread.sleep(MINIMUM_DISPLAY_TIME - elapsed);
                        } catch (InterruptedException e) {
                        }
                    }

                    if (!Thread.interrupted()) {
                        // go to the next activity (either LoginActivity or directly
                        // to LocationActivity)
                        handler.sendMessage(new Message());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    showExceptionAndFinish(e);
                }
            }
        });

        initializer.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (initializer != null && initializer.isAlive()) {
            initializer.interrupt();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
    }

    @Override
    public void onRestart() {
        super.onRestart();
        System.out.println("#################################################### EntreeActivity.onRestart");
    }
}
