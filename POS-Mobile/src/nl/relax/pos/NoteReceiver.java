package nl.relax.pos;

import nl.relax.pos.client.NoteResponse;

public interface NoteReceiver {
    void notifyNoteReceived(NoteResponse noteResponse, int newNotes);
}
