package nl.relax.pos;

import java.io.IOException;

import nl.relax.pos.client.Talk2MeException;
import nl.relax.pos.client.Talk2MeSessionExpiredException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

public class ErrorHandler extends Handler {

    private final Activity context;
    private final boolean finish;

    public ErrorHandler(Activity context, boolean finish) {
        this.context = context;
        this.finish = finish;
    }

    public ErrorHandler(Activity context) {
        this(context, false);
    }

    @Override
    public void handleMessage(Message msg) {
        final Exception e = (Exception) msg.obj;

        if (!context.isFinishing()) {
            if (e != null && e instanceof Talk2MeException && e.getCause() != null && e.getCause() instanceof IOException) {
                // IOException as the cause will result in a "Details..." button
                showDialogWithDetails(e);
            } else {
                showDialog(e);
            }
        }
    }

    private void showDialog(final Exception e) {
        final boolean expired = (e instanceof Talk2MeSessionExpiredException)
                || ((e.getCause() != null) && e.getCause() instanceof Talk2MeSessionExpiredException);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(e.getMessage()).setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                if (finish || expired) {
                    if (expired) {
                        Intent i = context.getBaseContext().getPackageManager().getLaunchIntentForPackage(context.getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);
                        // System.exit(0);
                    }

                    context.finish();
                }
            }
        });
        if (expired) {
            builder.setOnCancelListener(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    Intent i = context.getBaseContext().getPackageManager().getLaunchIntentForPackage(context.getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                    // System.exit(0);
                }
            });
        }
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showDialogWithDetails(final Exception e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("De verbinding kon niet tot stand worden gebracht.").setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        if (finish) {
                            context.finish();
                        }
                    }
                }).setNeutralButton("Details...", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        AlertDialog.Builder _builder = new AlertDialog.Builder(context);
                        _builder.setMessage(e.getMessage()).setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (finish) {
                                    context.finish();
                                }
                            }
                        }).setTitle("Technische details");
                        AlertDialog _alert = _builder.create();
                        _alert.show();
                    }
                }).setTitle("Er is een netwerk fout opgetreden");
        AlertDialog alert = builder.create();
        alert.show();
    }
}
