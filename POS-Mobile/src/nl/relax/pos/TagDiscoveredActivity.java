package nl.relax.pos;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

/**
 * 2790480874, 2790368490
 * 
 * @author erik
 * 
 */
public class TagDiscoveredActivity extends Activity {
    static final int ACTIVITY_TIMEOUT_MS = 1 * 1000;

    long id;

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            finish();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tagactivity);

        Intent intent = getIntent();
        handleIntent(intent);

        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(ACTIVITY_TIMEOUT_MS);
                } catch (InterruptedException e) {
                }
                handler.sendMessage(new Message());
            }
        }).start();
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {
            System.out.println("****************** " + intent);
            Bundle b = intent.getExtras();
            if (b != null) {
                byte[] bid = b.getByteArray(NfcAdapter.EXTRA_ID);
                long id = 0;
                for (int i = bid.length - 1; i >= 0; i--) {
                    id <<= 8;
                    id |= bid[i] & 0xff;
                }
                System.out.println("**** ID=" + id);
                TextView tv = (TextView) findViewById(R.id.textTagID);
                tv.setText(Long.toString(id));
                this.id = id;
            }
        }
    }
}
