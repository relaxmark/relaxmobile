package nl.relax.pos;

import nl.relax.pos.client.Article;
import nl.relax.pos.client.TableOrderRow;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This Activity is displayed when ordering an article, but only if the article
 * is long-clicked.
 * 
 * @author erik
 */
public class AddOrderActivity extends PosActivity {

    private int amount = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        setContentView(R.layout.addorderactivity);
        setTitle(app().license.getName() + " | " + app().user.getName());

        final Button btnOk = (Button) findViewById(R.id.btnOk);
        final Button btnCancel = (Button) findViewById(R.id.btnCancel);
        final Button btnDec = (Button) findViewById(R.id.btnMinus);
        final Button btnInc = (Button) findViewById(R.id.btnPlus);

        final EditText edit = (EditText) findViewById(R.id.edtAmount);

        final TextView txtGuest = (TextView) findViewById(R.id.txtGuest);
        final TextView txtArticle = (TextView) findViewById(R.id.txtArticle);

        if (app().curGuest == null) {
            txtGuest.setText("");
        } else {
            txtGuest.setText(app().curGuest.getName());
        }

        txtArticle.setText(app().curArticle.toString());

        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                add(app().curArticle, amount);
                finish();
            }
        });

        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnDec.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amount > 1) {
                    amount--;
                }
                edit.setText(Integer.toString(amount));
            }
        });

        btnInc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                amount++;
                edit.setText(Integer.toString(amount));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        app().curActivity = this;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    protected void add(Article article, int count) {
        TableOrderRow row = new TableOrderRow(app().curLocation.id, app().curTable.id, article.id, article.getName(), count, article.getSalesPriceCents(),
                app().courses.get(article.getIdCourse()));
        if (app().curGuest != null) {
            row.setIdGuest(app().curGuest.id);
        }
        try {
            app().curTable.addOrder(app().client, app().sha, app().curGuest, row, true);
            Toast.makeText(getApplicationContext(), article + " toegevoegd.", Toast.LENGTH_SHORT).show();
        } catch (Exception e1) {
            showException(e1);
        }
    }
}
