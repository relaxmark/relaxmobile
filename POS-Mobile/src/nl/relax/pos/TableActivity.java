package nl.relax.pos;

import java.util.ArrayList;
import java.util.List;

import nl.relax.pos.client.Course;
import nl.relax.pos.client.OrderAction;
import nl.relax.pos.client.POSGuest;
import nl.relax.pos.client.PackageItem;
import nl.relax.pos.client.PackageItems;
import nl.relax.pos.client.ReservationDetailResponse;
import nl.relax.pos.client.ReservationDetailRow;
import nl.relax.pos.client.Setting;
import nl.relax.pos.client.TableOrderRow;
import nl.relax.pos.client.TableStatusAction;
import nl.relax.pos.client.Talk2MeException;
import nl.relax.pos.nfc.NfcActivity;
import nl.relax.pos.nfc.NfcEvent;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Table screen
 * 
 * @author erik
 */
public class TableActivity extends NfcActivity {

    final Handler fatal = new ErrorHandler(this, true);
    final Handler nonfatal = new ErrorHandler(this, false);

    /**
     * Workaround to prevent multiple clicks in quick succession
     */
    private static long lastOrdered = System.currentTimeMillis();
    private static long lastFreed = System.currentTimeMillis();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app().curActivity = this;
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        setContentView(R.layout.table);
        super.initBackgroundWorker(true, true, true);
        setTitle(app().license.getName() + " | " + app().user.getName());

        TextView tv1 = (TextView) findViewById(R.id.curLoc);
        tv1.setText(app().curLocation.getName());

        View btnAddArticle = findViewById(R.id.btnAddOrder);
        btnAddArticle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ArticleGroupsActivity.class);
                startActivity(intent);
            }
        });

        final Button btnPlaceOrder = (Button) findViewById(R.id.btnPlaceOrder);
        btnPlaceOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastOrdered != 0 && System.currentTimeMillis() - lastOrdered < 2000) {
                    return;
                } else {
                    lastOrdered = System.currentTimeMillis();
                }

                btnPlaceOrder.setEnabled(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (TableActivity.this) {
                            try {
                                app().client.setTableOrders(app().sha, app().user, app().device, app().curLocation, app().curTable, OrderAction.ORDER);
                                leaveTable();
                                finish();
                            } catch (Talk2MeException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        btnPlaceOrder.setEnabled(true);
                                    }
                                });
                                showException(e);
                            }
                        }
                    }
                }).start();
            }
        });

        View btnManualGuest = findViewById(R.id.btnManualGuest);
        btnManualGuest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                manualGuestEntry();
            }
        });

        LinearLayout guestLayout = (LinearLayout) findViewById(R.id.guestsLayout);
        final Button btnFreeTable = (Button) findViewById(R.id.btnFreeTable);
        final boolean guestRequired = app().curLocation.isGuestRequired();
        if (guestRequired) {
            btnFreeTable.setText("Vrijgeven");
            guestLayout.setVisibility(LinearLayout.VISIBLE);
        } else {
            btnFreeTable.setText("Afrekenen");
            guestLayout.setVisibility(LinearLayout.GONE);
        }

        Setting setting = app().settings.get(Setting.Key.INTEGER_5_TAG_IDENTIFICATION);
        final boolean mifare = setting.getInteger() == 3 || setting.getInteger() == 5;
        final boolean manualGuestInput = setting.getInteger() == 4;

        if (guestRequired && manualGuestInput) {
            btnManualGuest.setVisibility(View.VISIBLE);
        } else {
            btnManualGuest.setVisibility(View.GONE);
        }

        btnFreeTable.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (app().curLocation.isGuestRequired()) {
                    freeTableAndFinish(btnFreeTable);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(TableActivity.this);

                    String message = "Totaalbedrag: " + app().curTable.getTableTotalFormatted() + " " + app().license.getCurrencyCode();

                    builder.setMessage(message).setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            freeTableAndFinish(btnFreeTable);
                        }
                    }).setNegativeButton("Annuleren", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).setTitle("Afrekenen");
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });

        app().curGuest = null;
        app().curTable.setSelectedGuestIndex(-1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // MenuInflater inflater = getMenuInflater();
        // inflater.inflate(R.menu.table_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.view_reservations:
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void checkPackageItems() {
        final PackageItems packageItems = app().curTable.getPackageItemsFromSelectedGuest();
        if (packageItems != null && packageItems.size() > 0) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(app().curGuest.getName() + " heeft recht op de volgende artikelen uit het arrangement:");

            String[] items = new String[packageItems.size()];

            for (int i = 0; i < items.length; i++) {
                items[i] = packageItems.getRows().get(i).getArticleName();
            }
            final boolean[] checked = new boolean[items.length];
            builder.setMultiChoiceItems(items, checked, new OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    checked[which] = isChecked;
                }
            });
            builder.setPositiveButton("Toevoegen", new Dialog.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        for (int i = 0; i < checked.length; i++) {
                            if (checked[i]) {
                                PackageItem pi = packageItems.getRows().get(i);

                                TableOrderRow tor = new TableOrderRow(app().curLocation.id, app().curTable.id, pi);
                                app().curTable.addOrder(app().client, app().sha, app().curGuest, tor);
                                refreshViews();
                            }
                        }
                    } catch (Exception e1) {
                        showException(e1);
                    }

                }
            });

            builder.setCancelable(true);

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void manualGuestEntry() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.manual_guest_entry);
        dialog.setTitle("Voer de sleutel van de gast in");

        final EditText aName = (EditText) dialog.findViewById(R.id.manualguestid);

        Button button = (Button) dialog.findViewById(R.id.btnManualGuestOk);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final List<POSGuest> guests = app().curTable.getGuests();
                for (int i = 0; i < guests.size(); i++) {
                    POSGuest g = guests.get(i);
                    if (g != null && g.getTagCode().equals(aName.getText().toString())) {
                        Toast.makeText(getApplicationContext(), g.getName() + " geselecteerd.", Toast.LENGTH_SHORT).show();
                        app().curTable.setSelectedGuestIndex(i);
                        app().curGuest = g;
                        fatal.post(new Runnable() {
                            @Override
                            public void run() {
                                refreshViews();
                            }
                        });

                        return;
                    }
                }

                try {
                    POSGuest newGuest = app().client.getGuestByCode(app().sha, aName.getText().toString(), app().curTable);
                    Toast.makeText(getApplicationContext(), newGuest.getName() + " geselecteerd.", Toast.LENGTH_SHORT).show();
                    app().curTable.addGuest(app().client, app().sha, newGuest);
                    app().curTable.setSelectedGuestIndex(app().curTable.getGuests().size() - 1);
                    app().curGuest = newGuest;
                    dialog.dismiss();
                    fatal.post(new Runnable() {
                        @Override
                        public void run() {
                            refreshViews();
                        }
                    });

                } catch (Talk2MeException e1) {
                    showException(e1);
                }
            }
        });
        dialog.setCancelable(true);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();

    }

    private void freeTable() throws Talk2MeException {
        app().client.setTableOrders(app().sha, app().user, app().device, app().curLocation, app().curTable, OrderAction.FREE);
        leaveTable();
    }

    private void leaveTable() throws Talk2MeException {
        app().client.updateTableStatus(app().sha, app().device, app().curLocation, app().curTable, TableStatusAction.FREE, app().division);
    }

    @Override
    public void onResume() {
        super.onResume();
        app().curActivity = this;
        app().backToTable = false;
        refreshViews();
    }

    @Override
    public void onBackPressed() {

        if (!app().curTable.isUpToDate()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Er zijn artikelen toegevoegd die nog niet besteld zijn.\nWeet u zeker dat u de tafel wilt verlaten zonder te bestellen?")
                    .setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            TableActivity.super.onBackPressed();
                        }
                    }).setNegativeButton("Annuleren", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).setTitle("Verlaten zonder bestellen?");
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        leaveTable();
                    } catch (Talk2MeException e) {
                        showExceptionAndFinish(e);
                    }
                }
            }).start();
            TableActivity.super.onBackPressed();
        }
    }

    /**
     * 
     */
    private void refreshViews() {

        final List<POSGuest> guests = app().curTable.getGuests();

        View btnAdd = findViewById(R.id.btnAddOrder);
        if (app().curLocation.isGuestRequired()) {
            // You can't add orders if there are no guests
            btnAdd.setEnabled(guests.size() > 0 && guests.get(0) != null);
        } else {
            btnAdd.setEnabled(true);
        }

        Button btnOrder = (Button) findViewById(R.id.btnPlaceOrder);
        // no need to place order if there are no new orders
        btnOrder.setEnabled(!app().curTable.isUpToDate());

        final ArrayAdapter<POSGuest> adapter = new ArrayAdapter<POSGuest>(this, R.layout.guest_item, guests) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Resources res = getResources();

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View tv = convertView;

                if (tv == null) {
                    tv = inflater.inflate(R.layout.guest_item, null);
                }

                TextView view = (TextView) tv;
                String name = guests.get(position) != null ? guests.get(position).getName() : "(onbekend)";
                view.setText(name);

                if (position == app().curTable.getSelectedGuestIndex()) {
                    view.setTextColor(res.getInteger(R.color.tv_main_categories_selected));
                    view.setBackgroundColor(0xffffffff);
                } else {
                    view.setTextColor(res.getInteger(R.color.tv_main_categories_normal));
                    view.setBackgroundColor(res.getInteger(R.color.bg_left));
                }
                return tv;
            }
        };

        if (guests.size() > 0) {
            if (app().curGuest == null) {
                refreshOrders(app().curGuest = guests.get(0));
                app().curTable.setSelectedGuestIndex(0);
            } else {
                refreshOrders(app().curGuest);
            }
        } else {
            refreshOrders(null);
        }

        final ListView listGuests = (ListView) findViewById(R.id.guests);
        listGuests.setClickable(true);
        listGuests.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                POSGuest g = guests.get(position);
                app().curGuest = g;
                app().curTable.setSelectedGuestIndex(position);
                listGuests.setAdapter(adapter);
                refreshOrders(g);
                checkPackageItems();
            }
        });
        listGuests.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                final POSGuest g = guests.get(position);
                app().curGuest = g;
                app().curTable.setSelectedGuestIndex(position);
                listGuests.setAdapter(adapter);
                refreshOrders(g);

                final List<ReservationDetailRow> rows = new ArrayList<ReservationDetailRow>();
                new AsyncTask<Void, Void, Void>() {

                    Exception e;

                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            ReservationDetailResponse response = app().client.getReservationDetails(app().sha, g);
                            rows.addAll(response.getRows());

                        } catch (Exception e) {
                            this.e = e;
                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void v) {
                        if (e != null) {
                            showException(e);
                            return;
                        }

                        AlertDialog.Builder builder = new AlertDialog.Builder(TableActivity.this);
                        builder.setCancelable(true);

                        if (rows.size() > 0) {
                            builder.setTitle("Planning " + g.getName());
                            ArrayAdapter<ReservationDetailRow> aa = new ArrayAdapter<ReservationDetailRow>(TableActivity.this, R.layout.list_item, rows) {
                                @Override
                                public View getView(int position, View convertView, ViewGroup parent) {
                                    View cv = convertView;
                                    if (cv == null) {
                                        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        cv = vi.inflate(R.layout.list_item, null);
                                    }

                                    TextView tv = (TextView) cv;
                                    String text = rows.get(position).toString();
                                    if (text.contains("\n")) {
                                        text = "<b>" + text.replace("\n ", "</b><br/>&nbsp;");
                                    } else {
                                        text = "<font color='#404040'>&#8226; <i>" + text + "</i></font>";
                                    }
                                    tv.setText(Html.fromHtml(text));

                                    return cv;
                                }
                            };

                            builder.setSingleChoiceItems(aa, -1, null);
                            AlertDialog dialog = builder.create();
                            dialog.getListView().setScrollbarFadingEnabled(false);
                            dialog.getListView().setScrollBarStyle(ListView.SCROLLBARS_OUTSIDE_INSET);
                            dialog.show();
                        } else {
                            builder.setMessage("Geen planning gevonden");
                            builder.show();
                        }

                    }

                }.execute();

                return true;
            }
        });

        listGuests.setAdapter(adapter);
        listGuests.setScrollbarFadingEnabled(false);
        adapter.notifyDataSetChanged();

        TextView tv0 = (TextView) findViewById(R.id.table);
        tv0.setText("Tafel " + app().curTable.getName());

        TextView txtTotal = (TextView) findViewById(R.id.tableTotal);
        txtTotal.setText(app().curTable.getTableTotalFormatted());
    }

    /**
     * 
     * @param g
     * @throws Talk2MeException
     */
    private void refreshOrders(POSGuest g) {
        final List<TableOrderRow> orders = app().curTable.getOrders(g);
        final TableOrderAdapter adapter = new TableOrderAdapter(this, orders);
        final ListView view = (ListView) findViewById(R.id.orders);

        view.setClickable(true);
        view.setAdapter(adapter);
        view.setScrollbarFadingEnabled(false);
        view.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                final TableOrderRow tor = orders.get(position);

                showArticleDialog(orders, tor, position);
            }
        });

        fatal.post(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                view.smoothScrollToPosition(adapter.getCount());
            }
        });
    }

    static final String OPTION_DELETE = "Verwijderen";
    static final String OPTION_INPUT_FREETEXT = "Vrije Tekst";
    static final String OPTION_SET_COURSE = "Gang wijzigen";

    protected void showArticleDialog(final List<TableOrderRow> orders, final TableOrderRow tor, final int position) {
        if (tor.getDeleted() != 0) {
            return;
        }

        final CharSequence[] items;
        if (tor.getIdArticleParent() == 0 && tor.getDeleted() == 0) {
            if (tor.id == 0) {
                items = new CharSequence[] { OPTION_INPUT_FREETEXT, OPTION_DELETE, OPTION_SET_COURSE };
            } else {
                items = new CharSequence[] { OPTION_INPUT_FREETEXT, OPTION_DELETE };
            }
        } else {
            if (tor.getIdArticleParent() != 0 && tor.getDeleted() == 0) {
                items = new CharSequence[] { OPTION_DELETE };
            } else if (tor.getIdArticleParent() == 0 && tor.getDeleted() != 0) {
                items = new CharSequence[] { OPTION_INPUT_FREETEXT };
            } else {
                return;
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Opties\n" + tor.getArticleName());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(OPTION_INPUT_FREETEXT)) {
                    showFreeTextDialog(orders, tor, position);
                    dialog.cancel();
                } else if (items[item].equals(OPTION_DELETE)) {
                    deleteArticle(tor);
                    dialog.cancel();
                } else if (items[item].equals(OPTION_SET_COURSE)) {
                    showSetCourseDialog(orders, tor, position);
                    dialog.cancel();
                }

            }
        });
        builder.setCancelable(true);
        builder.setNegativeButton("Annuleren", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    protected void showSetCourseDialog(List<TableOrderRow> orders, final TableOrderRow tor, int position) {
        final List<Course> courses = app().courses.getAll();
        AlertDialog.Builder builder = new AlertDialog.Builder(TableActivity.this);
        builder.setTitle("Kies een gang");
        ArrayAdapter<Course> aa = new ArrayAdapter<Course>(TableActivity.this, android.R.layout.select_dialog_singlechoice, courses);
        builder.setSingleChoiceItems(aa, tor.getIdCourse() > 0 ? tor.getIdCourse() - 1 : 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                Course c = courses.get(item);
                tor.setCourse(c);
                fatal.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshViews();
                    }
                });
                dialog.dismiss();
            }
        });
        builder.setCancelable(true);
        builder.show();
    }

    protected void showFreeTextDialog(final List<TableOrderRow> orders, final TableOrderRow tor, final int position) {
        if (tor.getDeleted() != 0) {
            return;
        }

        final Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.freetext);
        dialog.setTitle("Vrije tekst");

        final EditText text = (EditText) dialog.findViewById(R.id.freetext);
        Button button = (Button) dialog.findViewById(R.id.btnFreeText);
        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO:
                TableOrderRow trow = new TableOrderRow(app().curLocation.id, app().curTable.id, 0, text.getText().toString(), 1, 0, 0, tor, 1);
                try {
                    app().curTable.addOrder(app().client, app().sha, app().curGuest, trow);
                } catch (Exception e1) {
                    showException(e1);
                }
                fatal.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshViews();
                    }
                });
                dialog.cancel();
            }
        });
        dialog.setCancelable(true);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }

    @Override
    public void notify(NfcEvent e) {
        if (!app().curLocation.isGuestRequired()) {
            // only react to NFC if guestRequired
            return;
        }

        // 1) link with a guest
        // IF no guest was found: Add guest to this table
        // OTHERWISE: select current guest
        final List<POSGuest> guests = app().curTable.getGuests();
        for (int i = 0; i < guests.size(); i++) {
            final POSGuest g = guests.get(i);
            if (g != null && g.getTagIdentification().equals(e.toString())) {
                fatal.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), g.getName() + " geselecteerd.", Toast.LENGTH_SHORT).show();
                    }
                });
                app().curTable.setSelectedGuestIndex(i);
                app().curGuest = g;
                fatal.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshViews();
                        checkPackageItems();
                    }
                });

                return;
            }
        }

        try {
            final POSGuest newGuest = app().client.getGuestByTag(app().sha, e.toString(), app().curTable);
            fatal.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), newGuest.getName() + " geselecteerd.", Toast.LENGTH_SHORT).show();
                }
            });

            app().curTable.addGuest(app().client, app().sha, newGuest);
            app().curTable.setSelectedGuestIndex(app().curTable.getGuests().size() - 1);
            app().curGuest = newGuest;

            fatal.post(new Runnable() {
                @Override
                public void run() {
                    refreshViews();
                    checkPackageItems();
                }
            });

        } catch (Exception e1) {
            showException(e1);
        }
    }

    private void freeTableAndFinish(Button btnFreeTable) {
        if (lastFreed != 0 && System.currentTimeMillis() - lastFreed < 2000) {
            return;
        } else {
            lastFreed = System.currentTimeMillis();
        }

        btnFreeTable.setEnabled(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    freeTable();
                    fatal.post(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    });
                } catch (Talk2MeException e) {
                    showExceptionAndFinish(e);
                }
            }
        }).start();
    }

    private void deleteArticle(final TableOrderRow tor) {
        AlertDialog.Builder builder = new AlertDialog.Builder(TableActivity.this);
        builder.setMessage(tor.toString()).setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if (tor.getDeleted() != 0) {
                    return;
                }

                try {
                    app().curTable.cancelOrder(app().client, app().sha, app().user, app().device, app().curLocation, tor);
                } catch (Talk2MeException e) {
                    showException(e);
                }

                refreshViews();
                dialog.cancel();
            }
        }).setTitle("Verwijderen?").setNegativeButton("Annuleren", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
