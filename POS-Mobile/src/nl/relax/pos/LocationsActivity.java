package nl.relax.pos;

import java.util.List;

import nl.relax.pos.client.Location;
import nl.relax.pos.client.Talk2MeException;
import nl.relax.pos.util.PosAsyncTask;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class LocationsActivity extends PosActivity {

    Handler handler = new ErrorHandler(this, true);

    ArrayAdapter<Location> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());
        final List<Location> rows = app().divisionData.getLocations();

        if (rows.size() == 1) {
            app().curLocation = rows.get(0);
            Intent intent = new Intent(getBaseContext(), TablesActivity.class);
            startActivity(intent);
            finish();
        }

        setContentView(R.layout.location);
        super.initBackgroundWorker(true, true, true);

        setTitle(((PosApplication) getApplication()).license.getName() + " | " + app().user.getName());
        this.adapter = new ArrayAdapter<Location>(this, android.R.layout.simple_list_item_1, rows);

        final ListView lv1 = (ListView) findViewById(R.id.locations);
        lv1.setClickable(true);
        lv1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                app().curLocation = rows.get(position);

                Intent intent = new Intent(getBaseContext(), TablesActivity.class);
                startActivity(intent);
            }
        });

        lv1.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        new PosAsyncTask<Void, Void, Void>(this) {
            @Override
            protected Void task(Void... params) throws Exception {
                app().divisionData.refreshLocations(app().sha);
                return null;
            }

            @Override
            protected void done(Void v) {
                adapter.notifyDataSetChanged();
            }
        }.execute();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        // logging out
        // TODO: Do the same in case there was only one Location (which means
        // there is no LocationActivity)
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Weet u zeker dat u wilt uitloggen?").setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            app().client.logout(((PosApplication) getApplication()).sha, app().division, app().user);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    LocationsActivity.super.onBackPressed();
                                }
                            });
                        } catch (Talk2MeException e) {
                            showExceptionAndFinish(e);
                        }
                    }
                }).start();
            }
        }).setNegativeButton("Annuleren", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}