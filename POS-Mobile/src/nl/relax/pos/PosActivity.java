package nl.relax.pos;

import java.io.IOException;

import nl.relax.pos.client.NoteHandledState;
import nl.relax.pos.client.NoteReadState;
import nl.relax.pos.client.NoteResponse;
import nl.relax.pos.client.Talk2MeClient;
import nl.relax.pos.client.Talk2MeException;
import nl.relax.pos.client.Talk2MeSessionExpiredException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Toast;

/**
 * Convenience base class
 * 
 * @author erik
 */
public class PosActivity extends Activity implements NoteReceiver {

    private static final long[] pattern = { 0, 300, 300, 300, 300, 300, 300 };
    private final Handler exceptionHandler = new ErrorHandler(this, false);
    private final Handler fatalHandler = new ErrorHandler(this, true);
    private boolean isNoteReceiver;

    protected final Handler ioErrorHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Toast.makeText(PosActivity.this, "Geen verbinding", Toast.LENGTH_SHORT).show();
        }
    };

    protected final Handler updateNoteResponseHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            NoteResponse notes = (NoteResponse) msg.obj;
            NoteResponse previous = app().curNotes;

            int prevNotes = previous == null ? 0 : previous.size();
            int curNotes = notes == null ? 0 : notes.size();

            int newNotes = curNotes - prevNotes;

            app().curNotes = notes;

            // if (newNotes > 0) {
            notifyNoteReceived(notes, newNotes);
            // }
        }
    };

    protected final Handler msgBoxHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String message = msg.obj.toString();

            AlertDialog.Builder builder = new AlertDialog.Builder(PosActivity.this);
            builder.setMessage(message).setCancelable(true).setPositiveButton("Sluiten", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    };

    private volatile Thread backgroundWorker;
    private boolean useBackgroundWorker;
    private boolean preDelay;
    private MenuItem sendMessageMenuItem;
    private Menu menu;
    private Object notes;
    private BaseAdapter notesAdapter;

    public void msgBox(String message) {
        Message msg = new Message();
        msg.obj = message;
        msgBoxHandler.sendMessage(msg);
    }

    public void showException(Exception e) {
        Message msg = new Message();
        msg.obj = e;
        exceptionHandler.sendMessage(msg);
    }

    public void showExceptionAndFinish(Exception e) {
        Message msg = new Message();
        msg.obj = e;
        fatalHandler.sendMessage(msg);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app().curActivity = this;
    }

    @Override
    public void onResume() {
        super.onResume();
        app().curActivity = this;
        if (useBackgroundWorker) {
            stopBackgroundWorker();
            backgroundWorker = new Thread(new Runnable() {
                @Override
                public void run() {

                    // Refresh the session timer the first time notes are
                    // received. After that, set to false
                    boolean keepAlive = true;

                    while (backgroundWorker != null && !backgroundWorker.isInterrupted()) {

                        if (preDelay) {
                            try {
                                Thread.sleep(app().config.getTablesRefreshInterval());
                            } catch (InterruptedException e) {
                                if (!forcedInterrupt) {
                                    return;
                                } else {
                                    forcedInterrupt = false;
                                }
                            }
                        }

                        try {
                            runBackgroundTasks(app().client);

                            if (isNoteReceiver) {
                                NoteResponse noteResponse = app().client.getNote(app().sha, keepAlive, null, 0, 0, NoteReadState.NOT_READ,
                                        NoteHandledState.NOT_HANDLED);
                                keepAlive = false;

                                if (noteResponse != null) {
                                    Message m = new Message();
                                    m.obj = noteResponse;
                                    updateNoteResponseHandler.sendMessage(m);
                                }
                            }
                        } catch (Talk2MeException e) {
                            if (e != null && e.getCause() != null && (e.getCause() instanceof IOException)) {
                                Message m = new Message();
                                ioErrorHandler.sendMessage(m);
                            } else {
                                showException(e);
                                final boolean expired = (e instanceof Talk2MeSessionExpiredException)
                                        || ((e.getCause() != null) && e.getCause() instanceof Talk2MeSessionExpiredException);

                                if (expired) {
                                    // end the thread if the session is expired
                                    return;
                                }
                            }
                        } catch (Exception e2) {
                            return;
                        }

                        if (!preDelay) {
                            try {
                                Thread.sleep(app().config.getTablesRefreshInterval());
                            } catch (InterruptedException e) {
                                if (!forcedInterrupt) {
                                    return;
                                } else {
                                    forcedInterrupt = false;
                                }
                            }
                        }
                    }
                }
            });
            backgroundWorker.start();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopBackgroundWorker();
    }

    protected void stopBackgroundWorker() {
        if (backgroundWorker != null) {
            backgroundWorker.interrupt();
            backgroundWorker = null;
        }
    }

    /**
     * Override to run extra tasks on the background thread
     * 
     * @param client
     * @throws Exception
     */
    protected void runBackgroundTasks(Talk2MeClient client) throws Exception {
    }

    /**
     * Short-hand for <code>(PosApplication) getApplication();</code>
     * 
     * @return
     */
    protected PosApplication app() {
        return (PosApplication) getApplication();
    };

    /**
     * Call this method in onCreate to use the background worker.
     * 
     * @param useBackgroundWorker
     * @param preDelay
     * @param isNoteReceiver
     */
    protected void initBackgroundWorker(boolean useBackgroundWorker, boolean preDelay, boolean isNoteReceiver) {
        this.useBackgroundWorker = useBackgroundWorker;
        this.preDelay = preDelay;
        this.isNoteReceiver = isNoteReceiver;

        if (isNoteReceiver) {
            updateMailButton();
        }

        if (sendMessageMenuItem != null) {
            sendMessageMenuItem.setEnabled(isNoteReceiver);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add("Bericht verzenden...");

        item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                new SendNoteDialog(PosActivity.this).show();
                return true;
            }
        });

        this.menu = menu;
        this.sendMessageMenuItem = item;

        item.setEnabled(isNoteReceiver);

        return true;
    }

    protected boolean isNoteReceiver() {
        return isNoteReceiver;
    }

    protected String check(String s) {
        if (s == null || "anyType{}".equals(s)) {
            return "";
        } else {
            return s;
        }
    }

    public void setNotesAdapter(BaseAdapter listAdapter) {
        this.notesAdapter = listAdapter;
    }

    /**
     * Override this method to update the GUI for received notes. This method is
     * called on the main GUI thread, so no need for handlers.
     * 
     * @param noteResponse
     */
    @Override
    public void notifyNoteReceived(NoteResponse noteResponse, int newNotes) {
        if (newNotes > 0) {
            Toast.makeText(this, "Nieuwe berichten: " + newNotes, Toast.LENGTH_SHORT).show();
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(pattern, -1);
        }
        updateMailButton();
        if (notesAdapter != null) {
            notesAdapter.notifyDataSetChanged();
        }
    }

    public void updateMailButton() {
        View button = findViewById(R.id.btnMail);
        if (button != null) {
            if (app().curNotes == null || app().curNotes.size() == 0) {
                button.setVisibility(View.GONE);
            } else {
                button.setVisibility(View.VISIBLE);
                button.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        new NotesDialog(PosActivity.this, app().curNotes).show();
                    }
                });
            }
        }
    }

    volatile boolean forcedInterrupt;

    public void forceBackgroundWorker() {
        if (useBackgroundWorker && backgroundWorker != null) {
            forcedInterrupt = true;
            try {
                backgroundWorker.interrupt();
            } catch (Throwable ignore) {
                ignore.printStackTrace();
            }
        }
    }
}
