package nl.relax.pos;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import nl.relax.pos.client.TableOrderRow;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TableOrderAdapter extends BaseAdapter {

    static final DecimalFormat FORMAT;
    static {
        FORMAT = new DecimalFormat("###0.00");
        FORMAT.setDecimalSeparatorAlwaysShown(true);
        FORMAT.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.FRANCE));
    }

    private final List<TableOrderRow> orders;
    Context mcontext;
    Resources res;

    public TableOrderAdapter(Context context, List<TableOrderRow> items) {
        this.orders = items;
        mcontext = context;
        res = context.getResources();
    }

    private String orderLeftString(TableOrderRow tor) {
        int level = tor.getLevel();
        String indent;
        if (tor.getIdArticleParent() > 0) {
            if (level > 1) {
                indent = "              ";
            } else {
                indent = "       - ";
            }
        } else {
            indent = "";
        }

        String s = (tor.getIdArticleParent() > 0 ? indent : (tor.getQuantity() + "x ")) + tor.getArticleName();
        return s;
    }

    private String orderRightString(TableOrderRow tor) {
        if (tor.getIdArticleParent() > 0 && tor.getArticlePrice() == 0) {
            return "";
        } else {
            return FORMAT.format((tor.getArticlePrice() / 100d) * tor.getQuantity());
        }
    }

    private void setColors(int pos, TableOrderRow tor, View cv, TextView t1, TextView t2, TextView t3) {
        int bg = getBgColor(pos, tor);
        t1.setBackgroundColor(bg);
        t2.setBackgroundColor(bg);
        t3.setBackgroundColor(bg);

        //        cv.setBackgroundColor(bg);

        int textColor;

        if (tor.id == 0) {
            // new article
            textColor = 0xffffffff;
        } else {
            // already ordered
            if (tor.getDeleted() != 0) {
                textColor = 0xff808080;
            } else {
                textColor = 0xffb0b0b0;
            }
        }

        t1.setTextColor(textColor);
        t2.setTextColor(textColor);
        t3.setTextColor(textColor);
    }

    private int getBgColor(int pos, TableOrderRow tor) {
        if (tor.id == 0) {
            return res.getInteger(R.color.bg_center);
        }

        boolean alt = true;
        int batch = -1;
        for (int i = 0; i <= pos; i++) {
            TableOrderRow t = orders.get(i);
            if (t.getIdArticleParent() == 0 && t.getOrderSession() != batch) {
                batch = t.getOrderSession();
                alt = !alt;
            }
        }

        return alt ? res.getInteger(R.color.bg_ordered_alt) : res.getInteger(R.color.bg_ordered);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View cv = convertView;
        if (cv == null) {
            LayoutInflater vi = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            cv = vi.inflate(R.layout.order_item, null);
        }
        TableOrderRow tor = orders.get(position);

        TextView tLeft = (TextView) cv.findViewById(R.id.orderLineLeft);
        TextView tCourse = (TextView) cv.findViewById(R.id.orderLineCourse);
        TextView tRight = (TextView) cv.findViewById(R.id.orderLineRight);

        if (tor.getLevel() > 1) {
            tLeft.setTypeface(null, Typeface.ITALIC);
        } else {
            tLeft.setTypeface(null, Typeface.NORMAL);
        }

        tLeft.setText(orderLeftString(tor));
        tCourse.setText(tor.getNameCourseShort());
        tRight.setText(orderRightString(tor));

        setColors(position, tor, cv, tLeft, tCourse, tRight);

        if (tor.getDeleted() != 0) {
            // deleted
            tLeft.setPaintFlags(tLeft.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tCourse.setPaintFlags(tLeft.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tRight.setPaintFlags(tRight.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            // not deleted
            tLeft.setPaintFlags(tLeft.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            tCourse.setPaintFlags(tLeft.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            tRight.setPaintFlags(tRight.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
        }

        if (tor.getIdArticleParent() > 0) {
            // sub article
            tLeft.setPadding(1, 0, 1, 2);
            tCourse.setPadding(1, 0, 1, 2);
            tRight.setPadding(1, 0, 10, 2);
            tLeft.setTextSize(13);
            tCourse.setTextSize(13);
            tRight.setTextSize(13);
        } else {
            // main article
            tLeft.setPadding(1, 10, 1, 10);
            tCourse.setPadding(1, 10, 1, 10);
            tRight.setPadding(1, 10, 10, 10);
            tLeft.setTextSize(14);
            tCourse.setTextSize(14);
            tRight.setTextSize(14);
        }

        return cv;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Object getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
