package nl.relax.pos;

import java.util.ArrayList;
import java.util.List;

import nl.relax.pos.client.Article;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ArticlesAdapter extends BaseAdapter {

    private final PosActivity activity;

    private final List<Article> list = new ArrayList<Article>();
    private final LayoutInflater vi;

    public ArticlesAdapter(PosActivity activity) {
        super();
        this.activity = activity;
        this.vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public void clear() {
        list.clear();
        super.notifyDataSetChanged();
    }

    public void add(Article article) {
        list.add(article);
        super.notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = vi.inflate(R.layout.article_item2, parent, false);
        }

        TextView txtName = (TextView) view.findViewById(R.id.txtArticleName);
        TextView txtPrice = (TextView) view.findViewById(R.id.txtArticlePrice);

        Article sg = (Article) getItem(position);
        String name = sg.getName();
        String price = sg.getPriceFormatted();
        txtName.setText(name.replace("anyType{}", "(onbekend)"));
        txtPrice.setText(price);

        if (sg.getIdArticleChoices() > 0) {
            txtName.setText(txtName.getText().toString() + "*");
        }

        int start;
        boolean unavailable = !sg.isAvailable();
        if (unavailable) {
            txtName.setTextColor(0xff505050);
            txtPrice.setTextColor(0xff606060);

            start = 0xff000000;
        } else {
            txtName.setTextColor(0xffffffff);
            txtPrice.setTextColor(0xffffffff);

            start = ((sg.getRed() << 16) | (sg.getGreen() << 8) | sg.getBlue() | 0xff000000);
        }

        GradientDrawable g = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[] { start, 0xff181818 });
        g.setGradientCenter(0.5f, 0.5f);
        g.setCornerRadius(3);
        g.setStroke(1, 0xff808080);
        view.setBackgroundDrawable(g);
        return view;
    }

}
