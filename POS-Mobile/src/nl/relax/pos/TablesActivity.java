package nl.relax.pos;

import java.util.List;

import nl.relax.pos.client.Course;
import nl.relax.pos.client.Table;
import nl.relax.pos.client.TableStatusAction;
import nl.relax.pos.client.TableStatusResponse;
import nl.relax.pos.client.Talk2MeClient;
import nl.relax.pos.client.Talk2MeException;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TablesActivity extends PosActivity {

    private volatile ArrayAdapter<Table> tablesModel;

    private final Handler updateTableViewHandler = new Handler() {
        @SuppressWarnings("unchecked")
        @Override
        public void handleMessage(Message msg) {
            List<Table> tables = (List<Table>) msg.obj;
            tablesModel.clear();
            for (Table table : tables) {
                tablesModel.add(table);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablesactivity_grid);
        super.initBackgroundWorker(true, false, true);
        setTitle(app().license.getName() + " | " + app().user.getName());

        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().build());

        TextView txt = (TextView) findViewById(R.id.curLoc);
        txt.setText(app().curLocation.getName());

        AbsListView tablesView = (AbsListView) findViewById(R.id.tables_grid);
        tablesView.setClickable(true);

        tablesModel = new ArrayAdapter<Table>(this, R.layout.table_item) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View tv = convertView;

                if (tv == null) {
                    tv = inflater.inflate(R.layout.table_item, null);
                }

                View view = super.getView(position, tv, parent);

                Table sg = getItem(position);
                int start = (sg.getRed() << 16) | (sg.getGreen() << 8) | sg.getBlue() | 0xff000000;
                GradientDrawable g = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[] { start, 0xff181818 });
                g.setGradientCenter(0.5f, 0.5f);
                g.setCornerRadius(5);
                view.setBackgroundDrawable(g);
                return view;
            }
        };

        tablesView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                app().curTable = tablesModel.getItem(position);
                try {
                    app().client.updateTableStatus(app().sha, app().device, app().curLocation, app().curTable, TableStatusAction.BUSY, app().division);
                    app().curTable.initGuestsAndOrders(app().client, app().sha, app().curLocation);
                    stopBackgroundWorker();

                    Intent intent = new Intent(getBaseContext(), TableActivity.class);
                    startActivity(intent);
                } catch (Talk2MeException e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(TablesActivity.this);
                    builder.setMessage(e.getMessage()).setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });

        tablesView.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                final Table t = tablesModel.getItem(position);
                final int status = t.getIdStatus();
                final int course = t.getIdCourse();

                // if (status == TableStatus.STATUS_FREE || course == 0) {
                // return false;
                // }

                final List<Course> courses = app().courses.getAll();

                AlertDialog.Builder builder = new AlertDialog.Builder(TablesActivity.this);
                builder.setTitle("Tafel " + t.getName());
                String msg = "Status: " + t.getStatus();
                msg += "\nTotaalbedrag: " + t.getTableTotalFormatted();

                if (course != 0) {
                    msg += "\nHuidige gang: " + t.getCourse();
                }

                // ArrayAdapter<Course> aa = new
                // ArrayAdapter<Course>(TablesActivity.this,
                // android.R.layout.select_dialog_singlechoice, courses);
                // builder.setSingleChoiceItems(aa, course - 1, new
                // DialogInterface.OnClickListener() {
                // @Override
                // public void onClick(DialogInterface dialog, int item) {
                // Course c = courses.get(item);
                // t.setIdCourse(c.id);
                // try {
                // app().client.updateTableStatus(app().sha, app().device,
                // app().curLocation, t, TableStatusAction.FREE);
                // } catch (POSException e) {
                // // TODO Auto-generated catch block
                // e.printStackTrace();
                // showException(e);
                // }
                // dialog.dismiss();
                // }
                // });

                builder.setMessage(msg);
                if (course != 0 && course != courses.size()) {
                    try {
                        final TableStatusResponse tableStatus = app().client.getTableStatus(app().sha, app().curLocation, t, app().courses);
                        if (tableStatus.getCourses().size() > 0) {
                            builder.setPositiveButton("Volgende gang", new OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                    showSetCourseDialog(t, tableStatus);

                                }
                            });
                        }
                    } catch (Talk2MeException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        showException(e);
                    }

                }
                builder.setNegativeButton("Sluiten", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCancelable(true);

                AlertDialog alert = builder.create();
                alert.show();

                return true;
            }
        });

        tablesView.setAdapter(tablesModel);
    }

    protected void showSetCourseDialog(final Table t, final TableStatusResponse status) {
        final List<Course> courses = status.getCourses();
        AlertDialog.Builder builder = new AlertDialog.Builder(TablesActivity.this);
        builder.setTitle("Kies een gang");
        ArrayAdapter<Course> aa = new ArrayAdapter<Course>(TablesActivity.this, android.R.layout.select_dialog_singlechoice, courses);
        builder.setSingleChoiceItems(aa, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                Course c = courses.get(item);

                t.setIdCourse(c.id);
                try {
                    app().client.updateTableStatus(app().sha, app().device, app().curLocation, t, TableStatusAction.CHANGE_COURSE, app().division);
                } catch (Talk2MeException e) {
                    e.printStackTrace();
                    showException(e);
                }

                dialog.dismiss();
            }
        });
        builder.setCancelable(true);
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        app().curTable = null;
    }

    @Override
    protected void runBackgroundTasks(Talk2MeClient client) throws Exception {
        List<Table> tables = app().client.getTables(app().sha, false, app().curLocation);
        if (tables != null) {
            Message m = new Message();
            m.obj = tables;
            updateTableViewHandler.sendMessage(m);
        }
    }

}
